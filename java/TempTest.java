/**
 *This class tests the methods of Temperature.java
 *
 *@author Rebecca Dornin
 *
 */

public class TempTest
{
        public static void main(String args[])
    {
	Temperature temp1 = new Temperature();
	System.out.println();
	System.out.println("Created the temp1 object, testing first constructor, no params");
	System.out.println("This is the value for temp1 temperature in fahrenheit:" + temp1.getFahrenheit());
	System.out.println("This is the value for temp1 scale:" + temp1.getScale());
	System.out.println();

	System.out.println("Testing the setValue() method:");
	System.out.println("Current temp1 temperature value:" + temp1.getCelcius());
	System.out.println("Setting new temperature value to 100 degrees celcius:");
	temp1.setValue(100);
	System.out.println("The new temperature value for for temp1 is:" + temp1.getCelcius());
	System.out.println();
	System.out.println();

	Temperature temp2 = new Temperature(212);
	System.out.println("Created the temp2 object, testing second constructor, with param of 212");
	System.out.println("This is the value for temp2 temperature in celcius:" + temp2.getCelcius());
	System.out.println("This is the value for temp2 scale:" + temp2.getScale());
	System.out.println();

	System.out.println("Testing the setScale() method:");
	System.out.println("Current temp2 scale value:" + temp2.getScale());
	System.out.println("Setting new temp2 scale value to celcius:");
	temp2.setScale('C');
	System.out.println("The new scale value for for temp2 is:" + temp2.getScale());
	System.out.println();
	System.out.println();
    
	Temperature temp3 = new Temperature('F');
	System.out.println("Created the temp3 object, testing 3rd constructor, with param of F");
	System.out.println("This is the value for temp3 temperature in fahrenheit:" + temp3.getFahrenheit());
	System.out.println("This is the value for temp3 scale:" + temp3.getScale());
	System.out.println();


	System.out.println("Testing the setBoth() method:");
	System.out.println("Current temp3 scale value:" + temp3.getScale());
	System.out.println("Current temp3 temperature value:" + temp3.getFahrenheit());
	System.out.println("Setting new temp3 scale value to celcius and new temperature value to -40:");
	temp3.setBoth('C',-40);
	System.out.println("The new scale value for for temp3 is:" + temp3.getScale());
	System.out.println("The new temperature value for for temp3 is:" + temp3.getCelcius());
	System.out.println();
	System.out.println();

    	Temperature temp4 = new Temperature('F',40);
	System.out.println("Created the temp4 object, testing fourth constructor, with params of F and 40");
	System.out.println("This is the value for temp4 temperature in fahrenheit:" + temp4.getFahrenheit());
	System.out.println("This is the value for temp4 scale:" + temp4.getScale());
	System.out.println();


	System.out.println("Testing the isEqualTo() method:");
	System.out.println("temp3.isEqualTo(temp4), temp3 is -40 celcius and temp4 is 40F");
	temp3.isEqualTo(temp4);
	System.out.println();
	System.out.println();
    
	


	System.out.println("Testing the isGreaterThan() method:");
	System.out.println("temp3.isGreaterThan(temp4), temp3 is -40 celcius and temp4 is 40F");
	temp3.isGreaterThan(temp4);
	System.out.println();
	System.out.println();
    
	


	System.out.println("Testing the isLessThan() method:");
	System.out.println("temp3.isLessThan(temp4), temp3 is -40 celcius and temp4 is 40F");
	temp3.isLessThan(temp4);
	System.out.println();
	System.out.println();
   
	System.out.println("Testing 0 degree celcius and 32 degrees fahrenheit.");
	System.out.println("Resetting temp4 values to 0 degrees C and temp3 to 32 degrees F");
	temp4.setBoth('C',0);
	temp3.setBoth('F',32);
	System.out.println("temp4 value in fahrenheit is:" + temp4.getFahrenheit());
	System.out.println("temp3 value in celcius is:" + temp3.getCelcius());
	System.out.println();
	System.out.println();


	System.out.println("Testing -40 degree celcius and -40 degrees fahrenheit.");
	System.out.println("Resetting temp4 values to -40 degrees C and temp3 to -40 degrees F");
	temp4.setBoth('C',-40);
	temp3.setBoth('F',-40);
	System.out.println("temp4 value in fahrenheit is:" + temp4.getFahrenheit());
	System.out.println("temp3 value in celcius is:" + temp3.getCelcius());
	System.out.println("are temp3 and temp4 equal? testing isEqualTO()");
	temp3.isEqualTo(temp4);
	System.out.println();
	System.out.println();


	System.out.println("Testing 100 degree celcius and 212 degrees fahrenheit.");
	System.out.println("Resetting temp4 values to 100 degrees C and temp3 to 212 degrees F");
	temp4.setBoth('C',100);
	temp3.setBoth('F',212);
	System.out.println("temp4 value in fahrenheit is:" + temp4.getFahrenheit());
	System.out.println("temp3 value in celcius is:" + temp3.getCelcius());
	System.out.println();
	System.out.println();


    }

}
