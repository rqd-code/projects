//Temperature.java
/**
 *
 *This class has four constructor methods: 
 *a zero parameter method that creates an object of Zero Degrees Celcius
 *a one parameter method that takes a number value and creates a temperature value, assuming we mean farhenheit
 *a one paraemter method that takes a characted, C of F, and creates a Temperature object of Zero Degrees
 *a two parameter method that takes both a number and a character argument--with error checking if character is inappropriate
 *
 *Three Accessor (Getter) Methods:
 *Two return the temperature value, getFahrenheit() and getCelcius()
 *One returns the value, getScale()
 *
 *Three Setter Methods to change an existing Temperature Object:
 *one sets the temp value--setValue(a numeric value)
 *one sets the type to be Fahrenheit or Celcius--setScale(fahrenhiet or celcius scale)
 *one sets both the temp value and the scale type--setBoth(a numeric value,scale)
 *
 *Three Comparison Methods:
 *one to test if two temps are equal
 *one to test if one temp is greater than another
 *one to test if one temp is less than another
 *
 *@author Rebecca Dornin
 */

import utils.*;

public class Temperature {
    
    private float tempValue;
    private char scale;

    /**
     *This method creates an object of zero degrees celcius
     */
    public Temperature()
    {
	tempValue = 0;
	scale = 'C';
    }

    /**
     * a one parameter method that takes a number value and creates a temperature value, assuming we mean farhenheit
     *
     *@param newTempValue the new temperature value
     */
    public Temperature(float newTempValue)
    {
	tempValue = newTempValue;
	scale = 'F';
    }
    
    /**
     *a one paraemter method that takes a characted, C of F, and creates a Temperature object of Zero Degrees
     *
     *@param newScale this value will be the value of the scale of the object
     */
    public Temperature(char newScale)
    {
	tempValue = 0;
	scale = newScale;
    }
    
    /**
     *a two parameter method tht takes in a char and float, checking to make
     *sure the char is a C or F and then resets the temperature and scale values to the param values
     *
     *@param newScale this value will be the value of the scale of the object
     *@param newTempValue  this value will be the value of the temp of the object
     */
    public Temperature( char newScale, float newTempValue)
    {
	if (newScale != 'F' &&  newScale !='C')
	    {
		System.out.println("You need to use an appropriate value for the scale, C or F. We are going to use F");
		scale = 'F';
	    }
	else
	    {
		scale = newScale;
	    }

	tempValue = newTempValue;
    }

    /**
     *this accessor method takes a celcius temperature value and returns a fahrenheit value
     *
     *@param degreesC this value is the celcius temperature value
     *@return         a celcius temperature value
     */
    public float getFahrenheit()
    {
	if (scale == 'C')
	    {
		tempValue = ((9 * tempValue) / 5) + 32; 
		return tempValue;
	    }
	else
	    {
		tempValue = tempValue;
	    }
	return tempValue;
    }

    /**
     *this accessor method takes a fahrenheit temperature value and returns a celcius value
     *
     *@param degreesF this value is the fahrenhiet temperature value
     *@return         a fahrenheit temperature value
     */
    public float getCelcius ()
    {
	if (scale == 'F')
	    {
		
		tempValue = 5 * (tempValue - 32) / 9;
		return tempValue;
	    }
	else
	    {
		tempValue = tempValue;
	    }
	return tempValue;
	    
    }

    /**
     *this accessor method resets the scale to a newscale
     *
     *@param newScale this is the newScale value, either C or F
     */
    
    public char getScale()
    {
	return scale;
    }

    /**
     *this setter method resets the tempValue
     *
     *@param newTempValue this is the newTemp value
     */

    public void setValue(float newTempValue)
    {
	tempValue = newTempValue;
    }


    /**
     *this setter method resets the tempvalue to match the scale value, checking to make sure the scales are equal or not
     *
     *@param newScale this is the newScale value
     */

    public void setScale(char newScale)
    {
	if (scale != newScale)
	    {
		if (newScale == 'C')
		    {
			tempValue = 5 * (tempValue - 32) / 9;
		    }
		else if (newScale == 'F')
		    {
			tempValue = ((9 * tempValue) / 5) + 32;
		    }
	    }
	else
	    {
		tempValue = tempValue;//basically do nothing;
	    }
	scale = newScale;
    }


    /**
     *this setter method resets the tempvalue and scale
     *
     *@param newScale this is the newScale value
     *@param newTempValue this is the newTemp value
     */

    public void setBoth(char newScale, float newTempValue)
    {
	if (newScale != 'F' && newScale !='C')
	    {
		System.out.println("You need to use an appropriate value for the scale, C or F. We are going to use F");
		scale = 'F';
	    }
	else
	    {
		scale = newScale;
	    }

	tempValue = newTempValue;
    }

    /**
     *this setter method checks to see if the temp values are equal based on scale value
     *
     *@param t this is the object that the temp and scale values are associated with
     */
    public void isEqualTo( Temperature t)
    {
	if (this.scale == t.scale)
	    {
		if (this.tempValue == t.tempValue)
		    {
			System.out.println("These temperature values are the same!");
		    }
		else
		    {
			System.out.println("They are not the same--what do you know...");
		    }
	    }
	else if (this.scale == 'F' && t.scale == 'C')
	    {
		if( this.tempValue == getFahrenheit())
		    {
			System.out.println("These temperature values are the same!");
		    }
		else
		    {
			System.out.println("They are not the same--what do you know...");
		    }
	    }
	else if (this.scale == 'C' && t.scale == 'F')
	    {
		if( this.tempValue == getCelcius())
		    {
			System.out.println("These temperature values are the same!");
		    }
		else
		    {
			System.out.println("They are not the same--what do you know...");
		    }
	    }

    }

    /**
     *this setter method checks to see if the temp values are greater than one another
     *
     *@param t this is the object that the temp and scale values are associated with
     */
    public void isGreaterThan( Temperature t)
    {
	if (this.scale == t.scale)
	    {
		if (this.tempValue > t.tempValue)
		    {
			System.out.println("This temperature is greater than the other temp.");
		    }
		else
		    {
			System.out.println("This temperature is less than the other temp.");
		    }
	    }
	else if (this.scale == 'F' && t.scale == 'C')
	    {
		if( this.tempValue > getFahrenheit())
		    {
			System.out.println("This temperature is greater than the other temp.");
		    }
		else
		    {
			System.out.println("This temperature is less than the other temp.");
		    }
	    }
	else if (this.scale == 'C' && t.scale == 'F')
	    {
		if( this.tempValue > getCelcius())
		    {
			System.out.println("This temperature is greater than the other temp.");
		    }
		else
		    {
			System.out.println("This temperature is less than the other temp.");
		    }
	    }

    }


    /**
     *this setter method checks to see if the temp values are less than one another
     *
     *@param t this is the object that the temp and scale values are associated with
     */
    public void isLessThan( Temperature t)
    {
	if (this.scale == t.scale)
	    {
		if (this.tempValue < t.tempValue)
		    {
			System.out.println("This temperature is less than the other temp.");
		    }
		else
		    {
			System.out.println("This temperature is greater than the other temp.");
		    }
	    }
	else if (this.scale == 'F' && t.scale == 'C')
	    {
		if( this.tempValue < getFahrenheit())
		    {
			System.out.println("This temperature is less than the other temp.");
		    }
		else
		    {
			System.out.println("This temperature is greater than the other temp.");
		    }
	    }
	else if (this.scale == 'C' && t.scale == 'F')
	    {
		if( this.tempValue < getCelcius())
		    {
			System.out.println("This temperature is less than the other temp.");
		    }
		else
		    {
			System.out.println("This temperature is greater than the other temp.");
		    }
	    }

    }

    public static void main(String args[])
    {
	
    }
}
















