import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
/**
 * This class sets up a swing window that displays a calculator
 *
 * @author Rebecca Dornin
 */
public class Calculator extends JFrame implements ActionListener
{
    public static final int WIDTH = 255;
    public static final int HEIGHT = 300;
    String num = null;
    String op = null;
    double val = 0.0;
    String action;
 	JButton button;
 	private JTextField jt;
 	JPanel jp;
 
    /***
     * Creates a new instance of Calculator and makes it visible on the screen
     */
    public static void main(String[] args)
    {
  Calculator c = new Calculator();
  c.setVisible(true);
    }
    /**
     * This method sets up the Calculator window object, closes it, and
     * makes it visible, sets up the buttons, frames, panels It sets up
     * the size and title and more,
  *  
  *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  * THIS IS A GRIDBAGLAYOUT FOR GRADUATE CREDIT
  *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */    
    public Calculator()
    {
  setSize(WIDTH,HEIGHT);  
  setTitle("Calculator");
  addWindowListener(new WindowDestroyer());
  getContentPane().setBackground(Color.gray);
  Container cp = getContentPane();
  cp.setLayout(new BorderLayout());
  jp = new JPanel();
  jt = new JTextField(val + "",20);
  jt.setBackground(Color.white);
  jp.add(jt);
  cp.add(jp,BorderLayout.NORTH);
 
 
  jp = new JPanel();
  GridBagLayout gridbag = new GridBagLayout();
  GridBagConstraints c = new GridBagConstraints();
  jp.setLayout(gridbag);
  c.fill = GridBagConstraints.BOTH;
  button = new JButton("C");
  c.weightx = 0.25;
  c.gridx = 0;
  c.gridy = 1;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
  button = new JButton("SQRT");
  c.gridx = 1;
  c.gridy = 1;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
  button = new JButton("/");
  c.gridx = 2;
  c.gridy = 1;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
 
  button = new JButton("*");
  c.gridx = 3;
  c.gridy = 1;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
  button = new JButton("7");
  c.gridx = 0;
  c.gridy = 2;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
  button = new JButton("8");
  c.gridx = 1;
  c.gridy = 2;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
  button = new JButton("9");
  c.gridx = 2;
  c.gridy = 2;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
  button = new JButton("-");
  c.gridx = 3;
  c.gridy = 2;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
  button = new JButton("-");
  c.gridx = 3;
  c.gridy = 2;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
  button = new JButton("4");
  c.gridx = 0;
  c.gridy = 3;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
  button = new JButton("5");
  c.gridx = 1;
  c.gridy = 3;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
  button = new JButton("6");
  c.gridx = 2;
  c.gridy = 3;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
  button = new JButton("+");
  c.gridx = 3;
  c.gridy = 3;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
  button = new JButton("1");
  c.gridx = 0;
  c.gridy = 4;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
  button = new JButton("2");
  c.gridx = 1;
  c.gridy = 4;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
  button = new JButton("3");
  c.gridx = 2;
  c.gridy = 4;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
  button = new JButton("=");
  c.gridheight = 2;
  c.gridx = 3;
  c.gridy = 4;
  c.weighty = 1;
  //c.fill = GridBagConstraints.BOTH;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
  button = new JButton("0");
  c.gridwidth = 2;
  c.gridheight = 1;
  c.gridx = 0;
  c.gridy = 5;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
 
  button = new JButton(".");
  c.gridx = 2;
  c.gridy = 5;
  c.gridwidth = 1;
  gridbag.setConstraints(button,c);
  button.addActionListener(this);
  jp.add(button);
  cp.add(jp,BorderLayout.CENTER);
    }
 public void calculate ()
    {
        if (num == null)
        {
            num = val + "";
        }
            double d = Double.valueOf(num).doubleValue();
            if (op == null)
            {
                val = d;
                num = null;
            }
      else if (op.equals("+"))
            {
       val += d;
                num = null;
            }
      else if (op.equals("-"))
            {
       val -= d;
                 num = null;
            }
      else if (op.equals("*"))
            {
       val *= d;
                num = null;
            }
      else if (op.equals("/"))
            {
       val /= d;
                num = null;
            }
            else if (op.equals("SQRT"))
&

nbsp;           {
                val = Math.sqrt(d);
                num = val + "";
            }
            else
                System.out.println("unknown: " + op);
        jt.setText(Double.toString(val));
    }
    
    public void append(String newNum)
    {
        if (num == null)
        {
            if (newNum.equals("."))
                num = "0.";
            else
                num = newNum;
        } else
        {
                num += newNum;
        }
    &n
bsp;   jt.setText(num);
    }
    /**
     * This is the method which actually handles the events which are
     * triggered by clicking on one of the buttons.  Figures out which
     * button was clicked, and changes the colors of the stop light
     * accordingly
  *
     * @param e this is the object that gets sent when someone fires
     *     the button
     */
    public void actionPerformed(ActionEvent e) 
    {
  action = e.getActionCommand();
  if (action.equals("C"))
   {
    val = 0.0;
    op = null;
                num = null;
    jt.setText(val + "");
   }
  else if (action.equals(("SQRT")))
   {
                op = action;
          calculate();
                op = null;
   }
  else if (action.equals(("/")))
   {
    calculate();
                op = action;
   }
  else if (action.equals(("*")))
    {
    calculate();
                op = action;
   }
  else if (action.equals("7"))
   {
    append(action);
   }
  else if (action.equals(("8")))
   {
    append(action);
   }
  else if (action.equals(("9")))
   {
    append(action);
   }
  else if (action.equals(("-")))
   {
    calculate();
                op = action;
   }
  else if (action.equals(("4")))
   {
    append(action);
   }
  else if (acti on.equals(("5")))
   {
    append(action);
   }
  else if (action.equals(("6")))
   {
    append(action);
   }
  else if (action.equals(("+")))
   {
    calculate();
                op = action;
   }
  else if (action.equals(("1")))
   {
    append(action);
   }
  else if (action.equals(("2")))
   {
    append(action);
   }
  else if (action.equals(("3")))
   {
    append(action);
      }
  else if (action.equals(("=")))
   {
                 calculate();
                op = null;
   }
  else if (action.equals(("0")))
   {
    append(action);
   }
  else if (action.equals((".")))
   {
                append(action);
   }
  else
   {
    jt.setText("Error");
   }
    }
}