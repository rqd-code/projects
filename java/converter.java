//Converter.java
/**
 *
 *This class sets up a window that converts miles to kilos, kilos to miles, and inches to cm
 *It has window listeners that know when to toggle between the three
 *I borrowed a little from the SwingDemo we did in class a few weeks ago
 *@author Rebecca Dornin
 */

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class Converter extends JFrame implements ActionListener {

    //all variables need to be class scope

    // in this version, declare the variable as instance variables
    JFrame jframe;
    JPanel topPanel, bottomPanel, middlePanel;
    JButton convert;
    JButton toggle;
    JButton convertInches;
    JLabel jlabel;
    JTextField input, output;

    public static final String I2CM = "Convert Inches to Centimeters";
    public static final String TOGGLE = "Toggle Button: Kilo to Miles/Miles to Kilometers";
    public static final String TM = "Type a distance in miles:";
    public static final String TK = "Type a distance in kilometers:";
    public static final String TI = "Type a number of inches:";


    private double inputField = 0;

    /**
     *This method sets up the Converter window object, closes it, and makes it visible, sets up the buttons, labels, frames, textfields
     *It sets up the size and more
     */     
    public Converter() {
        //initialize the instance variables in the constructor
        jframe = new JFrame("Miles to Kilometer, Kilometer to Mile, Inches to CM Converter");

        jframe.setSize(600,200);
        // in case you do not want to use an anonymous class
        //jframe.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE);
		jframe.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					System.exit(0);
				}
		});


        topPanel = new JPanel();
	middlePanel = new JPanel();
        bottomPanel = new JPanel();
        convert = new JButton("Convert!");
	toggle = new JButton(TOGGLE);
	convertInches = new JButton(I2CM);
	input = new JTextField(10);
	output = new JTextField(30);
	output.setEditable(false);
        jlabel = new JLabel(TM);

        topPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        topPanel.add(jlabel);
	topPanel.add(input);
	topPanel.add("Convert!",convert);
	convert.addActionListener(this);
        middlePanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        middlePanel.add(output);
        bottomPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
	bottomPanel.add(toggle);
	toggle.addActionListener(this);
	bottomPanel.add(convertInches);
	convertInches.addActionListener(this);
        jframe.getContentPane().add(topPanel, BorderLayout.NORTH);
        jframe.getContentPane().add(middlePanel, BorderLayout.CENTER);
        jframe.getContentPane().add(bottomPanel, BorderLayout.SOUTH);
       	jframe.setVisible(true);


    }

    /**
      * This is the method which actually handles the events which
      * are triggered by clicking on one of the buttons.
      * Figures out which button was clicked, and changes jlabel's
      * text to reflect the new information. Based on what jlabel is set at, it then determines whether it should convert kilos to miles, miles to kilos, 
      * inches to cm
      * @param e this is the object that gets sent when someone fires the button
      */
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==convert) {
	    if(jlabel.getText() == (TM)){
		double temp = Double.parseDouble(input.getText().trim());
		inputField = (temp)/0.62;
		output.setText(temp + " miles equals " + inputField + " kilometers.");
               }
            else if (jlabel.getText() == (TK)){
		double temp = Double.parseDouble(input.getText().trim());
		inputField = (temp)*0.62;
		output.setText(temp + " kilometers equals " + inputField + " miles.");
               }
            else if (jlabel.getText() == (TI)){
		double temp = Double.parseDouble(input.getText().trim());
		inputField = (temp)*2.54;
		output.setText(temp + " inches equals " + inputField + " centimeters.");
               }
        }
        else if(e.getSource()==toggle) {
	    if(jlabel.getText() == (TM)){
                  jlabel.setText(TK);
		  input.setText("");
		  output.setText("");
               }
            else if (jlabel.getText() == (TK)){
                  jlabel.setText(TM);
		  input.setText("");
		  output.setText("");
               }
            else if (jlabel.getText() == (TI)){
                  jlabel.setText(TK);
		  input.setText("");
		  output.setText("");
               }
        }
        else if(e.getSource()==convertInches) {
                  jlabel.setText(TI);
		  input.setText("");
		  output.setText("");
               }
    }

    /***
     * Creates a new instance of Converter, and calls the methods
     * required to assemble the JFrame and make it visible
     ***/
    public static void main(String [] args) {
        // we need a new instance
        Converter c = new Converter();

    }

}
