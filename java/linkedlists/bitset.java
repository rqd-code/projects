/** Bitset.java
 *
 *  Stores sets of small integers, in the range 0..maxSize-1, 
 *    for user-specified 'maxSize'
 *
 *  NOT related to java.util.Bitset!
 *  Last Modified January 2, 2003
 */

import java.util.StringTokenizer;
import java.io.*;

class Bitset
{
    private byte[] byteArray;    // the array of bytes (8-bit integers)
    private int    maxSize;      // max # of members allowed.
	
//**************************************************************************
//PART A--testing cardinality
//**************************************************************************
	
	int cardinality()
	{
		int i;
		int j = 0;
		if (is_null() || (byteArray.length == 0))
		{
			return 0;
		}
		else
		{
       for (i = 0 ; i < maxSize; i++) 
             if ( member(i)) 
			 	{
				j++;
				}
			return j;
		}
	}
	
//**************************************************************************
//PART B--finding the subset
//**************************************************************************
	
	boolean subset(Bitset set)
	{
				Bitset temp2 = intersect(set);
				if (set.equals(temp2))
					{
						return true;
					}
				else
					{
						return false;
					}	
	}

	
	

	//int[] temp = new int[];     //creating temporary array for subset method    
    // First 3 constructor methods
    Bitset()    // make an empty set of capacity zero.
     {
        maxSize = 0;
        byteArray = null;
     }

    // make a set able to hold 'size' elements each in range 0 .. size-1
    Bitset (int size) 
     {
        maxSize   = size;
        int nbyte = (size+7)/8;
        byteArray = new byte[nbyte];    // new array, all zeroes
     }

    // make a copy of 'setA'
    Bitset (Bitset setA)
     {
        maxSize   = setA.maxSize;
        int nbyte = setA.byteArray.length;
        byteArray = new byte[nbyte];        // new array, all zeroes
        System.arraycopy(setA.byteArray, 0, byteArray, 0, setA.byteArray.length);  
     } 

	 
	 
	
    // Sets the bit at given offset from address byteArray to 1 
    // i.e., adds element 'offset' to the set
    private void setBit (int n)
     {
        int whichByte = n / 8;
        int whichBit = n % 8;
        byteArray[whichByte] |= (1 << whichBit);
     }

    // Returns true if the bit at given offset from address 
    // .. byteArray is set; else false. 
    // i.e., determines whether element 'offset' is in the set
    private boolean getBit (int n)
     {
        int whichByte = n / 8;
        int whichBit = n % 8;
        return ( (byteArray[whichByte] & (1 << whichBit)) != 0 );
     }

    // Clears the bit at given offset from address byteArray to 0 
    // i.e., removes element 'offset' from the set, if present.
    private void clearBit (int n)
     {
        int whichByte = n / 8;
        int whichBit = n % 8;
        byteArray[whichByte] &= ( (1 << whichBit)^255);
     }

    void clear()
    {
        if (byteArray == null)
            error ("clear: Can't a set that hasn't been constructed!");
        for (int i = 0; i < byteArray.length; i++)
            byteArray[i] = 0;
    }

    // change existing set to empty set of capacity 'size'
    void setSize (int size)
    {
        maxSize   = size;
        int nbyte = (size+7)/8;
        byteArray = new byte[nbyte];    // new array, all zeroes
    }
	

    boolean member (int i)
    {
        if (i >= maxSize) return false;
        return ( getBit(i) );
    }

    boolean contains (int i)  // same as member(), reads well:
    {                        // e.g., mySet.contains(3);
        return member(i);
    }

    void include (int i)
    {
        if (i >= maxSize)
            error ("include: " + i + "  is too large to fit inside the set");
        setBit (i);
    }

    void exclude (int i)
    {
        if (i >= maxSize)
            error ("exclude: " + i + "  is too large be inside the set");
        clearBit (i);
    }

    // copies setA to receiver, without changing latter's capacity.
    Bitset gets (Bitset setA)
    {
        if (byteArray.length < setA.byteArray.length)
            error ("gets: source set larger than dest. set");
        clear();

        int nbyte = setA.byteArray.length;
        for (int i = 0; i < nbyte; i++) // copy byteArray from arg.
            byteArray[i] = setA.byteArray[i];
        return this;                    // return receiver, updated
    }

    Bitset union (Bitset setB)
    {
        Bitset temp = new Bitset (maxSize>setB.maxSize ? this : setB);

        int nbyte = Math.min (byteArray.length, setB.byteArray.length);
        for (int i = 0; i < nbyte; i++)
            temp.byteArray[i] = (byte)(byteArray[i] | setB.byteArray[i]);
        return temp;
    }

    Bitset difference (Bitset setB)
    {
        Bitset temp = new Bitset (this);
        int nbyte = Math.min (byteArray.length, setB.byteArray.length);
        for (int i = 0; i < nbyte; i++)
            temp.byteArray[i] = (byte)(byteArray[i] & (setB.byteArray[i] ^ 255));
        return temp;
    }

    Bitset intersect (Bitset setB)
    {
        Bitset temp = new Bitset (Math.min (maxSize, setB.maxSize));
        int nbyte = Math.min (byteArray.length, setB.byteArray.length);
        for (int i = 0; i < nbyte; i++)
            temp.byteArray[i] = (byte)(byteArray[i] & setB.byteArray[i]);
        return temp;
    }

    boolean equals (Bitset setB)
    {
        int nbyte = Math.min (byteArray.length, setB.byteArray.length);
        for (int i = 0; i < nbyte; i++)
            if (byteArray[i] != setB.byteArray[i])
                return false;
        if (byteArray.length > nbyte)
            for (int i = nbyte; i < byteArray.length; i++)
              if (byteArray[i] != 0)
                  return false;

        if (setB.byteArray.length > nbyte)
            for (int i = nbyte; i < setB.byteArray.length; i++)
              if (setB.byteArray[i] != 0)
                  return false;
        return true;
    }

    boolean is_null()
    {
        for (int i = 0; i < byteArray.length; i++)
            if (byteArray[i] != 0) return false;
        return true;
    }

    void readSet(BufferedReader in) 
			  throws IOException  // reads ints from a line of input
    {  
        int i;
        String input = in.readLine();
        StringTokenizer s = new StringTokenizer (input);

        clear();
        while (s.hasMoreTokens())
        {   // now get the input: String nextToken => Integer => int
            i = Integer.parseInt(s.nextToken());
            if (i >= 0 && i < maxSize)  include (i);
        }
    }
	


    public String toString() 
    {  
       String str = "{  ";
       for (int i = 0 ; i < maxSize; i++) 
             if ( member(i)) str += i + "  ";
       return str + "}";
    }

    private void error (String msg)
    {
        System.out.print (" " + msg);
        System.exit(1);
    }
}
