//DequeueUnderFlowException.java
/******************************************************************
 *
 *This class throws a customized exception for extra credit...
 *@author Rebecca Dornin
 *****************************************************************/
public class DequeueUnderFlowException extends Exception {

   public DequeueUnderFlowException()
   {
      super( "The  Queue is empty" );
   }

} 