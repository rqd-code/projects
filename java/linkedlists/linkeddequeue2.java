//LinkedDequeue2.java
/******************************************************************
 *
 *This class sets up a LinkedDequeue and throws a customized exception for extra credit...
 * Some of these methods are borrowed from Savitch Text Book
 *@author Rebecca Dornin
 *****************************************************************/
public class LinkedDequeue2
{
    private QueueNode front;
    private QueueNode rear;


//SQueueNode.java
/******************************************************************
 *
 *This inner class intializes the item and link variables
 *
 *****************************************************************/	
    private class QueueNode
    {
        private Object item;
        private QueueNode link;

    /******************************************************************
      * This constructs a queuenode from scratch
      *****************************************************************/
        public QueueNode()
        {
            link = null;
            item = null;
        }
    /******************************************************************
      * This constructs a queuenode from scratch
      * @param newData this sets the value for the item
      * @param linkVaalue this sets the value for the link--rear,front, or null
      *****************************************************************/
        public QueueNode(Object newData, QueueNode linkValue)
        {
            item = newData;
            link = linkValue;
        }
    }
	

//LinkedDequeue2.java
/******************************************************************/

    public LinkedDequeue2()
    {
        front = null;
        rear = null;
    }

    /*****************************************
     *Returns the number of nodes in the list.
     *****************************************/
    public int size()
    {
        int count = 0;
        QueueNode position = front;
        while (position != null)
        {
            count++;
            position = position.link;
        }
        return count;
    }
	
    /*****************************************************************
     *Checks to see if list is empty
     *****************************************************************/
    public boolean isEmpty()
    {
	if ((rear == null) && (front == null))
	    {
		return true;
	    }
	else
	    {
		return false;
	    }
    }

    /*****************************************************************
     *Adds a node at the start of the list. The added node has addData
     *as its item. The added node will be the first node in the list.
	 *@ param addData this is the value for the item
     *****************************************************************/
    void headAdd(Object addData) 
    {	
      if ( isEmpty() )
	  {
         front = rear = new QueueNode( addData, null ); //initializing front and rear if empty
	  }
      else 
	  {
         front = new QueueNode( addData, front );
	  }
    }

    /*****************************************************************
     *Removes a node from the front, throws exception if list is empty
     *****************************************************************/
    Object headRemove() throws DequeueUnderFlowException
    {
        if (front != null)
        {
            front = front.link;
        }
        else
        {
            throw new DequeueUnderFlowException();
        }
		return front;
    }

    /*****************************************************************
     *Checks to see if list is empty
     *****************************************************************/
    Object headPeek() throws DequeueUnderFlowException
    {
		Object temp = null;
        if (isEmpty())
        {
 			throw new DequeueUnderFlowException();
        }
        else
        {
			temp = front.item;
        }
		return temp;
    }
    /******************************************************************
      * This adds a node to the tail
      * @param addData this sets the value for the item
      *****************************************************************/
    void tailAdd(Object addData) 
    {
	QueueNode temp = new QueueNode();
	temp.item = addData;
	temp.link = null;

	if (rear == null) 
		{
		front = rear = temp;
		}
	else
	    {
		rear.link = temp;
		rear = temp;
	    }
    }

	
    /*****************************************************************
     *removes node from tail
     *****************************************************************/	
     Object tailRemove()
    {
        if (rear == front)
        {

            front = rear = null;

        }
        else 
        {
			QueueNode current = front;
			while(current.link != rear)
			{
				current = current.link;

			}
				rear = current;
				current.link = null;

        }
			return rear;
    }
	
    /*****************************************************************
     *returns value of rear
     *****************************************************************/	
     Object tailPeek() throws DequeueUnderFlowException
    {

        if (isEmpty())
        {

            throw new DequeueUnderFlowException();
        }

		return rear.item;
    }

	
    /*****************************************************************
     *prints out all values in the list
     *****************************************************************/	
    public void print()
	
    {
        if (isEmpty())
		{
			System.out.println("Queue is Empty");
		}
		else
		{
		QueueNode position;
        position = front;
        while (position != null)
        {
            System.out.println(position.item);
            position = position.link;
        }
		}
    }

}