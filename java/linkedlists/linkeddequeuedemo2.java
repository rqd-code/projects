//LinkedDequeueDemo2.java
/******************************************************************
 *
 *This class test LinkedDequeue2 and throws a customized exception for extra credit...
 *@author Rebecca Dornin
 *****************************************************************/

public class LinkedDequeueDemo2
{
    public static void main(String[] args)
    {
      try {        
		LinkedDequeue2 list = new LinkedDequeue2();
		System.out.println("adding to head:");
        list.headAdd("One");
        list.print();
		System.out.println("adding to head:");
		list.headAdd("Two");
        list.print();
		System.out.println("adding to head:");
        list.headAdd("Three");
        list.print();
		System.out.println("adding to tail:");
        list.tailAdd("Four");
        list.print();
		System.out.println("adding to tail:");
        list.tailAdd("Five");
        list.print();
		
		System.out.println("Showing a head peek:");
        System.out.println(list.headPeek());
		
        System.out.println("List has " + list.size()
                            + " entries.");
        list.print();
        System.out.println("List has " + list.size()
                            + " entries.");
		System.out.println("Showing a tail peek:");
        System.out.println(list.tailPeek());
		System.out.println("removing from head:");
        list.headRemove();
        list.print();
        System.out.println("List has " + list.size()
                            + " entries.");

		System.out.println("removing from tail:");
        list.tailRemove();
        list.print();
		System.out.println("removing from head:");
        list.headRemove();
        list.print();
		System.out.println("removing from tail:");
        list.tailRemove();
        list.print();	
	        System.out.println("List has " + list.size()
                            + " entries.");
		System.out.println("removing from tail:");
        list.tailRemove();
        list.print();	
	        System.out.println("List has " + list.size()
                            + " entries.");

        System.out.println("Start of list:");
        list.print();
        System.out.println("End of list.");
		System.out.println("removing from head:");
        list.headRemove();
        list.print();
      }
      catch ( DequeueUnderFlowException e ) 
	  {
         e.printStackTrace();
      }
    }
}