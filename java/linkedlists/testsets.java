/**
*  TestSets.java
*
*  @version: Last Modified January 12, 2003
*  @author:  Henry Leitner
*/

import java.io.*;

public class TestSets
{
  static void menu()
  {
    System.out.println ();
    System.out.print ("Type 1 to CREATE SET A\n");
    System.out.print ("Type 2 to CREATE SET B\n");
    System.out.print ("Type 3 to CREATE INTERSECTION (A * B)\n");
    System.out.print ("Type 4 to CREATE UNION (A + B)\n");
    System.out.print ("Type 5 to CREATE DIFFERENCE (A - B)\n");
	System.out.print ("Type 6 to FIND CARDINALITY OF SET A\n");
	System.out.print ("Type 7 to FIND CARDINALITY OF SET B\n");
	System.out.print ("Type 8 to FIND OUT IF SETB IS SUBSET OF SETA\n");
	System.out.print ("Type 9 to FIND OUT IF SETA IS SUBSET OF SETB\n");
    System.out.print ("Type 10 to EXIT PROGRAM \n\n");
    System.out.print ("Command: ");
  }
  
  public static void main (String[] args) throws IOException
  {
    Bitset setA = new Bitset(16);
    Bitset setB = new Bitset(8);
    int command;
    
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
  
    do {
         menu();
         String s = in.readLine();
         command = Integer.parseInt(s);
         switch (command) 
         {
           case 1:
            System.out.println ("TYPE SOME SMALL INTEGERS each < 16" 
                                 + ", ALL ON ONE LINE ");
            setA.readSet(in);
            System.out.print ("     SET A = " + setA);
            break;

           case 2:
            System.out.println ("TYPE SOME SMALL INTEGERS each < 8"
                                 + ", ALL ON ONE LINE ");
            setB.readSet(in);
            System.out.print ("     SET B = " + setB);
            break;

           case 3:
            System.out.print ("     Intersection (A * B) = ");
            System.out.print (setA.intersect(setB));
            break;
           
		   case 4:
            System.out.print ("     Union (A + B) = ");
            System.out.print (setA.union(setB));
            break;

           case 5:
            System.out.print ("     Difference (A - B) = ");
            System.out.print (setA.difference(setB));
            break;

           case 6:
            System.out.print ("     CARDINALITY of SET A = ");
            System.out.print (setA.cardinality());
            break;
		
           case 7:
            System.out.print ("     CARDINALITY of SET B = ");
            System.out.print (setB.cardinality());
            break;
			
           case 8:
            System.out.print ("     IS SET B a SUBSET OF SET A = ");
            System.out.print (setA.subset(setB));
            break;
			
           case 9:
            System.out.print ("     IS SET A a SUBSET OF SET B = ");
            System.out.print (setB.subset(setA));
            break;
			
           default:  System.exit(0);
        
         }
       } while (command > 0 && command < 10);
  }
}
