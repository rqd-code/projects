import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

/**
 * This class sets up a swing window that displays the tic tac window
 *
 * @author Rebecca Dornin
 */
public class TicTac extends JFrame implements ActionListener
{

    public static final int WIDTH = 200;
    public static final int HEIGHT = 200;
	
    boolean begin = true;
    String error = "Error";
    String disp;
    String action;
    JTextField jt;
    JPanel buttonPanel;
    JButton but0,but1,but2,but3,but4,but5,but6,but7,but8;
    JButton startover;
    int toggle = 0;
    String b0 = "    ";
    String b1 = "    ";
    String b2 = "    ";
    String b3 = "    ";
    String b4 = "    ";
    String b5 = "    ";
    String b6 = "    ";
    String b7 = "    ";
    String b8 = "    ";
    String jt1 = "Click a square to place an X for tic tac toe...";
    String [] tics = {"X","O","X","O","X","O","X","O","X"};
	
    String [] storeVals = new String[9];

	

    public static void main(String[] args)
    {
	TicTac tc = new TicTac();
	tc.setVisible(true);
	tc.pack();	
    }	
	
    /**
       Constructs the frame
    */
    public TicTac()
    {
	setSize(WIDTH,HEIGHT);  
	setTitle("Tic Tac Toe");
	addWindowListener(new WindowDestroyer());
	getContentPane().setBackground(Color.gray);
	Container cp = getContentPane();
	cp.setLayout(new BorderLayout());
	JPanel buttonPanel = new JPanel();
	buttonPanel.setLayout(new GridLayout(3, 3));
	  
	but0 = new JButton(b0);
	but0.addActionListener(this);
	buttonPanel.add(but0);
	  
	but1 = new JButton(b1);
	but1.addActionListener(this);
	buttonPanel.add(but1);
	  
	 
	but2 = new JButton(b2);
	but2.addActionListener(this);
	buttonPanel.add(but2);

	but3 = new JButton(b3);
	but3.addActionListener(this);
	buttonPanel.add(but3);
	  
	but4 = new JButton(b4);
	but4.addActionListener(this);
	buttonPanel.add(but4);	  
	 
	but5 = new JButton(b5);
	but5.addActionListener(this);
	buttonPanel.add(but5);
	  
	but6 = new JButton(b6);
	but6.addActionListener(this);
	buttonPanel.add(but6);
	  
	but7 = new JButton(b7);
	but7.addActionListener(this);
	buttonPanel.add(but7);
	  
	but8 = new JButton(b8);
	but8.addActionListener(this);
	buttonPanel.add(but8);

	jt = new JTextField(jt1);
	jt.setEditable(false);

	startover = new JButton("Startover");
	startover.addActionListener(this);
	

	cp.add(jt, BorderLayout.NORTH);
	cp.add(buttonPanel, BorderLayout.CENTER);
	cp.add(startover, BorderLayout.SOUTH);
	addWindowListener(new WindowDestroyer());
    }

    public void check()
    {

	//We're doing the checking--its the magic square all over again...
	
	
	if ((storeVals[0] == "X" && storeVals[1] == "X" && storeVals[2] == "X") ||
	    (storeVals[3] == "X" && storeVals[4] == "X" && storeVals[5] == "X") ||
	    (storeVals[6] == "X" && storeVals[7] == "X" && storeVals[8] == "X") || 
	    (storeVals[0] == "X" && storeVals[3] == "X" && storeVals[6] == "X") || 
	    (storeVals[1] == "X" && storeVals[4] == "X" && storeVals[7] == "X") || 
	    (storeVals[2] == "X" && storeVals[5] == "X" && storeVals[8] == "X") || 
	    (storeVals[0] == "X" && storeVals[4] == "X" && storeVals[8] == "X") || 
	    (storeVals[2] == "X" && storeVals[4] == "X" && storeVals[6] == "X"))
	    {
		if (toggle > 3)
		    {
			jt.setText("X wins");
		    }
	    }	  
	
	else if ((storeVals[0] == "O" && storeVals[1] == "O" && storeVals[2] == "O") ||
		 (storeVals[3] == "O" && storeVals[4] == "O" && storeVals[5] == "O") ||
		 (storeVals[6] == "O" && storeVals[7] == "O" && storeVals[8] == "O") || 
		 (storeVals[0] == "O" && storeVals[3] == "O" && storeVals[6] == "O") || 
		 (storeVals[1] == "O" && storeVals[4] == "O" && storeVals[7] == "O") || 
		 (storeVals[2] == "O" && storeVals[5] == "O" && storeVals[8] == "O") || 
		 (storeVals[0] == "O" && storeVals[4] == "O" && storeVals[8] == "O") || 
		 (storeVals[2] == "O" && storeVals[4] == "O" && storeVals[6] == "O"))
	    {
		if (toggle > 3)
		    {
			jt.setText("O wins");
		    }
	    }	
	else
	    {
		if (toggle == 8)
		    {
			jt.setText("It's a tie!");
		    }
	    }		
    }

    public void actionPerformed(ActionEvent e) 
    {
	if (e.getSource()==startover)
	    {
		toggle = 0;
		but0.setText("    ");
		but1.setText("    ");
		but2.setText("    ");
		but3.setText("    ");
		but4.setText("    ");
		but5.setText("    ");
		but6.setText("    ");
		but7.setText("    ");
		but8.setText("    ");
		jt.setText(jt1);

		for(int i = 0; i < storeVals.length; i++)
		    {
			storeVals[i] = "";//tracing variables
		    }		
		
	    }
	if (toggle < 9)
	    {
				
		System.out.println(toggle);//tracing variable
		if(e.getSource()==but0)
		    {
			if ((!but0.getText().equals("X")) && (!but0.getText().equals("O")))
			    {
				but0.setText(tics[toggle]);
				storeVals [0] = tics[toggle];
				check();
				toggle++;
			    }
		    }
		else if(e.getSource()==but1)
		    {
			if ((!but1.getText().equals("X")) && (!but1.getText().equals("O")))
			    {
				but1.setText(tics[toggle]);
				storeVals [1] = tics[toggle];
				check();
				toggle++;
			    }
		    }
		else if(e.getSource()==but2)
		    {
			if ((!but2.getText().equals("X")) && (!but2.getText().equals("O")))
			    {
				but2.setText(tics[toggle]);
				storeVals [2] = tics[toggle];
				check();
				toggle++;
			    }
		    }
		else if(e.getSource()==but3)
		    {
			if ((!but3.getText().equals("X")) && (!but3.getText().equals("O")))
			    {
				but3.setText(tics[toggle]);
				storeVals [3] = tics[toggle];
				check();
				toggle++;
			    }
		    }
		else if(e.getSource()==but4)
		    {
			if ((!but4.getText().equals("X")) && (!but4.getText().equals("O")))
			    {
				but4.setText(tics[toggle]);
				storeVals [4] = tics[toggle];
				check();
				toggle++;
			    }
		    }
		else if(e.getSource()==but5)
		    {
			if ((!but5.getText().equals("X")) && (!but5.getText().equals("O")))
			    {
				but5.setText(tics[toggle]);
				storeVals [5] = tics[toggle];
				check();
				toggle++;
			    }
		    }
		else if(e.getSource()==but6)
		    {
			if ((!but5.getText().equals("X")) && (!but6.getText().equals("O")))
			    {
				but6.setText(tics[toggle]);
				storeVals [6] = tics[toggle];
				check();
				toggle++;
			    }
		    }
		else if(e.getSource()==but7)
		    {
			if ((!but7.getText().equals("X")) && (!but7.getText().equals("O")))
			    {
				but7.setText(tics[toggle]);
				storeVals [7] = tics[toggle];
				check();
				toggle++;
			    }
		    }
		else if(e.getSource()==but8)
		    {
			if ((!but8.getText().equals("X")) && (!but8.getText().equals("O")))
			    {
				but8.setText(tics[toggle]);
				storeVals [8] = tics[toggle];
				check();
				toggle++;
			    }
		    }
		else
		    {
		    }
	    }
	else
	    {
		for(int i = 0; i < storeVals.length; i++)
		    {
			System.out.println(storeVals[i] + " " + i);//tracing variables
		    }
	    }
    }
    
}
