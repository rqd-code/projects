/*
** TABS.js - Library Javascript Library
** rdornin
**
*/

(function($){

var TABS = {
      
    /*
     * Adds A-Z headers to the company sections, extra checks have been added for when the letters might be missing
     */ 
     

   makeAZ: function(a,b,c) {
 	var listitem = a;
 	var classval = b;
 	var counter = c;
 	if (classval == "start") {
 	    classval = "";
 	} else {
 	    classval = b.charAt(0).toUpperCase();
 	}
	//console.info(classval + '    ' + listitem);
	var header = "";
	if((classval == "") &&    (   (listitem.charAt(0).toUpperCase() == "A")  ||  (listitem.charAt(0).toUpperCase() == "B")   ||  (listitem.charAt(0).toUpperCase() == "C")    ||   (  /[0-9]/.test(listitem.charAt(0))       )   )   ) {
		header = '<li class="hbs-entrepreneurs-header"><div class="header-alpha"><a href="javascript:void(0);" class="closed" onClick="javascript:TABS.flipper(\'A-D\',\'bycompany\');">A-D</a> <small class="login">(99)</small></div></li>';
	}
	if((classval == "C") &&   (   (listitem.charAt(0).toUpperCase() == "E")  ||  (listitem.charAt(0).toUpperCase() == "F") ||  (listitem.charAt(0).toUpperCase() == "G")) ) {
		header = '<li class="hbs-entrepreneurs-header"><div class="header-alpha"><a href="javascript:void(0);" class="closed" onClick="javascript:TABS.flipper(\'E-I\',\'bycompany\');">E-I</a> <small class="login">(73)</small></div></li>';
	}
	if((classval == "D") &&   (   (listitem.charAt(0).toUpperCase() == "E")  ||  (listitem.charAt(0).toUpperCase() == "F") ||  (listitem.charAt(0).toUpperCase() == "G")) ) {
		header = '<li class="hbs-entrepreneurs-header"><div class="header-alpha"><a href="javascript:void(0);" class="closed" onClick="javascript:TABS.flipper(\'E-I\',\'bycompany\');">E-I</a> <small class="login">(73)</small></div></li>';
	}
	if((classval == "H") &&   (   (listitem.charAt(0).toUpperCase() == "K")  ||  (listitem.charAt(0).toUpperCase() == "L") ||  (listitem.charAt(0).toUpperCase() == "M") ) ) {
		header = '<li class="hbs-entrepreneurs-header"><div class="header-alpha"><a href="javascript:void(0);" class="closed" onClick="javascript:TABS.flipper(\'J-O\',\'bycompany\');">J-O</a> <small class="login">(51)</small></div></li>';
	}
	if((classval == "I") &&   (  (listitem.charAt(0).toUpperCase() == "J")  || (listitem.charAt(0).toUpperCase() == "K")  ||  (listitem.charAt(0).toUpperCase() == "L") ||  (listitem.charAt(0).toUpperCase() == "M") ) ) {
		header = '<li class="hbs-entrepreneurs-header"><div class="header-alpha"><a href="javascript:void(0);" class="closed" onClick="javascript:TABS.flipper(\'J-O\',\'bycompany\');">J-O</a> <small class="login">(51)</small></div></li>';
	}
	if((classval == "I") &&   (   (listitem.charAt(0).toUpperCase() == "K")  ||  (listitem.charAt(0).toUpperCase() == "L") ||  (listitem.charAt(0).toUpperCase() == "M") ) ) {
		console.info(classval);
		header = '<li class="hbs-entrepreneurs-header"><div class="header-alpha"><a href="javascript:void(0);" class="closed" onClick="javascript:TABS.flipper(\'J-O\',\'bycompany\');">J-O</a> <small class="login">(51)</small></div></li>';
	}
	if((classval == "N") &&   (   (listitem.charAt(0).toUpperCase() == "P")  ||  (listitem.charAt(0).toUpperCase() == "Q") ) ) {
		header = '<li class="hbs-entrepreneurs-header"><div class="header-alpha"><a href="javascript:void(0);" class="closed" onClick="javascript:TABS.flipper(\'P-S\',\'bycompany\');">P-S</a> <small class="login">(64)</small></div></li>';
	}
	if((classval == "O") &&   (   (listitem.charAt(0).toUpperCase() == "P")  ||  (listitem.charAt(0).toUpperCase() == "Q") ) ) {
		header = '<li class="hbs-entrepreneurs-header"><div class="header-alpha"><a href="javascript:void(0);" class="closed" onClick="javascript:TABS.flipper(\'P-S\',\'bycompany\');">P-S</a> <small class="login">(64)</small></div></li>';
	}
	if((classval == "R") &&   (   (listitem.charAt(0).toUpperCase() == "T")  ||  (listitem.charAt(0).toUpperCase() == "U") ) ) {
		header = '<li class="hbs-entrepreneurs-header"><div class="header-alpha"><a href="javascript:void(0);" class="closed" onClick="javascript:TABS.flipper(\'T-Z\',\'bycompany\');">T-Z</a> <small class="login">(56)</small></div></li>';
	}
	if((classval == "S") &&   (   (listitem.charAt(0).toUpperCase() == "T")  ||  (listitem.charAt(0).toUpperCase() == "U") ) ) {
		header = '<li class="hbs-entrepreneurs-header"><div class="header-alpha"><a href="javascript:void(0);" class="closed" onClick="javascript:TABS.flipper(\'T-Z\',\'bycompany\');">T-Z</a> <small class="login">(56)</small></div></li>';
	}
	return header;
    },
    
    /*
     * Creates the list
     */ 
	 
    createLists: function() {
        //console.time("createLists")
      	//console.time("gather")
	var data = new Array;
      	$("#byname li.comp").each(function() {     	
      		var personname = $(this).children('span:first').text();
      		var video = "";
      		if($(this).children('a.video').attr('href')) {
			video = '<a class="video" href="'+ $(this).children('a.video').attr('href')+'" onClick="javascript:Core.new_window(this);return false;">video</a>';      				
      		}  
      		var content = "";
      		$(this).children('.company').each(function() {
			mycomp = $(this).text();
			var mylink = "";
			var mylinkend = "";
	      		if($(this).attr('href')) {
				mylink = '<a href="'+ $(this).attr('href')+'" class="ext" onClick="javascript:Core.new_window(this);return false;">';      				
	      			mylinkend = '</a>';
	      		}      							
      			var cleancomp =  TABS.cleanData(mycomp);
      			content = '<p>'+personname+video+'</p>';
			if (data[cleancomp]) {
				storage = data[cleancomp]
				content = storage[1] + content;
			} 
			data[cleancomp] = [mylink+mycomp+mylinkend,content];	
		});   			      		
      	});
	//console.timeEnd("gather")
      	//console.time("sortcomps")
      	data = TABS.sortAssoc(data);
      	//console.timeEnd("sortcomps")
	
	//console.time("draw")
	var myfinalcontent = "";
	var header = "";
	var classval = "start";
	var counter = 0;
      	for (var i in data) {
      		var save = data[i];
      		header = TABS.makeAZ(i,classval,counter);
      		if (header != "") {
      			counter=0;
      		}
		myfinalcontent += header + '<li class="comp" title="'+i+'">\n<span class="toplist">'+save[0]+'</span>\n<div>\n'+save[1]+'</div>\n</li>\n';
		classval = i;
		counter++;
	}
	myfinalcontent = '<ul id="bycompany" class="entrepreneurs">'+myfinalcontent+'</ul>';
      	$('#bycompany').replaceWith(myfinalcontent);
        //TABS.hideLists();
     	//console.timeEnd("draw")
      	//console.timeEnd("createLists")      	
    },
    
   
    /*
     * Sorts the hash
     */ 
     
    sortAssoc: function(aInput){		
	var aTemp = [];
	for (var sKey in aInput) {
		aTemp.push(sKey);
	}
	aTemp.sort();
	var aOutput = [];
	for(j=0;j<aTemp.length;j++) {
		aOutput[aTemp[j]] = aInput[aTemp[j]];
	}
	return aOutput;		
   },
   
   
    /*
     * Creates a clean version of the hash key value
     */ 
     
    
   cleanData: function(a) {
    	comp = a;
	comp = comp.replace(/[^a-zA-Z 0-9]+/g,'');
	comp = comp.replace(/ /g,'');
	comp = comp.toUpperCase();
	return comp;
   },

    /*
     *  Toggles the menus using onclick attribute
     */    

   flipper: function(a,b) {    	
   	var mytext = a;
   	var section = b;
	    $("ul#"+section+" li.hbs-entrepreneurs-header a").each(function() {
		var mylink = $(this);
		var newtext = $(this).text().substring(0,3);
		var section = "";
   		//console.info(newtext + '   ' + mytext);
		//console.info(mylink.attr('class'));
		mylink.removeClass('ext');
		if(mylink.attr('class') == "open" && (newtext == mytext)) {
				mylink.removeClass('open');
				mylink.addClass('closed');
		} else if(mylink.attr('class') == "closed" && (newtext == mytext)) {
				mylink.removeClass('closed');
				mylink.addClass('open');
		}
	        //return false;
             });      
	TABS.checkSection(section,mytext)
	return false;
   },
   
   
    /*
     *  Checks to see witch lists should be toggled
     */ 
     
   checkSection: function(a,b) {
   	newtext = b;
   	newsection = a;
   	//console.info(newtext.substring(0,2));
	if(newtext.substring(0,3) == "A-D") {	
		TABS.flipAD(newsection);
	}
	if(newtext.substring(0,3) == "E-I") {
		TABS.flipEI(newsection);
	}	
	if(newtext.substring(0,3) == "J-O") {
		TABS.flipJO(newsection);
	}
	if(newtext.substring(0,3) == "P-S") {
		TABS.flipPS(newsection);
	}
	if(newtext.substring(0,3) == "T-Z") {
		TABS.flipTZ(newsection);
	}
	return false;
   },
   
    /*
     * These functions Toggle the lists
     */ 
     
   flipAD: function(s) {
   	section = s;
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='0']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='1']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='2']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='3']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='4']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='5']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='6']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='7']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='8']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='9']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='A']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='B']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='C']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='D']").toggle();
	return false;
   },
   
   flipEI: function(s) {
   	section = s;
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='E']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='F']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='G']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='H']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='I']").toggle();
	return false;
   },
 
   flipJO: function(s) {
   	section = s;
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='J']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='K']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='L']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='M']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='N']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='O']").toggle();
	return false;
   },
   
   flipPS: function(s) {
   	section = s;
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='P']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='Q']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='R']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='S']").toggle();
	return false;
   },
   
   flipTZ: function(s) {
   	section = s;
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='T']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='U']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='V']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='W']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='X']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='Y']").toggle();
	$("body#home div#container div#content div#hbs-entrepreneurs div.description ul#"+section+" li[title^='Z']").toggle();
	return false;
   },      
  
   last:''
  
}

window.TABS = TABS;

//$(document).ready(function(){TABS.ondomready();});

})(jQuery);
