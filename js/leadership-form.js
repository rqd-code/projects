	        jQuery(function($) {

	        	var total = Number(QUIZ.checkTotal()/1);

	        	$("div[id^='readings']").hide();

	        	$("p#again").hide();

	        	console.info(total);

	        	if (total > 0 ) {

	      	    	$("#start-quiz").hide();

	      			$("#start-directions").hide();

	   				showBooks(total);

	   				$("p#again").show();

	        	}

	        	$("#start-quiz a.more").click(function() {

	        		count = 0;

	   				$("#formtext input:radio[name^='ans']:checked").each(function(){

	   					//make sure at least one is selected

	   					console.info(count);

	   					count++;

	   				});

	   				if(count < 6) {

	   					console.info(count);

	   					$('.AnswerGroupMessage').text("Please select at least one number for each question.");

	   				} else {

	   					console.info(count);

	   					processQuiz();

	   					$('.AnswerGroupMessage').text("");

	   				}

	   				return false;

	        	});

	        	$("p#again a").click(function() {

	        		reset();

	   				return false;

	        	});

	        	function processQuiz() {

	        		total = Number(QUIZ.checkTotal()/1);

	        		console.info("start: " + total);

	        		$("#start-quiz").hide();

	        		$("#start-directions").hide();

	        		if (total == 0 ) {

	        			$("#start-quiz #formtext input[name^='ans']:checked").each(function() {

	        				total = total + (Number($(this).val()));

	        			});

	        		}

	        		total = (total/6);

	        		console.info("rounded: " + total);

	   				showBooks(total);

	        		$("p#again").show();

	        		set_name(total);

	        		total = 0;

	   				return false;

	   			}

	        	function showBooks(a) {

	        		total = a;

	        		if ( total <= 2 && total >= 1) {

	        			$('#readings-1').show();

	        		} else if (total > 2 && total < 4) {

	        			$('#readings-2').show();

	        		} else if (total >= 4 ) {

	        			$('#readings-3').show();

	        		}

	        		return false;

	        	}



	        	function reset() {

	        		$("div[id^='readings']").hide();

	        		$("p#again").hide();

	        		$("#start-quiz").show();

	        		$("#start-directions").show();

	   			    QUIZ.EraseCookie("count");

	   			    document.quiz.reset()

	   				return false;

	        	}

	        });
