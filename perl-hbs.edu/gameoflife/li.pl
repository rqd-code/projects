#!/usr/bin/perl

use strict;
use warnings;
use diagnostics;

#variable for initial file reading and grid storage
my @array;
my @array2;
my (@md1,@md2,@md3);
my (@nr1,@nr2,@nr3);
my ($a,$b,$c);

my @gridNow;#current grid value to create evolution from
my @empty = ();#for resetting array values
my $quit = "n";
my $ans;#value for tracking user input choice selection
my $vr; #value for variable array size storing user input
my $vc;#value for variable array column size
my $counter = 0;

my $temp; #temp row size for variable array
my $temp2;# temp column size for variable array
my $temp3;
my $RWS;#number of rows 
my $COLS;#$COLS being the variable cols size
my $ITLIVES = 'X';#constant value
my $FLATLINER  = '-';#constant value
my ($colsize1,$colsize2,$colsize3); #values for column sizes
my $ncnt = 0; #value for neighbor count needs to be initialized to zero
my ($diagRow,$diagCol);

#Got this method from 
#http://www.experts-exchange.com/Programming/Programming_Languages/Perl/Q_20942105.html
#http://perlmonks.thepen.com/47236.html
open (INPUT, "life.txt") or die "Can't open data file: $!";
{local $/= ''; @array = <INPUT>; }#using blank line as delimiter in the file
close INPUT;

foreach my $b (@array){
  #stripping blank lines from grid file
  chop($b);
  chomp($b);
  push(@array2,$b);
}

#storing array values in text files for easy splitting later
open(OUTA,">outa.txt");
print OUTA $array2[0];
close OUTA;          

open(OUTB,">outb.txt");
print OUTB $array2[1];
close OUTB;

open(OUTC,">outc.txt");
print OUTC $array2[2];
close OUTC;

#these while loops are from
#http://perl.about.com/library/weekly/aa062702a.htm
my $recorda = "";
open(IN, "outa.txt") or die "Couldn't open outa.txt";
while( <IN> ) {
    $recorda = $_;
    chomp($recorda);
    @nr1 = split(/ /, $recorda);
    $colsize1 = @nr1;
    push( @md1, [@nr1] );#storing array into multid
    }
close(IN);

my $recordb;
open(LISTB, "outb.txt") or die "Couldn't open outb.txt";
while( <LISTB> ) {
    $recordb = $_;
    chomp($recordb);
    @nr2 = split(/ /, $recordb);
    $colsize2 = @nr2;
    push( @md2, [@nr2] );#storing array into multid
    }
close(LISTB);


my $recordc;
open(LISTC, "outc.txt") or die "Couldn't open outc.txt";
while( <LISTC> ) {
    $recordc = $_;
    chomp($recordc);
    @nr3 = split(/ /, $recordc);
    $colsize3 = @nr3;
    push( @md3, [@nr3] );#storing array into multid
    }
close(LISTC);

while ($quit ne "q"){
    @gridNow = @empty;#resetting array
	
	print "\n\n*************************************\n";
    print "Get ready to play the game of life!\nThere are three first generations\nready for evolution.";
    print "\n*************************************\n";

    print "\nFirst enter the number of generations that you want to print out:\n";
    chomp($c = <STDIN>);
    
    print  "\nPick one:\n\n";
    
    print  "Choice (a) make 6 x 6 square\nChoice (b) make 5 x 5 square\nChoice (c) make 4 x 4 square\n\nChoice (v) Make a square of variable size w/ randomly generated population\n\nType (q) to quit program\n";
    print "\n*************************************\n";
    chomp($ans = <STDIN>);
    
    if($ans eq "a"){
	@gridNow = @md1;
	$COLS = $colsize1;
    }elsif($ans eq "b"){
	@gridNow = @md2;
	$COLS = $colsize2;
    }elsif($ans eq "c"){
	@gridNow = @md3;
	$COLS = $colsize3;
    }elsif($ans eq "v"){
	print "\nType in the ROW size of the variable generation\n";
	chomp($vr=<STDIN>);
	print "\nType in the COLUMN size of the variable generation\n";
	chomp($vc=<STDIN>);
	$COLS = $vc;
	$RWS = $vr;
	@gridNow =  makeV($vr,$vc);
    }elsif ($ans eq "q"){
	$quit = $ans;
	exit(0);
    } else {
	print "You entered a bad value. Try again\n";
    }
	
    #getting variable row size
    if ($ans eq "v"){
	$RWS = $vr;
    }else{
	$RWS = @gridNow;	
    }

###################################################   
#Continuos while loop that prints and makes evolutions
###################################################   
    
    for (my $bc = 0; $bc < $c; $bc++){
      if( checkforPulse( @gridNow ) != 1){
	my $temp = $bc + 1;#so generations print count start off at one
	print "\n-------------\nEvolution: $temp\n-------------\n";
	showArrays( @gridNow );
	@gridNow = evolve( @gridNow );	
      }else{$bc = $c;}#generation is dead, start over program
    }



#subroutines developed with help of this web site
#http://perl.about.com/library/weekly/aa011600a.htm
#and the course web site: webct mod 6

###################################################   
#Checking the whole array to see if it is alive
###################################################   

    sub checkforPulse { #function to test if whole array is dead or alive
      my @newgrid = @_;

      for ( my $rg = 0 ; $rg < $RWS; $rg++ ) {
	for ( my $cg = 0 ; $cg < $COLS; $cg++ ) {
	  if ( $newgrid[$rg][$cg] eq $ITLIVES ) {
	    return(0); #if one position is alive, whole array isn't dead
	  }
	}
      }#if no positions are alive, the grid is dead
      print "Generation is dead :(\n";
      return(1);
    }#end subroutine


###################################################   
#Make my dynamic array, variables created from user input
###################################################   
    my @varGen;#variable generation
    my $temp; #temp row size
    my $temp2;# temp column size
    sub makeV {#subroutine to make variable array, creates grid of variable row and col sizes
	($temp, $temp2) = @_;
	for ( my $r = 0 ; $r < $temp ; $r++) {
	    for ( my $c = 0 ; $c < $temp2 ; $c++ ) {
	      #learned use of rand from http://www.perldoc.com/perl5.8.0/pod/func/rand.html
		if (int(rand(10)) == 4 ) {#randomly populate the array with values
		    $varGen[$r][$c] = $ITLIVES;
		} else {
		    $varGen[$r][$c] = $FLATLINER;
		}
	    }
	} # initialize the $r by $c array
	return(@varGen);
    }

###################################################   
#Print out the latest array per generation
###################################################   
    sub showArrays {
	my @newg = ([]);#resetting new generation array for reuse
	@newg = @_;

	    for ( my $rb = 0 ; $rb < $RWS; $rb++ ) {
		for ( my $cb = 0 ; $cb < $COLS; $cb++) {
		    print "$newg[$rb][$cb] "; #printing out the generation
		}
		print "\n";
	    }
    }

###################################################   
#Count the num of neighbors
###################################################   
    sub ncount #counting the neighbors subroutine
      {
	my ($row, $col,@grid) = @_;
	my $nc = 0;#value for neighbor counting
	
	#checking values to see if they are out of bounds
	if ($row < $RWS ) {
	  $nc += notDead($grid[$row+1][$col]);  #directly above
	}
	if( $row > 0) {
	  $nc += notDead($grid[$row-1][$col]);  #directly below
	}
	if ($col > 0 ) {
	  $nc += notDead($grid[$row][$col-1]);  #directly left
	}
	if ($col < $COLS  ) {
	  $nc += notDead($grid[$row][$col+1]);  #directly right
	} 
	if ( (($row - 1) == -1 ) || (($col - 1) == -1)){
	}else {
	  $nc += notDead($grid[$row-1][$col-1]);#diagnol bot left
	  #print "diagnol bot left: $ncnt \n";
	}
	if ( (($row - 1) == -1 ) || (($col + 1) == $COLS)){
	}else {
	  $nc += notDead($grid[$row-1][$col+1]);#diagnol bot right
	  #print "diagnol bot right: $ncnt \n"; 		
	}
	if ( (($row + 1) ==  $RWS ) || (($col - 1) == -1)){
	}else{
	  $nc += notDead($grid[$row+1][$col-1]);#diagnol top left
	  #print "diagnol top left: $ncnt \n"; 		
	}
	if ( (($row + 1) == $RWS ) || (($col + 1) == $COLS)){
	}else{
	  $nc += notDead($grid[$row+1][$col+1]);#diagnol top right
	  #print "diagnol top right: $ncnt \n"; 
	}
	return $nc;
      }

###################################################   
#Make the next evolution
###################################################   
	
    sub evolve {#makes the next evolution of generation
      my @grid = @_;
      my @evolved;
      #$ncnt is the neighbor count,initialized at top of program
      
      for ( my $row = 0 ; $row < $RWS; $row++ ) {
	for ( my $col = 0 ; $col < $COLS; $col++ ) {
	  $ncnt = ncount($row,$col,@grid);
	  if ( notDead($grid[$row][$col]) == 1 ) {#if the cell is alive
	    if ( $ncnt == 2 || $ncnt == 3 ) { 
	      $evolved[$row][$col] = $ITLIVES;
	      #If position has 2 or 3 not dead neighbors,then this position is not dead
	    } else {
	      $evolved[$row][$col] = $FLATLINER;#otherwise its dead
	    }
	  } else {#if the cell is dead
	    if ( $ncnt != 3  ) { #and there aren't three neighbors
	      $evolved[$row][$col] = $FLATLINER;#its dead
	    } else {
	      $evolved[$row][$col] = $ITLIVES;#if dead square has three neighbors, it comes back from dead
	    }
	  }
	  $ncnt = 0;#resetting the neighbor count for the next position
	}
      }
      return(@evolved);
    }

###################################################   
#Check to see if not dead. Is it alive?
#Checking each individual position
###################################################   
    
    sub notDead { #function to test if each individual positions are not dead
      eval{#catching exception
	$temp3 = $_[0];
	if (!$temp3){#if not initialized for some reason
	  return(0);
	}else{
	  if ($temp3 eq $ITLIVES ) {
	    return (1); #it is alive
	  } else {
	    return (0); #position is dead
	  }
	} 
      };
    }

    
  }#end outer while loop of program
