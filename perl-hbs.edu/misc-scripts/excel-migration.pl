use strict;
use Win32::OLE qw(in with);
use Win32::OLE::Const 'Microsoft Excel';
use Win32::OLE::Variant;
use Time::Local;
#use Date;
#use Date::Manip;
#use Date::Parse
$Win32::OLE::Warn = 3;                                #die on errors...

#######################################3
#
#NOTE: To run this script you must open cygwin, 
#go to the directory where your script is and type
# perl create_files.pl enter_number_of_rows_from the excel spreadsheet
# for example: perl create_files 65
#
###############################

# get already active Excel application or open new
my $Excel = Win32::OLE->GetActiveObject('Excel.Application') || Win32::OLE->new('Excel.Application', 'Quit');  


# open Excel file
my $Book = $Excel->Workbooks->Open("c:/Users/Labuser/Desktop/perl-today/database.xls"); 

# You can dynamically obtain the number of worksheets, rows, and columns
# through the Excel OLE interface.  Excel's Visual Basic Editor has more
# information on the Excel OLE interface.  Here we just use the first
# worksheet, rows 1 through 4 and columns 1 through 3.
# select worksheet number 1 (you can also select a worksheet by name)

my $rows = $ARGV[0];

if(!$ARGV[0]) {
    print "Please remember to enter the number of rows in the excel spreadsheet. The format to run this script is perl create_files.pl enter_number_of_rows\n\n";
    exit;
}

#Name,address line 1,adress line 2,city	state,zip,country
#id,batch_id,processed,orderdate,cust_id,ship_fname,ship_lname,ship_email,ship_comp,ship_phone,ship_fax,ship_addr,ship_city,ship_state,ship_zip,ship_cntry,bill_fname,bill_lname,bill_email,bill_comp,bill_phone,bill_fax,bill_addr,bill_city,bill_state,bill_zip,bill_cntry,ship_id,ship_data,pay_id,pay_data,pay_secid,pay_seckey,pay_secdat,total
#id,pgrpcount,login,pw_email,password,ship_fname,ship_lname,ship_email,ship_comp,ship_phone,ship_fax,ship_addr,ship_city,ship_state,ship_zip,ship_cntry,bill_fname,bill_lname,bill_email,bill_comp,bill_phone,bill_fax,bill_addr,bill_city,bill_state,bill_zip,bill_cntry
#order_id,line_id,product_id,code,name,price,weight,taxable,upsold,quantity


#Name,address line 1,adress line 2,city	state,zip,country


my $Sheet = $Book->Worksheets(1);
my $temp = 1;
my $name;
my $fname;
my $lname;
my $addressone;
my $addresstwo;
my $city;
my $state;
my $zip;
my $country;
my $dateval;
my $email;
my $phone;
my $ends;
my $renew;
my $temp1 = $Sheet->Cells(2,2)->{'Value'};

##customers updated info : pgrpcount = 0
##orders updated == orderitems upsold = 1


open(OUTO, ">orders.csv") or die("Couldn't open output file: $!");
open(OUTC, ">customer.csv") or die("Couldn't open output file: $!");
open(OUTI, ">orderitems.csv") or die("Couldn't open output file: $!");
print OUTO "id,batch_id,processed,orderdate,cust_id,ship_fname,ship_lname,ship_email,ship_comp,ship_phone,ship_fax,ship_addr,ship_city,ship_state,ship_zip,ship_cntry,bill_fname,bill_lname,bill_email,bill_comp,bill_phone,bill_fax,bill_addr,bill_city,bill_state,bill_zip,bill_cntry,ship_id,ship_data,pay_id,pay_data,pay_secid,pay_seckey,pay_secdat,total\n";
print OUTC "id,pgrpcount,login,pw_email,password,ship_fname,ship_lname,ship_email,ship_comp,ship_phone,ship_fax,ship_addr,ship_city,ship_state,ship_zip,ship_cntry,bill_fname,bill_lname,bill_email,bill_comp,bill_phone,bill_fax,bill_addr,bill_city,bill_state,bill_zip,bill_cntry\n";

print OUTI "order_id,line_id,product_id,code,name,price,weight,taxable,upsold,quantity\n";
my $counter = 1;
for(my $cnt = 1;$cnt<=$rows;$cnt++){
	$lname = $Sheet->Cells($cnt,1)->{'Value'};
	$lname =~ s/\n//g;
	$fname = $Sheet->Cells($cnt,2)->{'Value'};
	$fname =~ s/\n//g;
	#($fname,$lname) = split(/\|/,$name);
	$addressone = $Sheet->Cells($cnt,3)->{'Value'};
	$addressone =~ s/\n//g;
	if ($addressone eq "") {
		$addressone = "";
	}
	$addresstwo = $Sheet->Cells($cnt,4)->{'Value'};
	$addresstwo =~ s/\n//g;
	$city = $Sheet->Cells($cnt,5)->{'Value'};
	$city =~ s/\n//g;
	$state = $Sheet->Cells($cnt,6)->{'Value'};
	$state =~ s/\n//g;
	$zip = $Sheet->Cells($cnt,7)->{'Value'};
	$zip =~ s/\n//g;
	$zip = "$zip";
	$country = $Sheet->Cells($cnt,9)->{'Value'};
	$country =~ s/\n//g;
    $dateval = $Sheet->Cells($cnt,56)->{'Value'};
	$dateval =~ s/\n//g;
	#my $m1 = 0;
	#my $y1 = 0;
	#my $d1 = 0;
	my ($m1,$d1,$y1) = split(/\//,$dateval);
	#$m1 = int($m1);
	#$d1 = int($d1);
	#$y1 = int($y1);
	$m1 =~ s/^0*//g;
	$d1 =~ s/^0*//g;
	$m1 = int($m1) - 1;
	$d1 = int($d1);
	if ($m1 < 0 ) { $m1 = 0; }
	if ($d1 < 1 or $d1 > 31 ) { $d1 = 1; }
	$dateval = timegm(0,0,0,$d1,$m1,$y1);
	my $datecheck = gmtime($dateval);
	#$dateval = POSIX::strftime("%S",$dateval);
	#$dateval = $m1 ."-". $d1. "-". $y1;
	#$dateval = $m1;
	$email = $Sheet->Cells($cnt,34)->{'Value'};
	$email =~ s/\n//g;
	my $id = int($Sheet->Cells($cnt,67)->{'Value'});
	$id =~ s/\n//g;
	my $phone = $Sheet->Cells($cnt,21)->{'Value'};
	$phone =~ s/\n//g;
	if ($phone =~ /\?/) { $phone = ""; }
	my $login = "glreview" . $id;
	my $password = "$zip";
	$password =~ s/\s+//g;
	my $ends = $Sheet->Cells($cnt,11)->{'Value'};
	$ends =~ s/\n//g;
	$renew = $Sheet->Cells($cnt,33)->{'Value'};
	$renew =~ s/\n//g;
	my ($volume,$issnum) = split(/\./,$ends);
	$password =~ s/(\d{5})\S+/$1/g;

	if ($password =~ /\s+/) {
		$password = "12345";
	}
	
	if ((int($volume)>=16 && int($issnum) >= 6) or ($volume > 16 && $issnum >= 1) or ($volume >= 99))  {
		$counter++;
		print $counter . "- $id - $password - $volume $issnum - ". $dateval . " - $datecheck\n";
	#id,batch_id,processed,orderdate,cust_id,ship_fname,ship_lname,ship_email,ship_comp,ship_phone,ship_fax,ship_addr,ship_city,ship_state,ship_zip,ship_cntry,bill_fname,bill_lname,bill_email,bill_comp,bill_phone,bill_fax,bill_addr,bill_city,bill_state,bill_zip,bill_cntry,ship_id,ship_data,pay_id,pay_data,pay_secid,pay_seckey,pay_secdat,total	
	
	#print OUTC "id,pgrpcount,login,pw_email,password,ship_fname,ship_lname,ship_email,ship_comp,ship_phone,ship_fax,ship_addr,ship_city,ship_state,ship_zip,ship_cntry,bill_fname,bill_lname,bill_email,bill_comp,bill_phone,bill_fax,bill_addr,bill_city,bill_state,bill_zip,bill_cntry\n";
	
	#print OUTI "order_id,line_id,product_id,code,name,price,weight,taxable,upsold,quantity,processed\n";
	
		print OUTO qq'$id,0,1,$dateval,$id,$fname,$lname,$email,$addresstwo,="$phone",,$addressone,$city,$state,="$zip",$country,$fname,$lname,$email,$addresstwo,="$phone",,$addressone,$city,$state,="$zip",$country,0,$ends,0,$renew,,,22.00\n';
		print OUTC qq'$id,0,$login,$email,="$password",$fname,$lname,$email,$addresstwo,="$phone",,$addressone,$city,$state,="$zip",$country,$fname,$lname,$email,$addresstwo,="$phone",,$addressone,$city,$state,="$zip",$country\n';
		print OUTI qq'$id,$cnt,1,OYS,One Year Subscription,22.00,0.00,0,1,1,1\n';
	}
}

close(OUTO);
close(OUTC);
close(OUTI);

$Book->Close;