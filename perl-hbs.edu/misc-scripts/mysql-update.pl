#!/usr/bin/perl
use DBI;
use Time::Local;
use strict;




#my $dbh = DBI->connect( "dbi:mysql:glreview:11.11.11.11", "username", "password", { RaiseError => 1, AutoCommit => 1 }) or &dienice("Can't connect to database: $DBI::errstr");

my $dbh = DBI->connect( "dbi:mysql:glreview:11.11.11.11", "username", "pasword", { RaiseError => 1, AutoCommit => 1 }) or &dienice("Can't connect to database: $DBI::errstr");


#UPDATE s01_OrderItems SET upsold='1', is_processed='1' where line_id < 29650

my $subendsval = $ARGV[0];

if(!$ARGV[0]) {
    print "Please remember to enter the current volume num. The format to run this script is perl create_files.pl enter_volume_num\n\n";
    exit;
}

#my $sql = "select * from subpages";
#my $sth = $dbh->prepare($sql);
#$sth->execute 
#or die "SQL Error: $DBI::errstr\n";

#opening tab seperated file of latest export of data
open(FH,"latest-data-for-script.txt") or &dienice("Can't open file: $!");
my @glrdb = <FH>;     # reads the ENTIRE FILE into array @b
close(FH);      # closes the file

open(OUTLOG,">updatelog.txt");
 
my $custUpdateSQL =  $dbh->prepare('UPDATE s01_Customers SET pw_email=?, ship_fname=?, ship_lname=?, ship_email=?, ship_comp=?, ship_phone=?, ship_addr=?, ship_city=?, ship_state=?, ship_zip=?, ship_cntry=?, bill_fname=?, bill_lname=?, bill_email=?, bill_comp=?, bill_phone=?,bill_addr=?, bill_city=?, bill_state=?, bill_zip=?, bill_cntry=? WHERE id=?');

my $ordersUpdateSQL = $dbh->prepare('Update s01_Orders SET orderdate=?,ship_fname=?, ship_lname=?, ship_email=?, ship_comp=?, ship_phone=?,ship_addr=?, ship_city=?, ship_state=?, ship_zip=?, ship_cntry=?, bill_fname=?, bill_lname=?, bill_email=?, bill_comp=?, bill_phone=?, bill_addr=?, bill_city=?, bill_state=?, bill_zip=?, bill_cntry=?, ship_data=? WHERE cust_id=?');

my $orderItemsUpdateSQL = $dbh->prepare("UPDATE s01_OrderItems SET upsold='1', is_processed='1' where order_id = ?");
my $custSelectAll = $dbh->prepare('Select * from s01_Customers where id = ?');
my $ordersSelectAll = $dbh->prepare('Select * from s01_Orders where id = ?');
my $orderItemsSelectAll = $dbh->prepare('Select * from s01_OrderItems where order_id = ?');
		


foreach my $line (@glrdb) {			
			my @ary = split '\t',$line;
			my $lname = $ary[0];
			my $fname = $ary[1];
			my $city = $ary[2];
			my $state = $ary[3];
			my $zip = $ary[4];
			my $phone = $ary[5];
			if ($phone =~ /\?/){ $phone = ""; }
			my $ends = $ary[7];
			my $email = $ary[8];
			my $serialnum = $ary[9];
			my $line_id = int($serialnum) + 600000; #to give unique line_id and keep SQL happy for OrderItems TBL
			my $dateval = $ary[10]; #datechanged field dd/mm/yyyy
			my $custid= $ary[12];
			my $addressone =  $ary[13]; #uses street 1, ship_comp
			my $addresstwo =  $ary[14]; #uses street 2, actual address
			my $name = $ary[15];
			my $country = $ary[16];	
			if ($country =~ /\s+/) { $country = "US";}	
			print "dateval:" . $dateval . "\n\n\n\n\n\n\n";
			#my $dateval =~ s/\n//g;
		        my $login = "glreview" . $serialnum;
			my $password = $zip;
			$password =~ s/\s+//g;       
			$password =~ s/(\d{5})\S+/$1/g; #getting first five vals of zip for foreign zips
			if ($password =~ /\s+/) { #if no password or zip
					$password = "12345";
			}
			my ($m1,$d1,$y1) = split(/\//,$dateval);
			print "dateval:" . $dateval . "\n\n\n\n\n\n\n";
			#$m1 =~ s/^0*//g;
			#$d1 =~ s/^0*//g;
			$m1 = int($m1) - 1;
			$d1 = int($d1);
			if ($m1 < 0 ) { $m1 = 0; }
			if ($d1 < 1 or $d1 > 31 ) { $d1 = 1; }
			$dateval = timegm(0,0,0,$d1,$m1,$y1); #converts to unix time woo hoo :)
                        print $m1 . $d1 . $y1 . "  "  . $dateval .  "\n\n";
		if (int($ends) >= int($subendsval) && int($serialnum) > 900) {	#ends must be greater then current volume
			if ($custid > 200000 ) {
				    #make sure the cust id actually exists, in case entered wrong
					$custSelectAll->execute($custid);
					if ($custSelectAll->rows > 0) {
						print $name . " custid greater " . $custid . "\n\n\n";
						print OUTLOG $name . " custid greater " . $custid . "\n";;
	
						$ordersUpdateSQL->execute($dateval,$fname,$lname,$email,$addressone,$phone,$addresstwo,$city,$state,$zip,$country,$fname,$lname,$email,$addressone,$phone,$addresstwo,$city,$state,$zip,$country,$ends,$custid) # Execute the query
						or die "Couldn't execute statement: " . $ordersUpdateSQL->errstr;
						
						$custUpdateSQL->execute($email,$fname,$lname,$email,$addressone,$phone,$addresstwo,$city,$state,$zip,$country,$fname,$lname,$email,$addressone,$phone,$addresstwo,$city,$state,$zip,$country,$custid) # Execute the query
						or die "Couldn't execute statement: " . $custUpdateSQL->errstr;
						
						#$orderItemsUpdateSQL->execute($custid) # Execute the query
						#or die "Couldn't execute statement: " . $orderItemsUpdateSQL->errstr;
					} else {
						print "error: custid " . $custid . "  xx " . $serialnum. " greater then 200000 does not exist\n";
						print OUTLOG "error: custid " . $custid . "  xx " . $serialnum. " greater then 200000 does not exist\n";

					}
			}	
			####################
			##END CUSTID CHECK, Looking for prexisting customers with custID < 220000##
			####################
			else {
					$custSelectAll->execute($serialnum);
			  		$ordersSelectAll->execute($serialnum);
			 		$orderItemsSelectAll->execute($serialnum);
					print $custSelectAll->rows . " < custsqlresult ";			
					####################
					##if customer does not exist in all three tables, then insert
					####################
					if($custSelectAll->rows == 0 && $ordersSelectAll->rows == 0 && $orderItemsSelectAll->rows ==0) {					  
			 			my $insertCust = $dbh->prepare('INSERT INTO s01_Customers (id, pgrpcount, login, pw_email, password, ship_fname, ship_lname, ship_email, ship_comp, ship_phone, ship_addr, ship_city, ship_state, ship_zip, ship_cntry, bill_fname, bill_lname, bill_email, bill_comp, bill_phone, bill_addr, bill_city, bill_state, bill_zip, bill_cntry) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');

						my $insertOrders = $dbh->prepare('INSERT INTO s01_Orders (id, batch_id, orderdate, cust_id, ship_fname, ship_lname, ship_email, ship_comp, ship_phone, ship_addr, ship_city, ship_state, ship_zip, ship_cntry, bill_fname, bill_lname, bill_email, bill_comp, bill_phone, bill_addr, bill_city, bill_state, bill_zip, bill_cntry, ship_id, ship_data) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');

						my $insertOI = $dbh->prepare('INSERT INTO s01_OrderItems (order_id, line_id, product_id, code, name, price, weight, taxable, upsold, is_processed,quantity) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)');

						$insertOrders->execute($serialnum,'0',$dateval,$serialnum,$fname,$lname,$email,$addressone,$phone,$addresstwo,$city,$state,$zip,$country,$fname,$lname,$email,$addressone,$phone,$addresstwo,$city,$state,$zip,$country,'0',$ends) 
						or die "Couldn't execute statement: " . $insertOrders->errstr;

						$insertCust->execute($serialnum,'0',$login,$email,$password,$fname,$lname,$email,$addressone,$phone,$addresstwo,$city,$state,$zip,$country,$fname,$lname,$email,$addressone,$phone,$addresstwo,$city,$state,$zip,$country)
						or die "Couldn't execute statement: " . $insertCust->errstr;

						
						#inserting mostly dummy data just to make sql happy with FK constraints
						#upsold must 1 as well as is_processed to keep from showing in new subscribers list
						$insertOI->execute($serialnum,$line_id,'1','OYS','One Year Subscription','22.00','0.00','0','1','1','1')
						or die "Couldn't execute statement: " . $insertOI->errstr;


						print $country . " " . $serialnum . "adding  cool" . "\n";
						print OUTLOG $country . " " . $serialnum . "adding  cool" . "\n";
					} else {			
						####################
						##if customer does exist then update appropriate data
						####################
						print "exists already $serialnum xx am updating\n";	
						print OUTLOG "exists already $serialnum xx am updating\n";	   
			 			
						$ordersUpdateSQL->execute($dateval,$fname,$lname,$email,$addressone,$phone,$addresstwo,$city,$state,$zip,$country,$fname,$lname,$email,$addressone,$phone,$addresstwo,$city,$state,$zip,$country,$ends,$serialnum) # Execute the query
						or die "Couldn't execute statement: " . $ordersUpdateSQL->errstr;
			 			
						$custUpdateSQL->execute($email,$fname,$lname,$email,$addressone,$phone,$addresstwo,$city,$state,$zip,$country,$fname,$lname,$email,$addressone,$phone,$addresstwo,$city,$state,$zip,$country,$serialnum) # Execute the query
			 			or die "Couldn't execute statement: " . $custUpdateSQL->errstr;

						$orderItemsUpdateSQL->execute($serialnum) # Execute the query
						or die "Couldn't execute statement: " . $orderItemsUpdateSQL->errstr;
					}
				}
		}  else { #end subends check, must greater then 18 to update or insert to prevent old stuff from getting in
			print $serialnum . "  " . int($ends) ."  ". int($subendsval) . " subends less then current vol\n\n";
			print OUTLOG $serialnum . "  " . int($ends) ."  ". int($subendsval) . " subends less then current vol\n\n";
		}			 
} #end loop of udpdate file data

my $ordersTestSQL = $dbh->prepare('Select orderdate, ship_comp, ship_address, bill_comp, bill_address from s01_Orders');
#my $orderItemsSelectAll = $dbh->prepare('Select * from s01_OrderItems where order_id = ?');
$ordersTestSQL->execute();

while (my($orderdate,$ship_comp,$ship_address,$bill_comp,$bill_address) = $ordersTestSQL->fetchrow_array) {
   print qq($orderdate  $ship_comp  $ship_address   $bill_comp    $bill_address  \n);
}

$ordersUpdateSQL->finish;
$custUpdateSQL->finish;
#$dbh->disconnect;
close(OUTLOG);
sub dienice {
    my($msg) = @_;
    print "Error\n";
    print $msg;
    exit;
}
sub dbdie {
    my($package, $filename, $line) = caller;
    my($errmsg) = "Database error: $DBI::errstr<br>
    called from $package $filename line $line";
    &dienice($errmsg);
}
