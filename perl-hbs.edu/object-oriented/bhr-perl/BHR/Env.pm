#!/usr/local/bin/perl

#
# Module to set the environment variables
#

use strict;
use warnings FATAL => 'all';
use Cwd;

# default group write on all files created
umask 2;

my $cwd = getcwd;

package Env;
use vars qw($DOCROOT $CGIROOT);

if ($cwd =~ m|/dev/|) {
    $ENV{DOCROOT} = "/san/dev/htdocs/wwwhbs/bhr";
    $ENV{CGIROOT} = "/san/dev/cgi-bin/wwwhbs/bhr";
} else {
    $ENV{DOCROOT} = "/san/stage/htdocs/wwwhbs/bhr";
    $ENV{CGIROOT} = "/san/stage/cgi-bin/wwwhbs/bhr";
}

1;
