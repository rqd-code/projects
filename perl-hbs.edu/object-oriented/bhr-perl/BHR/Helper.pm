#!/usr/local/bin/perl

#
# Module to set the environment variables
#

use strict;
use warnings FATAL => 'all';

package Helper;

sub trim {
    my $s = shift;
    if ($s) {
        $s =~ s/^\s+//;
        $s =~ s/\s+$//;
    }
    return $s; 
}

sub slurp {
    my $file = shift;
    {
        local $/;
        open( SLURP, "<:encoding(iso-8859-1)", $file) || die "$file $!";
        my $txt = <SLURP>;
        close SLURP or die "$file $!";
        return $txt;
    }
}

sub write_file {
    my $file = shift;
    my $html = shift;
    open(FH,">$file") || die "$! $file";
    binmode( FH,  ':encoding(iso-8859-1)' );
    print FH $html;
    close FH;
}

# evaluate serverside includes
sub ssi {
    my $html = shift;
    
    # doesn't die if file is missing
    
    sub ssi_slurp {
        my $file = shift;
        if (-e $file) { return slurp($file);} 
        else { return "File missing $file";}
    }

    foreach (1..4) {
          $html =~ s#<!--\#include virtual="([^"]+)"\s*-->#ssi_slurp("$ENV{DOCROOT}/../$1")#eig;
    }
    
    return $html;
}

sub headers {
     my $head = Helper::ssi(Helper::slurp("$ENV{DOCROOT}/ssi/standard.head.html"));
     my $header = Helper::ssi(Helper::slurp("$ENV{DOCROOT}/ssi/standard.header.html"));
     my $code = qq|<script type="text/javascript">
                // <![CDATA[
                        \$(document).ready(function(){
                                  \$("tr.specialiss").hide()\;
                                  \$("tr.article").hide()\;
                                  \$("tr.review").hide()\;
                                  if (\$("\.is_special option:selected").text() == "Yes") {
                                          \$("tr.specialiss").show()\;
                                  }
                                  if (\$("\.type option:selected").text() == "Article") {
                                          \$("tr.article").show()\;
                                  }
                                  if (\$("\.type option:selected").text() == "Review") {
                                          \$("tr.review").show()\;
                                  }
                                  \$("\.is_special")\.change(function() { 
                                          if (\$("\.is_special option:selected").text() == "Yes") {
                                                  \$("tr.specialiss").show()\;
                                          } 
                                          if (\$("\.is_special option:selected").text() == "No") {
                                                  \$("tr.specialiss").hide()\;
                                          }
                                  \})\;
                                  \$("\.type")\.change(function() { 
                                          if (\$("\.type option:selected").text() == "Review") {
                                                  \$("tr.article").hide()\;
                                                  \$("tr.review").show()\;
                                          } 
                                          if (\$("\.type option:selected").text() == "Article") {
                                                  \$("tr.article").show()\;
                                                  \$("tr.review").hide()\;
                                          }
                                  \})\;
                        \})\;
                // ]]>
        </script>|;
     my $html = qq|<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<head>
    <title>BHR Admin - Harvard Business School</title>
        
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="/bhr/css/core.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/bhr/css/bhr.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/admin/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/bhr/css/print.css" type="text/css" media="print" />
    <script type="text/javascript" src="http://www.hbs.edu/shared/js/jquery/1.2.3/jquery.js"></script>
    <script type="text/javascript" src="/bhr/js/core.js"></script>
     <script type="text/javascript" src="/bhr/js/bhr.js"></script>
    <script type="text/javascript" src="http://www.hbs.edu/js/analytics.js"></script>
    <script type="text/javascript">analytics.settings({profile:'bhr'})</script>
    <!--[if lte IE 7]>
       <link rel="stylesheet" href="/bhr/css/ie.css" type="text/css" />
    <![endif]-->
    <!--[if lte IE 6]>
       <link rel="stylesheet" href="/bhr/css/ie6.css" type="text/css" />
    <![endif]-->
    <link rel="stylesheet" href="/bhr/admin/manage.css" type="text/css" media="screen" />
    $code
</head>
<body class="home special" id="home">
$header
                    <div id="subnav">
                    <div class="inner">
                    <ul>
                        <li><a href="/cgi-bin/bhr/manage/issue?&action=new">Create New Issue</a></li>
                        <li><a href="/cgi-bin/bhr/manage/issue?&action=manage">Edit/Manage All Issues</a></li>
                    </ul>
                    </div>
                    </div>
    
                <div id="page-wrap">
|;
return "Content-type:text/html\n\n$html";
}

sub footer {
    my $html = Helper::ssi(Helper::slurp("$ENV{DOCROOT}/ssi/standard.footer.html"))." </body>\n</html>\n";
    return $html;
}

sub itemshtml {
    my $issue = shift;
    my $item_param = shift;
    my $items = qq|<p><a href="/bhr/${\$issue->url}">Preview Issue ${\$issue->id} &raquo;</a></p><h3>Items In ${\$issue->id}</h3><p><a href="item?id=${\$issue->id}&action=start">&raquo; Create An Item</a><br/><br/></p><ol>|;
    
    for my $item ($issue->items) {
        my $url = $item->url;
        my $show = $item->display;
        #if ($show =~/hide/i) { next; } #will hide item if display = Hide
        if ($item->body eq "") { $url = ""; } else { $url .= "\.html"; }  
        $items .= qq|<li>Display Status: $show<br/><a href="item?id=${\$issue->id}&item=${\$item->id}">Edit</a> \| <a href="issue?action=moveup&id=${\$issue->id}&item=${\$item->id}">Move Up</a>  \| <a href="issue?action=movedown&id=${\$issue->id}&item=${\$item->id}">Move Down</a> \| <a href="/bhr/${\$issue->url}$url">Preview Item</a> <br/><a href="item?id=${\$issue->id}&item=${\$item->id}">${\$item->type}: ${\$item->title}</a><br/>${\$item->bookauthor} <br/><br/></li>|;
    }
    $items .= qq|</ol>|;
    
    return $items;
}

sub rebuild {
    my $issue = shift;
    my $type = shift;
    use Tasks::MakeIssue;
    my $site = BHRSite->new;
    my @issues = $site->issues;
    if ($type eq "singlerebuild") {
    	    Tasks::MakeIssue::run($site,$issue);
	    
	    use Tasks::MakeIndex; 
	    Tasks::MakeIndex::run();

	    use Tasks::ListArchive; 
	    Tasks::ListArchive::run();
	    
    } elsif ($type eq "fullrebuild") {
    
	    for my $this_issue (@issues) {
		Tasks::MakeIssue::run($site,$this_issue);
	    }
	    
	    use Tasks::MakeIndex; 
	    Tasks::MakeIndex::run();

	    use Tasks::ListArchive; 
	    Tasks::ListArchive::run();
    }   

}

1;