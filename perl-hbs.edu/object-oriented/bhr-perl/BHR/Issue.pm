#!/usr/local/bin/perl

use strict;

{
    package BHRIssue;
    use Config::IniFiles;


    sub new {
        my $class = shift;
        my $file = shift;
        my $self = {};
        bless $self, $class;
        $self->init($file);
        return $self;
    }

    sub init {    
        my $self = shift;
        my $file = shift;
        return if !$file;
        my $cfg = new Config::IniFiles( -file => $file, -allowcontinue => 1 ) 
            || die "Unable to parse $file - ".join("\n",@Config::IniFiles::errors);
        $self->{cfg} = $cfg;
        $self->{file} = $file;
        my @attributes = ("title","byline","reviewer","summary","body","fname","lname","aboutauthor","type","bookauthor","featured","citation","authorinfo","url","display","altpdf");
        $self->{attributes} = \@attributes;
        $self->{volume} = $cfg->val('issue','volume');
        $self->{num} = $cfg->val('issue','num');
        $self->{year} = $cfg->val('issue','year','');
        $self->{season} = $cfg->val('issue','season','');
        $self->{tagline} = $cfg->val('issue','tagline','');
        $self->{is_special} = $cfg->val('issue','is_special','');
        $self->{specialtitle} = $cfg->val('issue','specialtitle','');
        $self->{guesteditor} = $cfg->val('issue','guesteditor','');
        my @items = $self->init_items();
        $self->{items} = \@items;
        die "Bad filename, must match issue-num.ini found $file" if $file !~ /\/${\$self->volume}-${\$self->num}.ini/
    }
    
    sub init_items {
        my $self = shift;
        my $file = $self->{file};
        my $cfg = $self->{cfg};
        my @items;
        my $num = 1;
        for my $item ($cfg->Sections) {
            next if $item eq 'issue';
            my $it = BHRItem->new($self);
            for my $attr (@{$self->{attributes}}) {
               $it->{$attr} = $cfg->val($item,$attr,"");
            }
            $it->{id} = $num;
            push(@items,$it);
            $num++;
        }
    	return @items;
    }  
    
    sub items { 
        my $self = shift;
        return () if !$self->{items};
        return @{$self->{items}};
    }
    
    sub attributes { 
        my $self = shift;
        return () if !$self->{attributes};
        return @{$self->{attributes}};
    }
    
    sub id {
        my $self = shift;
        return $self->volume . '-' . $self->num;
    }

    sub volume {
        my $self = shift;
        return $self->{volume};
    }

    sub num {
        my $self = shift;
        return $self->{num};
    }

    sub year {
        my $self = shift;
        return $self->{year};
    }
    
    sub url {
        my $self = shift;
        return $self->volume."/".$self->num."/";
    }
    
    sub season {
        my $self = shift;
        return $self->{season};
    }
    
    sub tagline {
        my $self = shift;
        return $self->{tagline};
    }
    
    sub is_special {
        my $self = shift;
        return $self->{is_special};
    }
    
    sub specialtitle {
        my $self = shift;
        return $self->{specialtitle};
    }
    
    sub guesteditor {
        my $self = shift;
        return $self->{guesteditor};
    }

    sub file {
        my $self = shift;
        return $self->{file};
    }
    
    sub set_items {
        my $self = shift;
        $self->{items} = \@_;
    }
    
    sub delete {
        my $self = shift;
        my $file = "$ENV{DOCROOT}/issues/${\$self->volume}-${\$self->num}.ini";
        unlink $file || die "can't delete $file";
    }
    
    sub save {
        my $self = shift;
        my $type = shift;
        die "bad fn" if $self->file eq '-.ini';
        open(OUT,">".$self->file);
        print OUT qq|[issue]
volume = ${\$self->volume}
num = ${\$self->num}
season = ${\$self->season}
year = ${\$self->year}
tagline = ${\$self->tagline}
is_special = ${\$self->is_special}
specialtitle = ${\$self->specialtitle}
guesteditor = ${\$self->guesteditor}\n
|;

        my $num = 0;
        for my $item ($self->items) {
            $num++;
            print OUT qq|[article $num]\n|;
            for my $attr (@{$self->{attributes}}) {
               my $temp = $item->{$attr};
               if ($temp =~ /\n/ ) { 
                         #checking for line breaks and adding special syntax
                         $temp = "<<EOT\n$temp\nEOT";  
               }
               print OUT qq|$attr = $temp\n|;
            }
            print OUT "\n";
        }

        close OUT;
        Helper::rebuild($self,$type);
    }
    
    sub set {
        my $self = shift;
        my $key = shift;
        my $val = shift;
        $self->{$key} = $val;
    }

    sub make_issue {
        my $class = shift;
        my $volume = shift;
        my $issue = shift;
        my $season = shift;
        my $file = "$ENV{DOCROOT}/issues/$volume-$issue.ini";
        open(OUT,">$file") || die $!;
        print OUT qq|[issue]
volume = $volume
num = $issue
season = $season
            |;
        close OUT;
        return BHRIssue->new($file);
    }
    
    sub save_current_issue {
        my $self = shift;
        my $volume = $self->volume;
        my $num = $self->num;
        return "$volume-$num";
    }  
    
    sub compare_current_issue {
        my $self = shift;
        my $site = shift;
        my $current = "archives";
        if ($self->id eq $site->current_issue) {
                $current = "home";
        } 
        return $current;
    }
    
    sub index_html {
        my $self = shift;
        my $site = shift;
        my $current = $self->compare_current_issue($site);
        my $year = $self->year;
        my $season = $self->season;
        my $volume = $self->volume;
        my $tagline = $self->tagline ? "<p>${\$self->tagline}</p>" : "";
        my $num = $self->num;
        my $lcseason = lc($season);
        my $is_special = $self->is_special;
        my $specialtitle = $self->specialtitle;
        my $guesteditor = $self->guesteditor;

        my $body = "";
        my $bookbody = "";
        my $special = "";
        my $count = 1;
        if ($is_special eq "Yes") {
            $special .= qq|<div class="special"><h4>$specialtitle</h4><span class="special-author">Guest Editor: $guesteditor</span></div>|;
        }

        for my $item ($self->items) {
           my $title = $item->title;
           my $summary = $item->summary;
           my $byline = $item->byline;
           my $url = $item->url;
           my $featured = $item->featured;
           my $type = $item->type;
           my $reviewer = $item->reviewer;
           my $display = $item->display;
           my $reviewbody = $item->body;
           if ($featured eq "Yes" && $type eq "Article" && $display eq "Show") {
            $body .= "<h3><a href=\"/bhr\/$volume\/$num\/$url\.html\">$title</a></h3><!-- url=\"/cgi-bin/bhr/manage/item?id=$volume-$num&item=$count\" label=\"Edit Item\" -->\n<span class=\"author\">By $byline</span><p>$summary</p>\n";
           } elsif ($type =~ /article/i  && $display eq "Show") {
            $body .= "<h3>$title</h3><!-- url=\"/cgi-bin/bhr/manage/item?id=$volume-$num&item=$count\" label=\"Edit Item\" -->\n<span class=\"author\">By $byline</span><p>$summary</p>\n";
           } elsif ($type =~ /review/i && $display eq "Show" && $reviewbody =~ /\S/i) {
            $bookbody .= qq|<dt><a href="/bhr/$volume/$num/$url\.html">$title</a><!-- url="/cgi-bin/bhr/manage/item?id=$volume-$num&item=$count" label="Edit Item" --></dt><!--<dd>by $byline</dd>--><dd><i>Reviewed by $reviewer</i></dd>|;
           } 
           $count++;
        }
        
        return qq|
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Volume $volume - Issue $num - Business History Review - Harvard Business School</title>
    <meta name="Description" content="The Business History Review is a quarterly publication of original research by historians, economists, sociologists, and scholars of business administration." />
    <meta name="Keywords" content="Business History Review, Business History, HBS, Harvard Business School Business History" />
    <!--#include virtual="/bhr/ssi/standard.head.html"-->
</head>
<body class="$current special" id="home">
    <!--#include virtual="/bhr/ssi/standard.header.html"-->

    <div id="subnav">
     <div class="inner">
     <a href="/bhr/$volume/$num/"><img src="/bhr/images/site/cover-$lcseason.jpg" width="190" height="285" alt="BHR $season Cover"  /></a>
      <h3>Full Articles:</h3>
<!--#include virtual="/bhr/full-articles_content.html"-->
    
      </div>
    </div>

<div id="page-wrap">
    <div id="masthead">
       
        
    
    <h2>$season $year <em><span>Volume</span> <span class="big">$volume <!-- url="/cgi-bin/bhr/manage/issue?id=$volume-$num" label="Edit Issue" --></span> <span>Issue</span> <span class="big">$num<!-- url="/cgi-bin/bhr/manage/issue?&action=new" label="Add Issue" --><!-- url="/cgi-bin/bhr/manage/item?id=$volume-$num&action=start" label="Add Item" --></span></em></h2>
    </div>

    <div id="content">
    
    $special

     <br clear="all" />
    <div class="inner">
    $body
    </div>
    </div><!-- content -->
<!-- #include virtual="/bhr/archive.gen.html" -->
   <div id="sidebar">
    <div class="inner">
     <div class="module1">
     <h3>Book Reviews:</h3>
        <dl>
        $bookbody
    </dl>
     <p><a class="more" href="/bhr/$volume/$num/all.html">all reviews</a></p>
     </div><!-- module 1 -->
    </div><!-- inner -->
   </div><!-- sidebar -->

 </div><!-- page-wrap -->
    <!--#include virtual="/bhr/ssi/standard.footer.html"-->
</body>
</html>       
|;
        
    } 

 sub all_items {
        my $self = shift;
        my $site = shift;
        my $current = $self->compare_current_issue($site);
        my $year = $self->year;
        my $season = $self->season;
        my $volume = $self->volume;
        my $tagline = $self->tagline ? "<p>${\$self->tagline}</p>" : "";
        my $num = $self->num;
        my $lcseason = lc($season);
        my $is_special = $self->is_special;
        my $specialtitle = $self->specialtitle;
        my $guesteditor = $self->guesteditor;

        my $body = "";
        my $bookbody = "";
        my $special = "";
        my $all_items = "";
        my $count = 1;
        my $first = "";


        for my $item ($self->items) {
           my $title = $item->title;
           my $summary = $item->summary;
           my $byline = $item->byline;
           my $url = $item->url;
           my $featured = $item->featured;
           my $type = $item->type;
           my $reviewer = $item->reviewer;
           my $display = $item->display;
           my $body = $item->body;
           if ($count == 1) { 
                $first = qq| class="first"|;
           } else { 
                $first = ""; 
           }
           if ($type =~ /review/i && $display eq "Show" && ($body =~ /\S/i) ) {
            $all_items .= qq|\n<dt$first>\n<a href="/bhr/$volume/$num/$url\.html">$title</a>\n<!-- url="/cgi-bin/bhr/manage/item?id=$volume-$num&item=$count" label="Edit Item" --></dt>\n<dd><i>Reviewed by $reviewer</i></dd>\n|;
            $count++;
           } elsif ($type =~ /review/i && $display eq "Show" && (!$body =~ /\S/i)) {
            $all_items .= qq|\n<dt$first>$title<!-- url="/cgi-bin/bhr/manage/item?id=$volume-$num&item=$count" label="Edit Item" --></dt>\n<dd><i>Reviewed by $reviewer</i></dd>|;
            $count++;
           } 
        }
     
        return qq|<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>All Book Reviews - Volume $volume - Issue $num - Business History Review - Harvard Business School</title>
    <!--#include virtual="/bhr/ssi/standard.head.html"-->
</head>
<body class="$current books" id="home">
    <!--#include virtual="/bhr/ssi/standard.header.html"-->

    <div id="subnav">
     <div class="inner">
     <a href="/bhr/$volume/$num/"><img src="/bhr/images/site/cover-$lcseason.jpg" width="190" height="285" alt="BHR Cover"  /></a>
      <h3>Full Articles:</h3>
<!--#include virtual="/bhr/full-articles_content.html"-->
      </div>
    </div>

<div id="page-wrap">
    <div id="masthead">
    <!-- url="/cgi-bin/bhr/manage/issue?id=$volume-$num" label="Edit Issue" -->
    <!-- url="/cgi-bin/bhr/manage/item?id=$volume-$num&action=start" label="Add Item" -->
    <!-- url="/cgi-bin/bhr/manage/issue?&action=new" label="Add Issue" -->    
    <h2>$season $year<em><span>Volume</span> <span class="big">$volume</span> <span>Issue</span> <span class="big">$num</span></em></h2>
    </div>

    <div id="content">
    <div class="books">
        <!--#include virtual="/bhr/books-essays-header_content.html"-->
    </div>
     <br clear="all" />
    <div class="inner">
    <dl>
    $all_items
    </dl>
    <p><a class="more" href="/bhr/$volume/$num/">Issue home</a></p>
    </div>
    <!-- #include virtual="/bhr/archive.gen.html" -->
    </div><!-- content -->

   <div id="sidebar">
    <div class="inner">
     <div class="module1">
     <h3>Full Issue</h3>
      <p><a class="more" href="/bhr/$volume/$num/">view articles</a></p>
     </div><!-- module 1 -->
    </div><!-- inner -->
   </div><!-- sidebar -->

 </div><!-- page-wrap -->
    <!--#include virtual="/bhr/ssi/standard.footer.html"-->
</body>
</html>
|;
    }
    
   

}


1;
