#!/usr/local/bin/perl

use strict;
use BHR::Env;
use BHR::Markdown;

{
    package BHRItem;

    sub new {
        my $class = shift;
        my $issue = shift;    
        my $self = {};
        bless $self, $class;
        $self->init($issue);
        return $self;
    }
    
    sub init {    
        my $self = shift;
        my $issue = shift;
        return if !$issue;
        $self->{issue} = $issue;
    } 
    
    sub issue {
        my $self = shift;
        return $self->{issue};
    }

    sub title {
        my $self = shift;
        return $self->{title};
    }

    sub byline {
        my $self = shift;
        return $self->{byline};
    }

    sub reviewer {
        my $self = shift;
        return $self->{reviewer};
    }

    sub summary {
        my $self = shift;
        return $self->{summary};
    }

    sub body {
        my $self = shift;
        return $self->{body};
    }
    
    sub fname {
        my $self = shift;
        return $self->{fname};
    }
    
    sub lname {
        my $self = shift;
        return $self->{lname};
    }
    
    sub aboutauthor {
        my $self = shift;
        return $self->{aboutauthor};
    }
    
    sub type {
        my $self = shift;
        return $self->{type};
    }

    sub bookauthor {
        my $self = shift;
        return $self->{bookauthor};
    }
    
    sub body {
        my $self = shift;
        return $self->{body};
    }
    
    sub featured {
        my $self = shift;
        return $self->{featured};
    }

    sub citation {
        my $self = shift;
        return $self->{citation};
    }

    sub authorinfo {
        my $self = shift;
        return $self->{authorinfo};
    }
    
    sub display {
        my $self = shift;
        return $self->{display};
    }
    
    sub altpdf {
        my $self = shift;
        return $self->{altpdf};
    }
    
    sub url {
        my $self = shift;
        return $self->{url};
    }
    
    sub set_url {
       my $self = shift;
       my $temp = $self->convert_title;
       my $url = $self->convert_title;
       my $count = 1;
       my $issue = $self->issue;
       my $titlectr = 0;
       for my $item ($issue->items) {
        if ($url eq $item->url && $item->id < $self->id) {
                $titlectr++;
                if ($titlectr > 0) {
                        $count++;
                        $url = $temp . $count;
                }
        }
       }
       return $url;
    }
    
    sub convert_title {
        my $self = shift;
        my $title = $self->title;
        my $id = $self->id;
        my  @words = split /\s+/, $title;
        for my $word (@words) {
                $word =~ s/\W+//g;
        }
        if ($words[1] ne "" ) { $words[1] = "-" . $words[1]; }
        if ($words[2] ne "" ) { $words[2] = "-" . $words[2]; }
        my $url = lc($words[0]) . lc($words[1]) . lc($words[2]);
        return $url;
    }
    
    sub seo_title {
        my $self = shift;
        my $title = $self->title;
        my $id = $self->id;
        my  @words = split /\s+/, $title;
        for my $word (@words) {
                $word =~ s/\W+//g;
        }
        if ($words[1] ne "" ) { $words[1] = " " . $words[1]; }
        if ($words[2] ne "" ) { $words[2] = " " . $words[2]; }
        if ($words[3] ne "" ) { $words[3] = " " . $words[3]; }
        my $title = $words[0] . " " . $words[1] . " " . $words[2] . " " . $words[3] . "\.\.\.";
        return $title;
    }

    sub id {
        my $self = shift;
        return $self->{id};
    }
    
    sub set {
        my $self = shift;
        my $key = shift;
        my $val = shift;
        if ($key eq "url") { $val = $self->set_url; }
        $self->{$key} = $val;
    }
    
    sub item_html {
        my $self = shift;
        my $item_type = shift;
        my $site = shift;
        my $issue = $self->issue;
        my $current = $issue->compare_current_issue($site);
        my $year = $issue->year;
        my $season = $issue->season;
        my $volume = $issue->volume;
        my $tagline = $issue->tagline ? "<p>${\$issue->tagline}</p>" : "";
        my $num = $issue->num;
        my $lcseason = lc($season);
        my $specialtitle = $issue->specialtitle;
        my $guesteditor = $issue->guesteditor;
        my $content = $self->body;
        my $title = $self->title;
        my $seo_title = $self->seo_title;
        my $summary = $self->summary;
        my $byline = $self->byline;
        my $url = $self->url;
        my $featured = $self->featured;
        my $type = $self->type;
        my $reviewer = $self->reviewer;
        my $aboutauthor = $self->aboutauthor;
        my $citation = $self->citation;
        my $author = $self->byline;
        my $bookauthor = $self->bookauthor;
        my $count = $self->id;
        my $altpdf = $self->altpdf;
        my $pdf = "";
        
        my $pdfcode = qq|<style type="text/css">
            #subnav {display: none;}
            #sidebar .module2 {display: none;}
            </style>|;
            
        if ($item_type eq "html") {
                $pdfcode = "";
        }
         
        if ($altpdf =~ /\S/) {
                $pdf = $altpdf;
        } else {
                $pdf = $url . "\.pdf";
        }
        my $body = "";
        $content = Markdown::Markdown($content);

        if ($type =~ /article/i) {

                $body = qq|<!-- url="/cgi-bin/bhr/manage/item?id=$volume-$num&item=$count" label="Edit Item" --><h3>$title</h3>
                <span class="author">$author</span>
                <p class="citation">For Citation: <em>$citation</em></p>
                <p>$content</p>|;


                return qq|<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">

        <head>
            <title>Feature Article ($seo_title) - Volume $volume - Issue $num - Business History Review - Harvard Business School</title>
            <meta name="Description" content="$title" />
            <meta name="Keywords" content="Business History Review, Business History, HBS, Harvard Business School Business History" />
            <!--#include virtual="/bhr/ssi/standard.head.html"-->
                $pdfcode

        </head>
        <body class="$current index" id="article">
            <!--#include virtual="/bhr/ssi/standard.header.html"-->

            <div id="subnav">
             <div class="inner">
             <a href="/bhr/$volume/$num"><img src="/bhr/images/site/cover-$lcseason.jpg" width="190" height="285" alt="BHR $season Cover"  /></a>
              </div>
            </div>

        <div id="page-wrap">
            <div id="masthead">
            <h2>$season $year <em><span>Volume</span> <span class="big">$volume <!-- url="/cgi-bin/bhr/manage/issue?id=$volume-$num" label="Edit Issue" --></span> <span>Issue</span> <span class="big">$num<!-- url="/cgi-bin/bhr/manage/item?id=$volume-$num&action=start" label="Add Item" --></span></em></h2>
            </div>

            <div id="content">
            <div class="inner">
              $body
            </div>
         </div><!-- content -->

         <div id="sidebar">
          <div class="inner">
           <div class="module1a">
           <h3>Article Summary</h3>
           <p>$summary</p>
           </div><!-- module 1a -->
            <div class="module2">
              <h3>About the Author</h3>
              <p>$aboutauthor</p>
           </div><!-- module 2 -->
           <div class="module2">
                 <h3>Download</h3>
                 <p class="last"><b><a class="pdf" href="/bhr/$volume/$num/$pdf">download this article</a></b></p>
           </div><!-- module 2 -->
          </div><!-- inner -->
         </div><!-- sidebar -->
         </div><!-- page-wrap -->
            <!--#include virtual="/bhr/ssi/standard.footer.html"-->
        </body>
        </html>        

        |;
       } else {

                   $body = qq|<h3>$title</h3><!-- url="/cgi-bin/bhr/manage/item?id=$volume-$num&item=$count" label="Edit Item" -->
                   <span class="author">$bookauthor</span>
                <p class="reviewer">Book Review by: <em>$reviewer</em></p>
                <p class="citation">For Citation: <em>$citation</em></p>
                   $content|;

                return qq|

                <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">

                <head>
                    <title>Book Review ($seo_title) - Volume $volume - Issue $num - Business History Review - Harvard Business School</title>
                    <meta name="Description" content="$title" />
                    <meta name="Keywords" content="Business History Review, Business History, HBS, Harvard Business School Business History" />
                    <!--#include virtual="/bhr/ssi/standard.head.html"-->
                        $pdfcode
                </head>
                <body class="bookreview $current" id="book">
                    <!--#include virtual="/bhr/ssi/standard.header.html"-->

                    <div id="subnav">
                     <div class="inner">
                     <a href="/bhr/$volume/$num/"><img src="/bhr/images/site/cover-$lcseason.jpg" width="190" height="285" alt="BHR $season Cover"  /></a>
                      </div>
                    </div>

                <div id="page-wrap">
                    <div id="masthead">
                    <h2>$season $year <em><span>Volume</span> <span class="big">$volume <!-- url="/cgi-bin/bhr/manage/issue?id=$volume-$num" label="Edit Issue" --></span> <span>Issue</span> <span class="big">$num <!-- url="/cgi-bin/bhr/manage/item?id=$volume-$num&action=start" label="Add Item" --></span></em></h2>
                    </div>

                    <div id="content">
                    <div class="inner">
                      $body
                    </div>
                 </div><!-- content -->

                 <div id="sidebar">
                  <div class="inner">
                   <div class="module2">
                   <h3>About the Reviewer</h3>
                   <p>$aboutauthor</p>
                   </div><!-- module 2 -->
                   <div class="module2">
                         <h3>Download</h3>
                         <p class="last"><b><a class="pdf" href="/bhr/$volume/$num/$pdf">download this article</a></b></p>
                   </div><!-- module 2 -->
                  </div><!-- inner -->
                 </div><!-- sidebar -->
                 </div><!-- page-wrap -->
                    <!--#include virtual="/bhr/ssi/standard.footer.html"-->
                </body>
        </html>      
        |;
       }
        
    } 
}

1;