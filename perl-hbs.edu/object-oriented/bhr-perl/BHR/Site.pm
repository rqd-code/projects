#!/usr/local/bin/perl

use strict;
use BHR::Env;
use BHR::Helper;
use BHR::Issue;
use BHR::Item;

{
    package BHRSite;

    sub new {
        my $class = shift;
        my $self = {};
        bless $self, $class;
        $self->init();
        return $self;
    }

    sub init {
        my $self = shift;
        my @issues = $self->read_issues;
        my @inis = glob("$ENV{DOCROOT}/issues/*.ini");
        @issues = sort {$b->volume <=> $a->volume || $b->num <=> $a->num} @issues;
        $self->{issues} = \@issues;
        $self->{inis} = \@inis;
    }
 
    sub read_issues {
        my $self = shift;
        my @issues;
        my @inis = glob("$ENV{DOCROOT}/issues/*.ini");
        for my $file (@inis) {
                my $issue = BHRIssue->new($file);
                push(@issues,$issue);
        }
        return @issues;
    }
    
    sub issues{ 
       my $self = shift;
       return @{$self->{issues}};
    }
    
    sub inis{ 
       my $self = shift;
       return @{$self->{inis}};
    }
    
    sub current_issue { 
       my $self = shift;
       my @issues = $self->issues;
       return $issues[0]->id;
    }
    sub make_issue {
        my $self = shift;
        my $volume = shift;
        my $issue = shift;
        my $season = shift;
        return BHRIssue->make_issue($volume,$issue,$season);
    }
    
    sub make_item{
        my $self = shift;
        my $issue = shift;
        return BHRItem->new($issue);
    }
}

1;