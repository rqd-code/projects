#!/usr/local/bin/perl

use strict;
use lib '..';
use lib '../../../system/lib/cpan';
use BHR::Site;
use File::Path;

{
    package Tasks::ListArchive;
    sub run {
       my $html = "";
       my $site = BHRSite->new();
       my $volume_checker = "start";
       for my $issue ($site->issues()) {
          my $volume = $issue->volume;
          my $num = $issue->num;
          my $year = $issue->year;
          my $season = $issue->season;
          my $id = $issue->id;
          my $spring = "$volume-1";
          my $summer = "$volume-2";
          my $fall = "$volume-3";
          my $winter = "$volume-4";
          if (grep(/$spring/,$site->inis)) {
                $spring = qq|<td><a href="/bhr/$volume/1/">Spring $year</a><br /></td>|;
          } else {
                $spring = qq|<td>Spring $year<br /></td>|;
          }
          if (grep(/$summer/,$site->inis)) {
                $summer = qq|<td><a href="/bhr/$volume/2/">Summer $year</a><br /></td>|;
          } else {
                $summer = qq|<td>Summer $year<br /></td>|;
          }
          if (grep(/$fall/,$site->inis)) {
                $fall = qq|<td><a href="/bhr/$volume/3/">Autumn $year</a><br /></td>|;
          } else {
                $fall = qq|<td>Autumn $year<br /></td>|;
          }
          if (grep(/$winter/,$site->inis)) {
                $winter = qq|<td class="last"><a href="/bhr/$volume/4/">Winter $year</a><br /></td>|;
          } else {
                $winter = qq|<td class="last">Winter $year<br /></td>|;
          }
          if ($volume eq "83") {
           $spring = qq|<td><a href="/bhr/archives/vol83/vol83-2009-1.html">Spring $year</a><br /></td>|;
           $summer = qq|<td><a href="/bhr/archives/vol83/vol83-2009-2.html">Summer $year</a><br /></td>|;
          }
          if ($volume ne $volume_checker) { 
                  $html .= qq|<tr>
                      <td>Volume $volume</td>
                      <!--<td><a href="/bhr/$volume/index.html">Index</a><br /></td>-->
                      $spring
                      $summer
                      $fall
                      $winter</tr>|;
          }
          $volume_checker = $volume;
       }
       Helper::write_file("$ENV{DOCROOT}/archive.gen.html",$html);
    }
}

if (__FILE__ eq $0) {
       Tasks::ListArchive::run();
}

1;