#!/usr/local/bin/perl

use strict;
use lib '..';
use lib '../../../system/lib/cpan';
use BHR::Site;
use File::Path;

{
    package Tasks::MakeIndex;
    sub run {
       my $html = "";
       my $site = BHRSite->new();
       my $volume_checker = "start";
       my $articles = "";
       my $reviews = "";
       for my $issue ($site->issues()) {
          my $volume = $issue->volume;
          for my $item ($issue->items) {
                my $title = $item->title;
                my $fname = $item->fname;
                my $lname = $item->lname;
                my $byline = $item->byline;
                my $type = $item->type;
                my $reviewer = $item->reviewer;
                if  ($type =~ /article/i) {
                        $articles .= qq|<p>$lname, $fname\. "$title"</p>|;
                } elsif ($type =~ /review/i) {
                        $reviews .= qq|<p>$lname, $fname\. $title. Reviewd by $reviewer\.</p>|;
                }
          }       
          if ($volume ne $volume_checker) { 
                  $html .= qq|<h1>Index to Volume $volume</h1>|;
                  $html .= "<h2>Articles</h2>" . $articles . "<h2>Reviews</h2>" . $reviews;
                  Helper::write_file("$ENV{DOCROOT}/$volume/index.html",$html);
          }
          $volume_checker = $volume;
          $articles = "";
          $reviews = "";
          $html = "";
       }

       
    }
}

if (__FILE__ eq $0) {
       Tasks::MakeIndex::run();
}

1;