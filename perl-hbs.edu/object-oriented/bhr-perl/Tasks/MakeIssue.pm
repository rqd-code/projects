#!/usr/local/bin/perl

use strict;
use lib '..';
use lib '../../../system/lib/cpan';
use BHR::Site;
use File::Path;

{
    package Tasks::MakeIssue;
    sub run {
       my $site = shift;
       my $issue = shift;
       my $volume = $issue->volume;
       my $num = $issue->num;
       my @issues = $site->issues;
       
       mkdir("$ENV{DOCROOT}/$volume");
       mkdir("$ENV{DOCROOT}/$volume/$num/");
       
       Helper::write_file("$ENV{DOCROOT}/index.html",$issues[0]->index_html($site));
       Helper::write_file("$ENV{DOCROOT}/$volume/$num/index.html",$issue->index_html($site));
       Helper::write_file("$ENV{DOCROOT}/$volume/$num/all.html",$issue->all_items($site)); 
       
       for my $item ($issue->items) {
               my $url = $item->url;
               my $body = $item->body;
               my $display = $item->display;
               if ($body =~ /\S/i && $display =~ /Show/i ) {
                        Helper::write_file("$ENV{DOCROOT}/$volume/$num/$url.html",$item->item_html("html",$site));
                        Helper::write_file("$ENV{DOCROOT}/$volume/$num/$url.pdft",$item->item_html("pdf",$site));
               }
       }
    }
}


if (__FILE__ eq $0) {
    my $site = BHRSite->new();
    for my $issue ($site->issues) {
       print "Updating Issue ".$issue->volume."-".$issue->num."\n";
       Tasks::MakeIssue::run($site,$issue);
    }
}

1;