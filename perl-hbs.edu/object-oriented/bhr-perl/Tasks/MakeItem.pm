#!/usr/local/bin/perl

use strict;
use lib '..';
use lib '../../../system/lib/cpan';
use BHR::Site;
use File::Path;

{
    package Tasks::MakeItem;
    
    sub run {
       my $site = shift;
       my $issue = shift;
       my $item = shift;
       my $volume = $issue->volume;
       my $num = $issue->num;
       my $featured = $item->featured;
       my $type = $item->type;
       my $url = $item->url;
       if ($type =~ /review/i) {
                Helper::write_file("$ENV{DOCROOT}/$volume/$num/$url.html",$issue->review_html($item));
       } else {
                Helper::write_file("$ENV{DOCROOT}/$volume/$num/$url.html",$issue->article_html($item));
       } 
    }
}

if (__FILE__ eq $0) {
    my $site = BHRSite->new();
    for my $issue ($site->issues) {
        for my $item ($issue->items) {
               print "Updating Items ".$issue->volume."-".$issue->num. "-" . $item->id . "\n";
               Tasks::MakeItem::run($site,$issue,$item);
        }
    }
}

1;