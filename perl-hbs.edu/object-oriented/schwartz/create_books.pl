use strict;
use Spreadsheet::SimpleExcel;                        
use Win32::OLE;
use Win32::OLE qw(in with);
use Win32::OLE::Variant;
use Win32::OLE::Const 'Microsoft Excel';
$Win32::OLE::Warn = 3;   

#######################################3
#
#NOTE: To run this script you must open cygwin, 
#go to the directory where your script is and type
# perl create_books.pl enter_number_of_rows_from the excel spreadsheet
#perl create_books.pl 20
#
###############################


###########################################################
###########################################################
###########################################################
#Script must be run from windows dos prompt w/ perl on it##
###########################################################
###########################################################

# get already active Excel application or open new
my $Excel = Win32::OLE->GetActiveObject('Excel.Application') || Win32::OLE->new('Excel.Application', 'Quit');  

# open Excel file
my $Book = $Excel->Workbooks->Open("Y:/htdocs/wwwhbs/schwartz/admin/sitespecs/content/books.xls"); 

# You can dynamically obtain the number of worksheets, rows, and columns
# through the Excel OLE interface.  Excel's Visual Basic Editor has more
# information on the Excel OLE interface.  Here we just use the first
# worksheet, rows 1 through 4 and columns 1 through 3.
# select worksheet number 1 (you can also select a worksheet by name)

my $rows = $ARGV[0];

if(!$ARGV[0]) {
    print "Please remember to enter the number of rows in the excel spreadsheet. The format to run this script is perl create_inis.pl enter_number_of_rows\n\n";
    exit;
}

my $basedir = "Y:/htdocs/wwwhbs/schwartz/bookcollection.gen.html";
my $Sheet = $Book->Worksheets(1);
my $booktitle;
my $subjecttitle;
my $pubdate;
my $author;
my $publisher;
my $callnumber;
my $url;
open(OUT, ">$basedir") or die("Couldn't open output file: $!");
for(my $cnt = 2;$cnt<=$rows;$cnt++){
	$booktitle = $Sheet->Cells($cnt,1)->{'Value'};
	$subjecttitle = $Sheet->Cells($cnt,2)->{'Value'};
	$pubdate = $Sheet->Cells($cnt,4)->{'Value'};
	$author = $Sheet->Cells($cnt,5)->{'Value'};
	$publisher = $Sheet->Cells($cnt,6)->{'Value'};
	$callnumber = $Sheet->Cells($cnt,8)->{'Value'};
	$url = $Sheet->Cells($cnt,9)->{'Value'};
	$url =~ s/&/&amp;/g;
	print OUT qq| <p><strong><a href="$url">$booktitle</a></strong> / $author;
     	$publisher, $pubdate.</p>|;
	print "$booktitle \n";

}
close(OUT);

$Book->Close;