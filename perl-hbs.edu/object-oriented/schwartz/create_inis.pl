use strict;
use Spreadsheet::SimpleExcel;                        
use Win32::OLE;
use Win32::OLE qw(in with);
use Win32::OLE::Variant;
use Win32::OLE::Const 'Microsoft Excel';
$Win32::OLE::Warn = 3;   

#######################################3
#
#NOTE: To run this script you must open cygwin, 
#go to the directory where your script is and type
# perl create_inis.pl enter_number_of_rows_from the excel spreadsheet
#perl create_inis.pl 209
#
###############################


###########################################################
###########################################################
###########################################################
#Script must be run from windows dos prompt w/ perl on it##
###########################################################
###########################################################

# get already active Excel application or open new
my $Excel = Win32::OLE->GetActiveObject('Excel.Application') || Win32::OLE->new('Excel.Application', 'Quit');  

# open Excel file
my $Book = $Excel->Workbooks->Open("Y:/htdocs/wwwhbs/schwartz/admin/sitespecs/content/perl-db.xls"); 

# You can dynamically obtain the number of worksheets, rows, and columns
# through the Excel OLE interface.  Excel's Visual Basic Editor has more
# information on the Excel OLE interface.  Here we just use the first
# worksheet, rows 1 through 4 and columns 1 through 3.
# select worksheet number 1 (you can also select a worksheet by name)

my $rows = $ARGV[0];

if(!$ARGV[0]) {
    print "Please remember to enter the number of rows in the excel spreadsheet. The format to run this script is perl create_inis.pl enter_number_of_rows\n\n";
    exit;
}

my $basedir = "Y:/htdocs/wwwhbs/schwartz/configs";
my $Sheet = $Book->Worksheets(1);
my $temp = 1;
my $artistname;
my $fname;
my $lname;
my $filename;
my $booktitles;
my $website;
my $yearpurchased;
my $title;
my $description;
my $datecreated;
my $dimensions;
my $medium;
my $location;
my $dirname;
my $imgname;
my $thumbname;
my $temp1 = $Sheet->Cells(2,2)->{'Value'};
my $Rangea;
my $Rangeb;
my $id;
my $fnameID;
my $lnameID;
my $coartist;
my $mediumfinal;
my $noflash;
my $idnumber;

for(my $cnt = 1;$cnt<=$rows;$cnt++){
	$idnumber = $Sheet->Cells($cnt,1)->{'Value'};
	$artistname = $Sheet->Cells($cnt,2)->{'Value'};
	($lname,$fname) = split(/,/,$artistname);
	$lname =~ s/^\s+//;
	$lname=~ s/\s+$//;
	$fname =~ s/^\s+//;
	$fname=~ s/\s+$//;
	#$fname =~ s/\s+//;
	$lnameID = lc($lname);
	$lnameID =~ s/\s+//;
	$lnameID =~ s/[^\w]+//g; 
	$fnameID = $fname;
	$fnameID =~ s/[^\w]+//g; 
	$fnameID =~ s/\s+//;
	$fnameID = lc($fnameID); 
	$booktitles = $Sheet->Cells($cnt,3)->{'Value'};
	$website = $Sheet->Cells($cnt,4)->{'Value'};
	$yearpurchased = $Sheet->Cells($cnt,5)->{'Value'};
	$title = $Sheet->Cells($cnt,6)->{'Value'};
	$description = $Sheet->Cells($cnt,7)->{'Value'};
	$datecreated = $Sheet->Cells($cnt,8)->{'Value'};
	$dimensions = $Sheet->Cells($cnt,9)->{'Value'};
	$medium = $Sheet->Cells($cnt,10)->{'Value'};
	$location = $Sheet->Cells($cnt,11)->{'Value'};
	$coartist = $Sheet->Cells($cnt,12)->{'Value'};
	$mediumfinal = $Sheet->Cells($cnt,13)->{'Value'};
	$noflash = $Sheet->Cells($cnt,14)->{'Value'};
	$filename = lc($lnameID) . $idnumber . ".html";
	$dirname = lc($lnameID); #also the id
	$imgname = $lnameID . $idnumber . "jpg";
	$thumbname = $lnameID .$idnumber . "gif";
	$id = $lnameID . $fnameID . $idnumber;
	open(OUT, ">$basedir/$id.ini") or die("Couldn't open output file: $!");
	print OUT qq|[$id]  
firstname         = $fname
lastname          = $lname
bio               = 
website           = $website
books		  = $booktitles
yearpurchased	  = $yearpurchased
title		  = $title
description       = $description
datecreated	  = $datecreated
dimensions     	  = $dimensions
medium		  = $medium
location	  = $location
coartist	  = $coartist
mediumsorted	  = $mediumfinal
noflash           = $noflash
|;
print "$id.ini \n";
close(OUT);
}

$Book->Close;