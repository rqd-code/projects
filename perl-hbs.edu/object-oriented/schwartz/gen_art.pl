#!/usr/local/bin/perl

my $country = (split(/\//,$ARGV[0]))[-1];

use strict;
use diagnostics;
use CGI::Carp "fatalsToBrowser";
use IO::File;
use File::Basename qw(basename);
use CGI qw(:standard);
use lib 'pub/lib';
use Config::IniFiles;

print "Content-type: text/html\n\n\n";

my $DOCROOT = "../../../../htdocs/wwwhbs/schwartz/items/";
my $INIROOT = "/san/dev/htdocs/wwwhbs/schwartz/configs/countriesini/";

my @filelisting = <$INIROOT/*>; #getting the file listing for all ini files
				#so I can loop through them

my $fileparam = shift;
my $filename = basename($fileparam);
$filename =~ s/\.ini//g;

my $tmp;
my $cpy;

foreach my $file (sort @filelisting) {

        #only render this one country
        next if $file !~ $country; 
        
	$cpy = basename($file);

	my $cfg = new Config::IniFiles( -file => "$file",
                                      -allowcontinue => 1 ) || die("Please check the syntax of your $file. $!");

	$cpy =~ s/\.ini//g;

	#print h1($cpy);

	my $name = $cfg->val($cpy,'title');

	#print h2($name);

my $hdr = qq%
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>$name Country Guide - Baker Library</title>

<!--

************* NOTICE ********************

This page is automatically generated from an internal application,
Any changes you make to this page will be lost.

*****************************************

-->


<!--#include virtual="/ssi/headerinfo.html"-->
<!--#exec cgi="/bkr_cgi/get_meta.pl" -->
</head><body class="gu twocol">
<!--#include virtual="/ssi/standard.header.html"-->
<!--#include virtual="/countries/nav.html"-->
<div id="content">
%;

my $ftr = qq%
</div>
<!--#include virtual="/ssi/standard.footer.html"-->

</body>
</html>
%;

open (OUT,">$DOCROOT/$cpy.html");
	print OUT $hdr;

	print OUT h1("Country Guides");


	print OUT "<h2><img src=\"/images/country/flags/$cpy.gif\" alt=\"\"> $name</h2>\n\n";

	print OUT h2("Profiles &amp; Descriptions");

	print OUT "<ul>\n\n";
	if ($cfg->val($cpy,'national_statistics')) {
		print OUT "<li><a href=\"", $cfg->val($cpy,'national_statistics') ,"\">",$cfg->val($cpy,'national_statistics_name') ,"</a></li>\n\n";
	}
	if ($cfg->val($cpy,'worldbank_region')) {
		print OUT "<li><a href=\"", $cfg->val($cpy,'worldbank_region') ,"\">World Bank Country Profile</a></li>\n\n";
	}
	if ($cfg->val($cpy,'cia')) {
		print OUT "<li><a href=\"", $cfg->val($cpy,'cia') ,"\">The World Factbook</a><br />(U.S. Central Intelligence Agency)</li>\n\n";
	}
	if ($cfg->val($cpy,'loc')) {
		print OUT "<li><a href=\"", $cfg->val($cpy,'loc') ,"\">Country Study</a><br />(Library of Congress)</li>\n\n";
	}
	if ($cfg->val($cpy,'tradeport')) {
		print OUT "<li><a href=\"", $cfg->val($cpy,'tradeport') ,"\">Tradeport</a></li>\n\n";
	}
	if ($cfg->val($cpy,'nation_master')) {
		print OUT "<li><a href=\"", $cfg->val($cpy,'nation_master') ,"\">NationMaster</a></li>\n\n";
	}
	if ($cfg->val($cpy,'custom_profiles')) {
		print OUT "<li>custom_profiles</li>\n\n";
	}
	print OUT "</ul>\n\n";

	if ($cfg->val($cpy,'boc') || $cfg->val($cpy,'custom_catalog_search')) {

	print OUT "<h2>Online Catalog Search</h2>\n\n";
	print OUT "<ul>\n\n";

	#print OUT "<li><a href=\"http://voyager.library.hbs.edu/cgi-bin/Pwebrecon.cgi?DB=local&Search_Arg=(SKEY+%22",$cfg->val($cpy,'boc') ,"%22&SL=None&Search_Code=CMD&CNT=25\">Recent Material</a></li>\n\n";
	print OUT "<li><a href=\"/bkr_cgi/voylink?subject=".$cfg->val($cpy,'boc') ,"\">Recent Material</a></li>\n\n";

		if ($cfg->val($cpy,'custom_catalog_search')) {
			print OUT "<li>", $cfg->val($cpy,'custom_catalog_search'),"</li>\n\n";
		}
	print OUT "</ul>";
	}



	if ($cfg->val($cpy,'exchange_url') || $cfg->val($cpy,'index_code') || $cfg->val($cpy,'bank_url')|| $cfg->val($cpy,'exchange_rate')|| $cfg->val($cpy,'custom_investment')) {
	print OUT "<h2>Investment &amp; Finance</h2>\n\n";

	print OUT "<ul>\n\n";

		if ($cfg->val($cpy,'exchange_url')) {
			print OUT "<li><a href=\"", $cfg->val($cpy,'exchange_url') ,"\">",$cfg->val($cpy,'exchange_name') ,"</a></li>\n\n";
		}
		if ($cfg->val($cpy,'index_code')) {
			print OUT "<li><a href=\"", $cfg->val($cpy,'index_code') ,"\">",$cfg->val($cpy,'index_name') ," Index</a><br /> (via Yahoo Finance)</li>\n\n";
		}
		if ($cfg->val($cpy,'bank_url')) {
			print OUT "<li><a href=\"", $cfg->val($cpy,'bank_url') ,"\">",$cfg->val($cpy,'bank_name') ," Index</a></li>\n\n";
		}
		if ($cfg->val($cpy,'exchange_rate')) {
			print OUT "<li><a href=\"", $cfg->val($cpy,'exchange_rate') ,"\">Current Exchange Rate</a><br /> (via Yahoo Finance)</li>\n\n";
		}
		if ($cfg->val($cpy,'custom_investment')) {
			print OUT "<li><a href=\"", $cfg->val($cpy,'custom_investment') ,"\">",$cfg->val($cpy,'custom_investment') ,"</a></li>\n\n";
		}

	print OUT "</ul>\n\n";

	print OUT "<h2>News</h2>\n\n";

	print OUT "<ul>";
	print OUT "<li><a href=\"http://www.library.hbs.edu/hbsonly_cgi/pqdsearch.pl?GEO(", $cpy ,")+and+cc(1110+or+1120)\">Business & Economic Conditions: Recent Articles</a>\n\n";
	print OUT "<br />(via ABI/Inform)\n\n";
	print OUT "<br />(Current HBS Only)\n\n";
	if ($cfg->val($cpy,'foreign_newspaper')) {
		print OUT "<li><a href=\"http://hcl.harvard.edu/libraries/widener/collections/newspapers/search.cfm\">Foreign Newspapers on Microform at Harvard</a> (check availability)</li>\n\n";
	}
	if ($cfg->val($cpy,'online_newspaper')) {
		print OUT "<li><a href=\"", $cfg->val($cpy,'online_newspaper') ,"\">Online Newspapers by Country--Internet Public Library (IPL)</a></li>\n\n";
	}

	print OUT "</ul>\n\n";


	}
	print OUT $ftr;

close(OUT);
`chmod -f 775 $DOCROOT/$cpy.html`;
}

print "<p>Countries Page Regeneration Successful!</p>";

print "View page: <a href=http://webstage.library.hbs.edu/countries/guides/$filename.html>http://webstage.library.hbs.edu/countries/guides/$filename.html</a><br/><br/>\n\n";
