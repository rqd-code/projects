#!/usr/local/bin/perl

use CGI ":standard";
use CGI::Carp "fatalsToBrowser";

print "Content-type: text/html\n\n";

$basedir = "/san/stage/htdocs/wwwhbs/schwartz/configs";

if (param('id')) {
    $id = param('id');
    $name = param('name');
    if (-e "$basedir/$id.ini") {print "$id.conf already exists";exit;}
    open(FILE,">$basedir/$id.ini");
	print FILE qq|[$id]  
firstname		  = 
lastname                  = 
bio                       = 
website                   = 
books			  = 
yearpurchased		  = 
title			  = $name;
description		  = 
datecreated		  = 
dimensions     		  = 
medium			  = 
location		  =  
|;
    close FILE;
    `chmod 665 $basedir/$id.ini`;
    print "<h3>$name ($id.ini) Created</h3>";
}


print "<ol>";
for $file (`ls $basedir`) {

    #$title = `egrep "^name" $basedir/$file`;
    #$title =~ s/^.*?=//g;
    print "<li><a href=\"/cgi-bin/pub/edit/schwartz/schwartz/configs/$file\">$file</a></li>\n";
}
print "</ol>";


print qq|<form method="post">
	<h2>Create a new Country</h2>
	<table>
	<tr><td>ID</td><td><input type="text" name="id"></td><td>Example: <code>artistname23 (artistname plus any unique item #)</code></tr>
	<tr><td>Name</td><td><input type="text" name="name"></td><td>Example: <code>East Germany</code></td></tr>
	<tr><td></td><td><input type="submit" value="Create"></td></tr>
	
	</form>|;

