#!/usr/local/bin/perl

use strict;
use CGI qw(:standard);
use CGI::Carp "fatalsToBrowser";

umask 2; #to manage permissions on the files

use lib '..';
use Schwartz::Site;

print "Content-type: text/html\n\n";

my $site = Site->new(DOCROOT => "../../../../htdocs/wwwhbs/schwartz");

my @databases = $site->read_databases;

foreach my $db (@databases) {
	$db->delete_sub_nav_html;
}

print "<h3>Creating Item HTML</h3>";
print "<ol>";
my @types = ("artist","title","datecreated","location","medium");
foreach my $sortType (@types) {
	print_sort_pages($sortType,@databases);
}
print "</ol>";


$site->create_item_pages(@databases);
$site->create_item_nav_css(@databases);
$site->create_art_in_collection(@databases);

print qq|<p><a href="/schwartz/">View your pages here</a></p>|;

sub print_sort_pages {
	my ($sortBy,@databases) = @_;
	$site->{sortby} = $sortBy;
	@databases = $site->sort_data(@databases);
	$site->create_sort_pages(@databases);
}
