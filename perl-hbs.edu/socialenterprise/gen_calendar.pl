#!/usr/local/bin/perl

use strict;
use CGI qw(:standard);
use CGI::Carp "fatalsToBrowser";
use POSIX 'strftime';
use lib '../../system/lib/';
use lib '../../system/lib/cpan';
use SharePoint::RenderList;

my $DEBUG = 0;
my $siteroot = "../../../htdocs/wwwhbs";
my $calendar_xml = $ARGV[0] ? RenderHelper::slurp($ARGV[0]) : param('calendar_xml');
RenderHelper::write_file("$siteroot/socialenterprise/calendar.xml",$calendar_xml);
die "No Calendar XML Provided" if !$calendar_xml;
my $calendar = SPList->new($calendar_xml);
my $listurl  = "https://inside.hbs.edu/sites/deptshare/ITG/WebandIntranet/WebContent/Lists/SEICalendar/EditForm.aspx";
my $postback = "https://inside.hbs.edu/sites/deptshare/ITG/WebandIntranet/WebContent/SEIPostbacks/calendar.html";
print "Content-type: text/plain\n\n" if $DEBUG;
sub render_item {
    my $item = shift;
    my %vars = $item->vars();
    my %templatevars;
    my %mapping;
    my $itemVisible = $item->val("Visible");
    #reformat dates to get time - AM
    $templatevars{StartTime} = $item->datefmt("EventDate","%l:%M %p");
    $templatevars{EndTime} = $item->datefmt("EndDate","%l:%M %p");
    $templatevars{EventTypeCSS} = $item->val("EventType0");
    $templatevars{EventTypeCSS} = make_css_class($templatevars{EventTypeCSS});
    $templatevars{DisplayDate} = $item->datefmt("EventDate","%m.%d.%y");
    $templatevars{class} = $mapping{$item->val("DateType")};
    $templatevars{EditURL} = qq|<!-- url="$listurl?ID=$vars{ID}&Source=$postback" label="EDIT EVENT" -->|;
    my $enddate = $item->datefmt("EndDate","%m.%d.%y");
    if ($enddate ne $templatevars{DisplayDate}) { $templatevars{DisplayDate} .= "&ndash; <br />$enddate";}
    $templatevars{DisplayDate} =~ s/0(\d)/$1/g;    
    $templatevars{dtstart} = $item->datefmt("EventDate","%y-%m-%d");
    $templatevars{visible} = $item->val("Visible");
    # if there is a link add it to the title - AM
    my $LinkForEvent = $item->val("Link");
    my $EventTitle = $item->val("LinkTitle");
    $templatevars{EventTitle} = $EventTitle;
    if ($LinkForEvent) {
        $templatevars{EventTitle} = qq|<a href="$LinkForEvent">$EventTitle</a>|;
    }   
    my $html = $item->render("../../../htdocs/wwwhbs/socialenterprise/templates/event.phtml",\%templatevars);
    return $html;
}
sub compare_dates {
    #**created by becca
    #**you can compare two dates when they are in format of YYmmdd
    my $date1 = $_[0];
    my($day, $month, $year) = split('-', $date1);
    #**creating a number out of event start time vals to use in return value
    my $event_start = sprintf('%04d%02d%02d', $year, $month, $day);
    my $today = strftime '%d-%m-%Y', localtime;
    ($day, $month, $year) = split('-', $today);
    #**creating a number out of today's date vals to use in return value
    my $todays_date = sprintf('%04d%02d%02d', $year, $month, $day);
    return ($event_start >= $todays_date);	
}
sub make_css_class {
    #strip whitespace, put in lowercase - EventType0 - for CSS Class - RD
    my $class_val = $_[0];
    $class_val = lc($class_val);
    $class_val =~ s/\s+//g;
    return $class_val;	
}
#**for ten most recent
my $calendar_html = "<table class=\"bigcalendar ten_most_recent\">\n";
#**for frontpagegen file
my $home_cal_html;
#**for view all view gen file
my $full_list_calendar_html = "<table class=\"bigcalendar full_list_calendar\">\n";
#**for tracking number of current events
my $count = 0;
#**for counting all events
my $tracker = 0;
for my $item ($calendar->items) {
    my %vars = $item->vars();
    next if $vars{Visible} eq "No";
    next if $vars{Visible} eq "DevOnly" && $ENV{SERVER_NAME} !~ /webdev/;
    my $eventTypeCSSClass = $item->val("EventType0");
    $eventTypeCSSClass = make_css_class($eventTypeCSSClass);
    my $compareMonth = $item->datefmt("EventDate","%d-%m-%Y");
    #for the homecalendar.gen file--gets top three recent items
    my $title = $item->val("LinkTitle");
    my $itemURL = $item->val("Link");
    my $itemVisible = $item->val("Visible");
    my $month = $item->datefmt("EventDate","%B %d, %Y");
    if(compare_dates($compareMonth) && $count < 10) {
    	#gets the ten most recent items or less and prints to calendar.gen file
    	#based on whether event start date is greater >= then todays date
	$calendar_html .= render_item($item);
	$count++;
    }
    if ($tracker < 3) {
    	#for the homecalendar.gen file--gets top three recent items
    	$home_cal_html .= qq|<p><a href="$itemURL">$month</a><br/>$title</p>|;
    }
    $full_list_calendar_html .= render_item($item);
    $tracker++;
}
$calendar_html .= "</table>\n";
$full_list_calendar_html .= "</table>\n";
RenderHelper::write_file("$siteroot/socialenterprise/mission-and-impact/calendar.gen.html",$calendar_html);
RenderHelper::write_file("$siteroot/socialenterprise/mission-and-impact/calendar_full_list.gen.html",$full_list_calendar_html);
RenderHelper::write_file("$siteroot/socialenterprise/frontpagecalendar.gen.html",$home_cal_html);
if (!$DEBUG) {
   print redirect(cookie("LastEditURL") || "http://$ENV{SERVER_NAME}/socialenterprise/");
}





