import requests
import urllib.request

#this is a simple screenscraper using python3.5 that logs into a site and downloads some files
#using ssl encryption tls1.2

with requests.Session() as s:

    url = 'https://www.domainnamehere.org/user.html'
    values = {'email' : 'username@fas.harvard.edu',
          	  'password' : 'passwordhere',
              'referer' : '',
              'op' : 'Sign in >' }
    
    r = s.post(url, data=values)

    #http://stackoverflow.com/questions/25415405/downloading-an-excel-file-from-the-web-in-python
    
    fileName = 'data/filenamehere.xls'
    fileName2 = 'data/ilenamehere.xls'
    
    req = s.get('https://www.domainnamehere.org/admin_users.html?filter_id=1&action_id=20')
    req2 = s.get('https://www.domainnamehere.org/admin_store.html?xls=1')    
    
    #fileName
    file = open(fileName, 'wb')
    for chunk in req.iter_content(100000):
        file.write(chunk)
    file.close()    
    
    #fileName2
    file2 = open(fileName2, 'wb')
    for chunk in req2.iter_content(100000):
        file2.write(chunk)
    file2.close()        
    
