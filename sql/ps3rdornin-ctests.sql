-- ******************************************************
-- ps3ctests.sql
--
-- Constraint Tester for Inventory Control Database
--
-- Description:	This script contains tests
--              for each of the not null, 
--		    foreign key, primary key, range, and
--			default value constraints
--
-- Author:  Rebecca Dornin
--
-- Date:   October 19, 2005
--
-- ******************************************************

-- ******************************************************
-- Testing range and not null constraints
-- ******************************************************

--(1) Testing Component noPartsReq Constraint
INSERT into tbComponent values(100,01,01,11);

--(2) Testing Component compNo Range Constraint
INSERT into tbComponent values(100,100,01,11);

--(3) Testing Component compNo not null Constraint
INSERT into tbComponent values(100,null,01,11);

--(4) Testing Component prodNo not null Constraint
INSERT into tbComponent values(null,01,01,11);

--(5) Testing Part qoh range constraint
INSERT into tbPart values (01,'Tub',1001);

--(6) Testing Part partno range constraint
INSERT into tbPart values (100,'Tub',10);

--(7) Testing Part partno not null constraint
INSERT into tbPart values (null,'Tub',10);

--(8) Testing Part partdesc not null constraint
INSERT into tbPart values (01,null,10);

--(9) Testing Product prodno range constraint
INSERT into tbProduct values (200,'Wheelbarrow');

--(10) Testing Product prodno not null constraint
INSERT into tbProduct values (null,'Wheelbarrow');

--(11) Testing Product proddesc not null constraint
INSERT into tbProduct values (100,null);

--(12) Testing Quote pricequote range constraint
INSERT into tbQuote values (123,01,-1);

--(13) Testing Quote partno not null constraint
INSERT into tbQuote values (990,null,50.00);

--(14) Testing Quote vendorno not null constraint
INSERT into tbQuote values (null,01,50.00);

--(15) Testing Shipment quantity range constraint
INSERT into tbShipment values (seq_shipmentadder.nextval,123,01,1001,SYSDATE);

--(16) Testing Shipment partno not null constraint
INSERT into tbShipment values (seq_shipmentadder.nextval,123,null,10,SYSDATE);

--(17) Testing Shipment vendorno not null constraint
INSERT into tbShipment values (seq_shipmentadder.nextval,null,01,10,SYSDATE);

--(18) Testing Shipment shipmentno not null constraint
INSERT into tbShipment values (null,123,01,10,SYSDATE);

--(19) Testing Vendor vendorno range constraint
INSERT into tbVendor values (1001,'ABC Co.','Boston');

--(20) Testing Vendor vendorname not null constraint
INSERT into tbVendor values (123,null,'Boston');

--(21) Testing Vendor vendorno not null constraint
INSERT into tbVendor values (null,'ABC Co.','Boston');

-- ******************************************************
-- Testing primary key constraints
-- ******************************************************

--(22) Component Table PK constraints
INSERT into tbComponent values(100,01,01,1);

--(23) Part Table PK constraints
INSERT into tbPart values (01,'Tub',10);

--(24) Product Table PK constraints
INSERT into tbProduct values (100,'Wheelbarrow');

--(25) Quote Table PK constraints
INSERT into tbQuote values (123,01,50.00);

--(26) Shipment Table PK constraints
INSERT into tbShipment values (2,123,01,500,SYSDATE);

--(27) Vendor Table PK constraints
INSERT into tbVendor values (123,'ABC Co.','Boston');

-- ******************************************************
-- Testing default value constraints
-- ******************************************************

-- (28) Testing Component nopartsReq default of 1
INSERT into tbComponent(prodNo,compNo,partNo)
values(100,08,01);
select * from tbComponent
where compNo = 08;
DELETE from tbComponent
where compNo = 08;


-- (29) Testing Quote priceQuote default of 0.00
INSERT into tbQuote(vendorNo,partNo)
values(456,98);
select * from tbQuote
where vendorNo = 456;
DELETE from tbQuote
where vendorNo = 456 and partNo = 98;


-- (30) Testing Shipment quantity default of one
--INSERT into tbShipment values (seq_shipmentadder.nextval,123,01,500,SYSDATE);
INSERT into tbShipment(shipmentNo,vendorNo,partNo,shipmentDate)
values(seq_shipmentadder.nextval,123,01,SYSDATE);
select * from tbShipment
where vendorNo = 123 and partNo = 01;
DELETE from tbShipment
where vendorNo = 123 and partNo = 01 and quantity = 1;

-- ******************************************************
-- Testing foreign key constraints
-- ******************************************************

-- (31) testing prodno fk constraint
INSERT into tbComponent values(133,01,01,1);

-- (32) testing partno fk constraint
INSERT into tbComponent values(101,10,33,1);

-- (33) Testing shipment vendorno fk constraint
INSERT into tbShipment values (seq_shipmentadder.nextval,333,01,500,SYSDATE);

-- (34) Testing shipment partno fk constraint
INSERT into tbShipment values (seq_shipmentadder.nextval,123,04,500,SYSDATE);

-- (36) Testing quote partno fk constraint
INSERT into tbQuote values (333,01,50.00);

-- (37) Testing quote vendorno fk constraint
INSERT into tbQuote values (123,04,50.00);

-- ******************************************************
-- Testing Cascade Deletes
-- ******************************************************

-- (38) testing the component prodno on delete cascade
select * from tbProduct;
select * from tbComponent;
DELETE from tbProduct
where prodNo = 100;
select * from tbProduct;
select * from tbComponent;


-- (39) testing the quote partno on delete cascade
select * from tbPart;
select * from tbQuote;
DELETE from tbPart
where partNo = 03;
select * from tbPart;
select * from tbQuote;






