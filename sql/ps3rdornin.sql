-- ******************************************************
-- ps3rdornin.sql
--
-- Loader for Inventory Control Database
--
-- Description:	This script contains the DDL to load
--              the tables of the
--		    INVENTORY database
--
-- There are 6 tables on this DB
--
-- Author:  Rebecca Dornin
--
-- Date:   October 19, 2005
--
-- ******************************************************

-- ******************************************************
--    SPOOL SESSION
--	    Creates a transcript of the session
--
--	Note:
--		For this to work properly, you must:
--		    set echo on OR create a login.sql file
--
--	Syntax:
--		SPOOL <session_name>.lst
-- ******************************************************
set echo on;
set serveroutput on;

spool ps3rdornin.lst

-- ******************************************************
--    CREATE SCHEMA
--	     Creates a collection of tables, views, and
--		     privileges as a single transaction
--
--              You could create all tables under one
--              CREATE schema command, without
--              considering the order of the tables
--              The downside is that if something goes
--              wrong, nothing will be created.
--
--	Syntax:
--		CREATE schema authorization <username>
-- ******************************************************

   CREATE schema authorization rdornin;

-- ******************************************************
--    DROP TABLES
--	   Drops tables you own
--		 Tables are droped in the opposite order
--			in which they were created
--
--	Note:
--		Script will throw errors the first time
--		       it runs, as the tables do not exist
--
--	Syntax:
--		DROP table <tablename> purge
-- ******************************************************


DROP table tbComponent purge;
DROP table tbShipment purge;
DROP table tbQuote purge;
DROP table tbProduct purge;
DROP table tbPart purge;
DROP table tbVendor purge;

-- ******************************************************
--    DROP SEQUENCES
--	   Drops sequences you own
--		 There is no specific order to drop sequences
--
--	Note:
--		Script will throw errors the first time
--		       it runs, as the sequences do not exist
--
--	Syntax:
--		DROP sequence <sequence_name>
-- ******************************************************

   DROP sequence seq_shipmentadder;


-- ******************************************************
--    CREATE TABLES
--	     Create new tables
--		    Must create free entities first,
--			 followed by basic entities,
--				  to allow for referential integrity constraints
--  
--	Independent (basic) tables must be created first
--
--	Syntax:
--		CREATE table <table_name> ...
--		       (see Reference Manual for more detail)
-- ******************************************************

CREATE table tbVendor (
       vendorNo	     		char(3)			not null
       		constraint     	rg_vendorNo  	check(vendorNo between '100' and '999')
       constraint	    	pk_vendor		primary key,       
       vendorName			varchar2(25)	not null,
       vendorcity			varchar2(15)	null
);
CREATE table tbPart (
       partNo	     char(2)				not null
       constraint    rg_partNo				check(partNo between '01' and '99')
       constraint    pk_part				primary key,
       partDesc	     varchar2(15)  			not null,
       qoh	     	 number(3)	   			null
       constraint    rg_qoh	  				check (qoh between '0' and '1000') 
);
CREATE table tbProduct(
       prodNo		char(3)					not null
       constraint   rg_prodno				check(prodNo between '100' and '199')
       constraint   pk_product				primary key,
       prodDesc	    varchar2(15)			not null       
);
CREATE table tbQuote (
       	vendorNo			char(3)			not null
			constraint 		fk_vendorNo_tbQuote references tbVendor(vendorNo),
       	partNo	     		char(2)			not null
			constraint		fk_partNo_tbQuote references tbPart(partNo) on delete cascade,		
       	priceQuote    		number(11,2)    default 0.00		null
      		 constraint     rg_priceQuote	check (priceQuote >= 0.00),
			 constraint		pk_quote		primary key (partNo, vendorNo)
);
CREATE table tbShipment (
		shipmentNo			number(9,0)					not null
			constraint		pk_shipment					primary key,				
		vendorNo			char(3)						not null,
		partNo				char(2)						not null,						
		quantity			number(4)					default 1	null
			constraint		rg_quantity  				check(quantity between '1' and '1000'),
		shipmentDate		date						null,		
		constraint   fk_vendorNo_partNo_tbShipment foreign key (vendorNo, partNo) references tbQuote (vendorNo, partNo)
);
CREATE table tbComponent (
		prodNo				char(3)		not null
			constraint		fk_prodNo_tbComponent references tbProduct(prodNo) on delete cascade,
		compNo				char(2)						not null
			constraint		rg_compNo		check  (compNo between '01' and '99'),
		partNo				char(2)						null
			constraint		fk_partNo_tbComponent references tbPart(partNo),
		noPartsReq			number(2)		default 1 		null						
			constraint		rg_noPartsReq check (noPartsReq between '1' and '10'),
			constraint		pk_Component			primary key  (prodNo, compNo)
);


-- ******************************************************
--    CREATE SEQUENCES
--		Autonumber fields need a sequence
--
--	Syntax:
-- 		CREATE sequence <sequence_name>
-- 			increment by <increments>
-- 			start with <start_number>;
--
-- 	To call:
--		<sequence_name>.currval   OR
--		<sequence_name>.nextval
-- ******************************************************

CREATE sequence seq_shipmentadder
    increment by 1
    start with 1;

    
-- ******************************************************
--    POPULATE TABLES
--
-- populate tables in the same order
-- in which they were created
-- ******************************************************

--Inventory tbVendor
INSERT into tbVendor values ('123','ABC Co.','Boston');
INSERT into tbVendor values ('456','XYZ Co.','Waltham');
INSERT into tbVendor values ('789','Wheel Co.','Cambridge');
INSERT into tbVendor values ('990','WeGotIt','Boston');

--Inventory tbPart
INSERT into tbPart values ('01','Tub',10);
INSERT into tbPart values ('03','Wheel',50);
INSERT into tbPart values ('96','Box',12);
INSERT into tbPart values ('98','Strut',10);
INSERT into tbPart values ('99','Handle',30);

--Inventory tbProduct
INSERT into tbProduct values ('100','Wheelbarrow');
INSERT into tbProduct values ('101','Cart');

--Inventory tbQuote
INSERT into tbQuote values ('123','01',50.00);
INSERT into tbQuote values ('123','98',10.00);
INSERT into tbQuote values ('456','99',9.00);
INSERT into tbQuote values ('789','03',25.00);
INSERT into tbQuote values ('990','01',60.00);
INSERT into tbQuote values ('990','03',20.00);
INSERT into tbQuote values ('990','96',60.00);
INSERT into tbQuote values ('990','98',15.00);
INSERT into tbQuote values ('990','99',5.00);

--Inventory tbShipment
INSERT into tbShipment values (seq_shipmentadder.nextval,'123','01',500,SYSDATE);
INSERT into tbShipment values (seq_shipmentadder.nextval,'456','99',300,SYSDATE);
INSERT into tbShipment values (seq_shipmentadder.nextval,'789','03',200,SYSDATE);
INSERT into tbShipment values (seq_shipmentadder.nextval,'990','01',399,SYSDATE);

--Inventory tbComponent
INSERT into tbComponent values('100','01','01',1);
INSERT into tbComponent values(100,'02','03',1);
INSERT into tbComponent values(100,'03','98',1);
INSERT into tbComponent values(100,'04','99',2);
INSERT into tbComponent values(101,'01','03',2);
INSERT into tbComponent values(101,'02','96',1);
INSERT into tbComponent values(101,'03','98',1);
INSERT into tbComponent values(101,'04','99',2);

-- ******************************************************
--    VIEW TABLES
-- ******************************************************

SELECT * FROM tbVendor;
SELECT * FROM tbPart;
SELECT * FROM tbProduct;
SELECT * FROM tbQuote;
SELECT * FROM tbShipment;
SELECT * FROM tbComponent;


-- ******************************************************
--    QUALITY CONTROLS
--
--    This comprises 2 steps:
--		1) Run qc script, and
--		2) Perform table testing
-- ******************************************************

--   @ps3rdornin-ctests.sql


-- ******************************************************
--    END SESSION
--		Closes the transcript file
--
--	Note:
--		If you forget to do it,
--		 ORACLE will close it when you log out
--
--	Syntax:
--		SPOOL off
-- ******************************************************

spool off

