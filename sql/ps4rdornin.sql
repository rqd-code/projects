set echo on;
set serveroutput on;
spool ps4rdornin.lst

-- ******************************************************
-- ps4rdornin.sql
--
-- Learning DML With Inventory Control DB
--
-- Description:	This script contains the DML 
-- 		for probs 1-10 in ps4
--
-- There are 6 tables on this DB
--
-- Author:  Rebecca Dornin
--
-- Date:   Nov. 10, 2005
--
-- ******************************************************

-- ******************************************************
--    SPOOL SESSION
--	    Creates a transcript of the session
--
--	Note:
--		For this to work properly, you must:
--		    set echo on OR create a login.sql file
--
--	Syntax:
--		SPOOL <session_name>.lst
-- ******************************************************



-- ******************************************************
-- Query One
-- ******************************************************

--using a nested subquery
select distinct p.prodDesc
from tbProduct p, tbComponent c
where p.prodNo = (select prodNo 
	from tbComponent
	where partNo = '01');


--using a join
select prodDesc
from tbProduct p, tbComponent c
where c.partNo = '01' and p.prodNo = c.prodNo;


-- ******************************************************
-- Query Two
-- ******************************************************

--using not in
select vendorName
from tbVendor
where vendorNo not in ( select vendorNo from tbQuote
			where partNo = '03');	       

--using not exists
select distinct vendorName
from tbVendor vend
where not exists ( select * 
		   from tbVendor v,tbQuote q
		   where v.vendorNo = q.vendorNo and v.vendorNo = vend.vendorNo and q.partNo = '03');


-- ******************************************************
-- Query Three
-- ******************************************************

--using not exists and subqueries
select VendorName
from tbVendor
where vendorNo in (select distinct vendorNo from tbQuote a
		           where not exists ( select * from tbPart b where not exists
						      (select * from tbQuote c where
						       a.vendorNo = c.vendorNo and b.partNo = c.partNo)));					    
--using a view with subqueries
create view partView
(num_of_parts)
as select count(partNo)
from tbPart;
      
select vendorName
from tbVendor
where vendorNo = ( select distinct vendorNo
					from tbQuote
					group by vendorNo		
					having count(vendorNo) = (select num_of_parts
		     					          from partView));

drop view partView;
-- ******************************************************
-- Query Four
-- ******************************************************

select distinct p.prodDesc, sum(noPartsReq * q.priceQuote) as Cost
from tbComponent c, tbQuote q, tbProduct p
where q.vendorNo = '990' and c.partNo = q.partNo
      and p.prodNo = c.prodNo
group by p.prodDesc;

-- ******************************************************
-- Query Five
-- ******************************************************

savepoint x;

--inserting values into values into part table
insert into tbPart values('50','Bolt',17);
insert into tbPart values('51','Nut',20);
insert into tbPart values('55','Screw',17);
insert into tbPart values('66','Paperclip',10);
insert into tbPart values('67','Cord',3);

--using the (+) syntax
select c.partNo, partDesc, prodNo, c.compNo
from tbPart p, tbComponent c
where p.partNo = c.partNo(+)
order by 3,1,2;

--using the outer join syntax
select c.partNo, partDesc, prodNo, c.compNo
from tbPart p LEFT OUTER JOIN tbComponent c
on (p.partNo = c.partNo)
order by 3,1,2;

-- ******************************************************
-- Query Six
-- ******************************************************


select distinct a.vendorNo, a.vendorName, b.vendorNo, b.vendorName, a.vendorCity
from tbVendor a, tbVendor b
where a.vendorCity = b.vendorCity and b.vendorNo > a.vendorNo;

savepoint y;

insert into tbVendor values ('444','Third Company','Boston');

select distinct a.vendorNo, a.vendorName, b.vendorNo, b.vendorName, a.vendorCity
from tbVendor a, tbVendor b
where a.vendorCity = b.vendorCity and b.vendorNo > a.vendorNo;

rollback to savepoint y;						       


-- ******************************************************
-- Query Seven
-- ******************************************************
 
select p.partNo, p.partDesc
from tbQuote q, tbPart p
where q.partNo(+) = p.partNo
group by p.partNo, p.partDesc
having count(q.vendorNo) < 2;

insert into tbPart values('80','peddle',30);

select p.partNo, p.partDesc
from tbQuote q, tbPart p
where q.partNo(+) = p.partNo
group by p.partNo, p.partDesc
having count(q.vendorNo) < 2;

delete from
tbPart where partNo = '80';

select p.partNo, p.partDesc
from tbQuote q, tbPart p
where q.partNo(+) = p.partNo
group by p.partNo, p.partDesc
having count(q.vendorNo) < 2;

rollback to savepoint x;

-- ******************************************************
-- Query Eight
-- ******************************************************

select vendorNo
from tbVendor
where vendorCity = 'Boston'
UNION
select vendorNo 
from tbQuote
where partNo = '99' and priceQuote is not null;

-- ******************************************************
-- Query Nine
-- ******************************************************

select vendorName, partNo,priceQuote
from tbVendor ven, tbQuote quo
where ven.vendorNo = quo.vendorNo and quo.priceQuote = (select min(priceQuote)
						        from tbQuote
							where partNo = quo.partNo);

-- ******************************************************
-- Query Ten
-- ******************************************************

alter table tbProduct
add (prodSchedule number(2) null);

update tbProduct
set prodSchedule = 3 
where prodNo = '100';

update tbProduct
set prodSchedule = 2
where prodNo = '101';

select distinct pt.partDesc, pt.qoh / sum(c.noPartsReq * pd.prodSchedule) as Weeks_Til_Runout
from tbComponent c,tbPart pt, tbProduct pd
where pt.partNo = c.partNo and c.prodNo = pd.prodNo
group by pt.partDesc, pt.qoh;

alter table tbProduct
drop column prodSchedule;



-- ******************************************************
--    END SESSION
--		Closes the transcript file
--
--	Note:
--		If you forget to do it,
--		 ORACLE will close it when you log out
--
--	Syntax:
--		SPOOL off
-- ******************************************************

spool off

