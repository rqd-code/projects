set serveroutput on;
create or replace procedure list_ship(whs_id in varchar2)
is
--this cursor is for the all case mostly
cursor ware_hs_id is
select warehouse_id from warehouse;
--getting all the records into a cursor
id_no_exist EXCEPTION;
cursor ware_hs is
	select s.shipment_id,s.shipment_date,to_char(s.arrival_date) as arrival_date,w.warehouse_id, count(sl.item_no) as total_items
	from warehouse w, shipment s, shipment_line sl
	where s.warehouse_id(+) = w.warehouse_id
	and s.shipment_id = sl.shipment_id(+)
	group by w.warehouse_id,s.shipment_id,s.shipment_date,s.arrival_date;	
TB constant varchar2(5):=chr(9);
id_check shipment.warehouse_id%type;
begin
if whs_id != 'ALL' then
--making sure the id exists
select warehouse_id into id_check from warehouse
where warehouse_id = whs_id;
if id_check is null then
	raise id_no_exist;
end if;

for ware_hs_rec in ware_hs_id loop
--printing out warehouse id if it exists
	if ware_hs_rec.warehouse_id = whs_id then
 dbms_output.put_line('Warehouse ID: ' || whs_id);
 end if;
end loop;

	for item_rec in ware_hs loop	
		if (item_rec.warehouse_id = whs_id and item_rec.shipment_id is not null) then
			if (item_rec.arrival_date is null) then
			item_rec.arrival_date := lpad('NULL',9);
			end if;
		dbms_output.put_line('Shipment ID: ' || item_rec.shipment_id || TB || 'Shipment_Date: ' || item_rec.shipment_date || TB || 'Arrival Date: ' || item_rec.arrival_date || TB || 'Total Items: ' || item_rec.total_items);
		end if;
	end loop;
elsif whs_id = 'ALL' then
for ware_hs_rec in ware_hs_id loop
dbms_output.put_line('Warehouse_id: ' || ware_hs_rec.warehouse_id);	
	for item_rec in ware_hs loop
		if (item_rec.warehouse_id = ware_hs_rec.warehouse_id and item_rec.shipment_id is not null) then
		dbms_output.put_line('Shipment ID: '||item_rec.shipment_id||TB||'Shipment_Date: '||item_rec.shipment_date || TB || 'Arrival Date: ' || item_rec.arrival_date || TB || 'Total Items: ' || item_rec.total_items);
		end if;
	end loop;
	dbms_output.put_line('---------------------');
end loop;
end if;
exception when id_no_exist then
	dbms_output.put_line('That id does not exist.');
when no_data_found then
	dbms_output.put_line('That id does not exist.');
when OTHERS then
	dbms_output.put_line('failed operation, sqlcode:' ||sqlcode||',sqlerrm: ' || sqlerrm);
	dbms_output.put_line('');
end;
/



select distinct sl.shipment_id,s.shipment_date,s.arrival_date, w.warehouse_id, count(sl.item_no) as Num_Items
from warehouse w, shipment s, shipment_line sl
where s.warehouse_id = w.warehouse_id
and s.shipment_id = sl.shipment_id
group by sl.shipment_id,s.shipment_date,s.arrival_date, w.warehouse_id;

select distinct sl.shipment_id,s.shipment_date,s.arrival_date, w.warehouse_id, nvl(sl.item_no,0)
from warehouse w, shipment s, shipment_line sl
where s.warehouse_id = w.warehouse_id
and s.shipment_id = sl.shipment_id;

select distinct s.shipment_date,s.arrival_date, w.warehouse_id
from warehouse w, shipment s
where s.warehouse_id = w.warehouse_id
order by w.warehouse_id;