SQL> 
SQL> set serveroutput on;
SQL> create or replace trigger sl_ai_trigger
  2  after insert on shipment_line
  3  for each row
  4   
  5  declare
  6   
  7  new_item shipment_line.item_no%type;
  8  new_warehouse shipment.warehouse_id%type;
  9  old_qoh inventory.quantity_on_hand%type;
 10  new_qoh inventory.quantity_on_hand%type;
 11   
 12  begin
 13  --connect shipment_line table to shipment to inventory
 14  
 15    --getting the new warehouse_id
 16    select warehouse_id into new_warehouse
 17    from shipment s
 18    where s.shipment_id = :new.shipment_id;
 19    
 20     dbms_output.put_line(new_warehouse);
 21   
 22    select quantity_on_hand into old_qoh
 23    from inventory
 24    where item_no = :new.item_no and warehouse_id = new_warehouse;
 25    
 26    update inventory
 27    set quantity_on_hand = old_qoh - :new.quantity
 28    where item_no = :new.item_no and warehouse_id = new_warehouse;
 29    
 30  end;
 31  /

Trigger created.

SQL>  
SQL> show error;
No errors.
SQL> 
SQL> 
SQL> 
SQL> 
SQL> 
SQL> 
SQL> 
SQL>  delete from shipment_line where shipment_id = '00-0004';

0 rows deleted.

SQL>  
SQL>  insert into shipment_line
  2   values ('00-0004','7829',4);
1-003

1 row created.

SQL>  select * from inventory where item_no = '7829';

ITEM_ WAREH QUANTITY_ON_HAND REORDER_LEVEL REORDER_DIFF
----- ----- ---------------- ------------- ------------
7829  1-001               33             5            4
7829  1-002                6             5            6
7829  1-003               12            15            5
7829  1-004                6             5            6
7829  1-005               -2             5            5

SQL>  delete from shipment_line where shipment_id = '00-0004';

1 row deleted.

SQL>  
SQL>  insert into shipment_line
  2   values ('00-0004','7829',4);
1-003

1 row created.

SQL>  
SQL>  select * from inventory where item_no = '7829';

ITEM_ WAREH QUANTITY_ON_HAND REORDER_LEVEL REORDER_DIFF
----- ----- ---------------- ------------- ------------
7829  1-001               33             5            4
7829  1-002                6             5            6
7829  1-003                8            15            5
7829  1-004                6             5            6
7829  1-005               -2             5            5