SQL> spool hw4_output
SQL> exec list_ship('bla');
That id does not exist.

PL/SQL procedure successfully completed.

SQL> exec list_ship('ALL');
Warehouse_id: 1-001
Shipment ID: 00-0001    Shipment_Date: 10-JAN-00        Arrival Date: 14-JAN-00 Total Items:
Shipment ID: 00-0003    Shipment_Date: 20-JAN-00        Arrival Date: 31-JAN-00 Total Items:
Shipment ID: 00-0005    Shipment_Date: 01-FEB-00        Arrival Date:   Total Items: 0
Shipment ID: 89-0002    Shipment_Date: 15-JAN-89        Arrival Date: 21-JAN-89 Total Items:
Shipment ID: 92-0001    Shipment_Date: 18-JAN-92        Arrival Date: 30-JAN-92 Total Items:
Shipment ID: 99-0003    Shipment_Date: 13-MAR-99        Arrival Date: 18-MAR-99 Total Items:
---------------------
Warehouse_id: 1-002
---------------------
Warehouse_id: 1-003
Shipment ID: 00-0004    Shipment_Date: 31-JAN-00        Arrival Date:   Total Items: 2
Shipment ID: 89-0001    Shipment_Date: 15-JAN-89        Arrival Date: 21-JAN-89 Total Items:
---------------------
Warehouse_id: 1-004
Shipment ID: 00-0006    Shipment_Date: 09-FEB-00        Arrival Date:   Total Items: 2
---------------------
Warehouse_id: 1-005
Shipment ID: 00-0002    Shipment_Date: 15-JAN-00        Arrival Date: 19-JAN-00 Total Items:
---------------------

PL/SQL procedure successfully completed.

SQL> exec list_ship('1-002');
Warehouse ID: 1-002

PL/SQL procedure successfully completed.

SQL> exec list_ship('1-001');
Warehouse ID: 1-001
Shipment ID: 00-0001    Shipment_Date: 10-JAN-00        Arrival Date: 14-JAN-00 Total Items:
Shipment ID: 00-0003    Shipment_Date: 20-JAN-00        Arrival Date: 31-JAN-00 Total Items:
Shipment ID: 00-0005    Shipment_Date: 01-FEB-00        Arrival Date:      NULL Total Items:
Shipment ID: 89-0002    Shipment_Date: 15-JAN-89        Arrival Date: 21-JAN-89 Total Items:
Shipment ID: 92-0001    Shipment_Date: 18-JAN-92        Arrival Date: 30-JAN-92 Total Items:
Shipment ID: 99-0003    Shipment_Date: 13-MAR-99        Arrival Date: 18-MAR-99 Total Items:

PL/SQL procedure successfully completed.

SQL> exec list_ship('1-003');
Warehouse ID: 1-003
Shipment ID: 00-0004    Shipment_Date: 31-JAN-00        Arrival Date:      NULL Total Items:
Shipment ID: 89-0001    Shipment_Date: 15-JAN-89        Arrival Date: 21-JAN-89 Total Items:

PL/SQL procedure successfully completed.

SQL> exec list_ship('1-004');
Warehouse ID: 1-004
Shipment ID: 00-0006    Shipment_Date: 09-FEB-00        Arrival Date:      NULL Total Items:

PL/SQL procedure successfully completed.

SQL> exec reorder('I');
Item_No Warehouse_id    Supplier_Id     Current         Reorder
                                        Quantity        Amount
==============================================================
2101    1-001           3-001   5               25
2101    1-002           3-001   23              7
2109    1-001           3-001   3               12
2109    1-002           3-001   2               14
3212    1-001           4-001   0               31
3212    1-002           4-001   12              13
3223    1-003           4-001   19              3
3297    1-003           4-001   5               10
4532    1-001           2-002   11              20
4532    1-002           2-002   11              20
4532    1-003           2-002   8               23
4533    1-001           2-001   22              15
4533    1-002           2-001   10              25
4533    1-003           2-001   23              13
7821    1-001           5-001   2               13

PL/SQL procedure successfully completed.

SQL> exec reorder('S');
Supplier_id: 2-001
Item_No Warehouse_id    Current         Reorder
                        Quantity        Amount
==============================================================
4533    1-001           22              15
4533    1-002           10              25
4533    1-003           23              13
Supplier_id: 2-002
Item_No Warehouse_id    Current         Reorder
                        Quantity        Amount
==============================================================
4532    1-001           11              20
4532    1-002           11              20
4532    1-003           8               23
Supplier_id: 3-001
Item_No Warehouse_id    Current         Reorder
                        Quantity        Amount
==============================================================
2101    1-001           5               25
2101    1-002           23              7
2109    1-001           3               12
2109    1-002           2               14
Supplier_id: 4-001
Item_No Warehouse_id    Current         Reorder
                        Quantity        Amount
==============================================================
3212    1-001           0               31
3212    1-002           12              13
3223    1-003           19              3
3297    1-003           5               10
Supplier_id: 5-001
Item_No Warehouse_id    Current         Reorder
                        Quantity        Amount
==============================================================
7821    1-001           2               13

PL/SQL procedure successfully completed.

SQL> exec reorder('W');
Warehouse_id: 1-001
Item_No Supplier_id     Current         Reorder
                        Quantity        Amount
==============================================================
2101    3-001           5               25
2109    3-001           3               12
3212    4-001           0               31
4532    2-002           11              20
4533    2-001           22              15
7821    5-001           2               13
Warehouse_id: 1-002
Item_No Supplier_id     Current         Reorder
                        Quantity        Amount
==============================================================
2101    3-001           23              7
2109    3-001           2               14
3212    4-001           12              13
4532    2-002           11              20
4533    2-001           10              25
Warehouse_id: 1-003
Item_No Supplier_id     Current         Reorder
                        Quantity        Amount
==============================================================
3223    4-001           19              3
3297    4-001           5               10
4532    2-002           8               23
4533    2-001           23              13

PL/SQL procedure successfully completed.

SQL> spool off