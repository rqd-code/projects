<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" omit-xml-declaration="yes"/>

<!-- ########### CONTENT_DATA ########### -->

<xsl:template name="contentData">
    The html of the document
</xsl:template>

<!-- ########### CONTENT_DATA_A ########### -->

<xsl:template name="contentDataA"> 
    The html of the document
</xsl:template>

<!-- ########### CONTENT_DATA_B ########### -->

<xsl:template name="contentDataB">
    The html of the document
</xsl:template>

<!-- ########### CONTENT TAGS ############ -->

<xsl:template match="review-body">
    <xsl:apply-templates />
</xsl:template>

<xsl:template match="article-body">
	<xsl:apply-templates></xsl:apply-templates>
</xsl:template>

<xsl:template match="related/*">
	<xsl:apply-templates></xsl:apply-templates>
</xsl:template>

<xsl:template match="pull-quote-anchor">
<xsl:variable name="pqid"><xsl:value-of select="@idref" /></xsl:variable>	
   <blockquote><xsl:attribute name="align"><xsl:value-of select="@align"/></xsl:attribute><xsl:value-of select="/content-unit/*/editorial-matter/pull-quote[@id=normalize-space($pqid)]"/></blockquote> 
</xsl:template>

<xsl:template match="interview">
	<xsl:apply-templates></xsl:apply-templates>
</xsl:template>

<xsl:template match="interviewer">
<h6>Interviewer:</h6>
<xsl:value-of select="name/given-name"></xsl:value-of><xsl:text> </xsl:text><xsl:value-of select="name/family-name"></xsl:value-of>
</xsl:template>

<xsl:template match="interviewee">
<h6>Interviewer:</h6>
<xsl:value-of select="name/given-name"></xsl:value-of><xsl:text> </xsl:text><xsl:value-of select="name/family-name"></xsl:value-of>
</xsl:template>

<xsl:template match="question"><h6>Question:</h6><xsl:apply-templates /></xsl:template>

<xsl:template match="answer"><h6>Answer:</h6><xsl:apply-templates /></xsl:template>

<xsl:template match="p">
    <p><xsl:apply-templates /></p>
</xsl:template>

<xsl:template match="em">

<xsl:choose>
	<xsl:when test="@type='i'">
    <i><xsl:apply-templates /></i>
	</xsl:when>
	<xsl:when test="@type='b'">
    <b><xsl:apply-templates /></b>
	</xsl:when>
</xsl:choose>
</xsl:template>


<xsl:template match="list">
   <ul><xsl:apply-templates /></ul>
</xsl:template>

<xsl:template match="item">
   <li><xsl:apply-templates /></li>
</xsl:template>

<xsl:template match="em">
    <i><xsl:apply-templates /></i>
</xsl:template>

<xsl:template match="web-ref">
   <a>
   <xsl:attribute name="href"><xsl:value-of select="@url"/></xsl:attribute>
   <xsl:apply-templates />
   </a>
</xsl:template>

</xsl:stylesheet>