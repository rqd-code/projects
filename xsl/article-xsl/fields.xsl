<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" omit-xml-declaration="yes"/>

<!-- ########### ITEM_ID ########### -->
<xsl:template name="item_id">
    <xsl:value-of select="substring-after(/content-unit/@id,'wk_')" />
</xsl:template>

<!-- ########### TITLE ########### -->

<xsl:template name="title">
    <xsl:if test="/content-unit/article">
        <xsl:apply-templates select="/content-unit/article/title-grp/title"/>
    </xsl:if>

    <xsl:if test="/content-unit/web-review">
        <xsl:apply-templates select="/content-unit/web-review/related-material/related[@relationship='is-review-of']/work/title-grp/title"/>
    </xsl:if>
 
    <xsl:if test="/content-unit/book-review">
        <xsl:apply-templates select="/content-unit/book-review/related-material/related[@relationship='is-review-of']/work/title-grp/title"/>
    </xsl:if>
</xsl:template>

<xsl:template match="title">
    <xsl:apply-templates />
</xsl:template>

<xsl:template match="subtitle">
    <xsl:text>: </xsl:text><xsl:apply-templates />
</xsl:template>

<!-- ########### SUBT1 ########### -->

<xsl:template name="subt1">
    <xsl:if test="/content-unit/web-review">
    </xsl:if>
 
    <xsl:if test="/content-unit/book-review">
    </xsl:if>
</xsl:template>

<!-- ########### SUBT2 ########### -->

<xsl:template name="subt2">
</xsl:template>

<!-- ########### PRODUCT_TYPE ########### -->

<xsl:template name="product_type">WK</xsl:template>

<!-- ########### CONTENT_STATUS ########### -->

<xsl:template name="content_status">PUB</xsl:template>

<!-- ########### ITEM_TYPE ########### -->

<xsl:template name="item_type">
<xsl:if test="/content-unit/article">ARTICLE</xsl:if>

    <xsl:if test="/content-unit/web-review">WEB SITE</xsl:if>
 
    <xsl:if test="/content-unit/book-review">BOOK REV</xsl:if>
</xsl:template>

<!-- ########### ORG_ID ########### -->

<xsl:template name="org_id">
    <xsl:if test="/content-unit/book-review">
        <xsl:variable name="oid"><xsl:value-of select="/content-unit/book-review/related-material/related[@relationship='is-review-of']/work/contribution-grp/contributor[@role='publisher']/@entid" /></xsl:variable>
        <xsl:choose>
            <xsl:when test="normalize-space($oid)"><xsl:value-of select="$oid"/></xsl:when>
            <xsl:otherwise>-1</xsl:otherwise>
        </xsl:choose>
    </xsl:if>
    
    <xsl:if test="/content-unit/web-review">
        <xsl:variable name="oid"><xsl:value-of select="/content-unit/web-review/related-material/related[@relationship='is-review-of']/work/contribution-grp/contributor[@role='publisher']/@entid"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="normalize-space($oid)"><xsl:value-of select="$oid"/></xsl:when>
            <xsl:otherwise>345345</xsl:otherwise>
        </xsl:choose>
    </xsl:if>
</xsl:template>

<!-- ########### URL ########### -->

<xsl:template name="url">
    <xsl:if test="/content-unit/web-review">
      <xsl:value-of select="/content-unit/web-review/related-material/related/work/work-url" />
    </xsl:if>
 
    <xsl:if test="/content-unit/book-review"></xsl:if>
</xsl:template>

<!-- ########### AUTHOR_TYPE ########### -->

<xsl:template name="author_type">
</xsl:template>

<!-- ########### AUTHOR1 ########### -->


<xsl:template name="author1">
<xsl:if test="/content-unit/article">
<xsl:value-of select="/content-unit/article/contribution-grp/contributor[1][@role='author']/name/given-name" /> <xsl:text> </xsl:text> <xsl:value-of select="/content-unit/article/contribution-grp/contributor[1][@role='author']/name/family-name" /> 
</xsl:if>    
    
<xsl:if test="/content-unit/book-review">
    <xsl:variable name="pid"><xsl:value-of select="/content-unit/book-review/related-material/related[@relationship='is-review-of']/work/contribution-grp/contributor[1][@role!='publisher']/@entid" /></xsl:variable>
    
    <xsl:choose>
        <xsl:when test="normalize-space($pid)"><xsl:value-of select="$pid"/></xsl:when>
        <xsl:otherwise>-1</xsl:otherwise>
    </xsl:choose>
</xsl:if>    


</xsl:template>

<!-- ########### AUTHOR2 ########### -->

<xsl:template name="author2">
    <xsl:variable name="pid"><xsl:value-of select="/content-unit/book-review/related-material/related[@relationship='is-review-of']/work/contribution-grp/contributor[2][@role!='publisher']/@entid" /></xsl:variable>
    
    <xsl:choose>
        <xsl:when test="normalize-space($pid)"><xsl:value-of select="$pid"/></xsl:when>
        <xsl:otherwise>-1</xsl:otherwise>
    </xsl:choose>

</xsl:template>

<!-- ########### AUTHOR3 ########### -->

<xsl:template name="author3">
    <xsl:variable name="pid"><xsl:value-of select="/content-unit/book-review/related-material/related[@relationship='is-review-of']/work/contribution-grp/contributor[3][@role!='publisher']/@entid" /></xsl:variable>

    <xsl:choose>
        <xsl:when test="normalize-space($pid)"><xsl:value-of select="$pid"/></xsl:when>
        <xsl:otherwise>-1</xsl:otherwise>
    </xsl:choose>

</xsl:template>

<!-- ########### AUTHOR4 ########### -->

<xsl:template name="author4">-1</xsl:template>

<!-- ########### AUTHOR5 ########### -->

<xsl:template name="author5">-1</xsl:template>

<!-- ########### AUTHOR6 ########### -->

<xsl:template name="author6">-1</xsl:template>

<!-- ########### AUTHOR JOB-TITLE ########### -->
<xsl:template name="job-title">
<xsl:value-of select="/content-unit/article/contribution-grp/contributor/contributor-info/affiliation/job-title"></xsl:value-of>
</xsl:template>


<!-- ########### PUBLISHED DATE ########### -->

<xsl:template name="publishedDate">
    <xsl:apply-templates select="/content-unit/*/date[@type='publication']" />
</xsl:template>

<xsl:template match="date">
    <xsl:value-of select="month"/>/<xsl:value-of select="day"/>/<xsl:value-of select="year"/>
</xsl:template>

<!-- ########### TOPIC ########### -->

<xsl:template name="ins_insight">

    
    <xsl:if test="/content-unit/*/about-grp/wk-topics/topic[@type='primary' and @topic='business-history']">(5) Business History</xsl:if>
    <xsl:if test="/content-unit/*/about-grp/wk-topics/topic[@type='primary' and @topic='career-effectiveness']">(14) Career Effectiveness</xsl:if>
    <xsl:if test="/content-unit/*/about-grp/wk-topics/topic[@type='primary' and @topic='entrepreneurship']">(4) Entrepreneurship</xsl:if>
    <xsl:if test="/content-unit/*/about-grp/wk-topics/topic[@type='primary' and @topic='finance']">(1) Finance</xsl:if>
    <xsl:if test="/content-unit/*/about-grp/wk-topics/topic[@type='primary' and @topic='globalization']">(7) Globalization</xsl:if>
    <xsl:if test="/content-unit/*/about-grp/wk-topics/topic[@type='primary' and @topic='innovation']">(6) Innovation</xsl:if>
    <xsl:if test="/content-unit/*/about-grp/wk-topics/topic[@type='primary' and @topic='leadership']">(13) Leadership</xsl:if>
    <xsl:if test="/content-unit/*/about-grp/wk-topics/topic[@type='primary' and @topic='marketing']">(9) Marketing</xsl:if>
    <xsl:if test="/content-unit/*/about-grp/wk-topics/topic[@type='primary' and @topic='operations']">(11) Operations</xsl:if>
    <xsl:if test="/content-unit/*/about-grp/wk-topics/topic[@type='primary' and @topic='organizations']">(12) Organizations</xsl:if>
    <xsl:if test="/content-unit/*/about-grp/wk-topics/topic[@type='primary' and @topic='social-enterprise']">(10) Social Enterprise</xsl:if>
    <xsl:if test="/content-unit/*/about-grp/wk-topics/topic[@type='primary' and @topic='strategy']">(24) Strategy</xsl:if>
    <xsl:if test="/content-unit/*/about-grp/wk-topics/topic[@type='primary' and @topic='technology']">(25) Technology</xsl:if>

<!--    
    <option  value=""> </option>
    <option  value=5>Business History</option>
    <option  value=2>Customers & Services</option>
    <option  value=3>E-commerce & Marketspace</option>
    <option  value=4>Entrepreneurship & Small Business</option>
    <option  value=7>Globalization</option>
    <option  value=6>Innovation & Change</option>
    
    <option  value=1>Investment & Finance</option>
    <option  value=8>Knowledge & the Information Economy</option>
    <option  value=13>Leadership, Strategy & Competition</option>
    <option  value=9>Marketing</option>
    <option  value=10>Nonprofits & Social Enterprise</option>
    <option  value=11>Operations & Technology</option>
    
    <option  value=12 selected>Organizations & People</option>
    <option  value=14>Careers</option>
    <option  value=15>Special Reports</option>
    <option  value=16>Heskett</option>
    <option  value=17>Dispatches</option>
    <option  value=18>Letters To The Editor</option>
    <option  value=19>Legends</option>
    <option  value=20>Global Initiative</option>
    
    <option  value=21>Notebook</option>
    <option  value=22>Career Effectiveness</option>
    <option  value=24>Strategy</option>
    <option  value=25>Technology</option>
    <option  value=26>Crisis Management</option>
    <option  value=27>Corporate Governance</option>
    <option  value=28>Moral Leadership</option>
    <option  value=29>Managing Uncertainty</option>
    <option  value=30>The Leaders Workshop</option>
    
    <option  value=31>Audio Conferences</option>
    <option  value=32>Managing Recovery</option>
    <option  value=33>Negotiation</option>
    <option  value=34>Outsourcing</option>
-->

    
</xsl:template>

<!-- ########### INS_ENTERED_BY ########### -->

<xsl:template name="ins_entered_by">
</xsl:template>

<!-- ########### ORIG PUB DATE ########### -->

<xsl:template name="origPubDate">
</xsl:template>

<!-- ########### COPYRIGHT DATE ########### -->

<xsl:template name="copyrightDate">
    <xsl:if test="/content-unit/book-review">
        <xsl:value-of select="/content-unit/book-review/related-material/related[@relationship='is-review-of']/work/date[@type='publication']/year" />
    </xsl:if>
</xsl:template>


<!-- ########### INS_PULLQUOTE_1_F2 ########### -->

<xsl:template name="ins_pullquote_1_f2">
    <xsl:if test="/content-unit/book-review">
    </xsl:if>
</xsl:template>

<!-- ########### HEADLINE2 ########### -->

<xsl:template name="headline2">
    <xsl:if test="/content-unit/web-review">
        <xsl:value-of select="/content-unit/web-review/related-material/related[@relationship='is-review-of']/work/title-grp/title" />
    </xsl:if>
</xsl:template>

<!-- ########### HEADLINELINK ########### -->

<xsl:template name="headlineLink">
    <xsl:if test="/content-unit/web-review">
        <xsl:value-of select="/content-unit/web-review/related-material/related[@relationship='is-review-of']/work/work-url" />
    </xsl:if>
</xsl:template>

<!-- ########### BLURB2 ########### -->

<xsl:template name="blurb2">

    <xsl:if test="/content-unit/web-review">
        <xsl:apply-templates select="/content-unit/web-review/review-body" />
    </xsl:if>
    
    <xsl:if test="/content-unit/book-review">
        <xsl:apply-templates select="/content-unit/book-review/review-body" />
    </xsl:if>
 
    <xsl:if test="/content-unit/article">
        <xsl:apply-templates select="/content-unit/article/article-body" />
    </xsl:if>   
    
</xsl:template>

<!-- ########### INS_PURCHASE_LINK_F2 ########### -->

<xsl:template name="ins_purchase_link_f2">
    <xsl:if test="/content-unit/book-review">
        <xsl:value-of select="/content-unit/book-review/related-material/related[@relationship='is-review-of']/work/work-buyurl" />
    </xsl:if>
</xsl:template>

<!-- ########### PULLQUOTE1 ########### -->

<xsl:template name="pullquote1">
    <xsl:if test="/content-unit/web-review">
        <xsl:apply-templates select="/content-unit/web-review/editorial-matter/blurb/*" />
    </xsl:if>
</xsl:template>

<!-- ########### RELATED-MATERIAL ########### -->
<xsl:template name="related-material">
  <xsl:for-each select="/content-unit/*/related-material/related">
	  <xsl:text>Relationship: </xsl:text><xsl:value-of select="@relationship"></xsl:value-of><br/>
	  <xsl:text>Article Ref Idref: </xsl:text><xsl:value-of select="substring-after(article-ref/@idref,'wk_')"></xsl:value-of><br/>
	  <xsl:text>Article Ref type: </xsl:text><xsl:value-of select="article-ref/@type"></xsl:value-of><br/>  
			  <xsl:apply-templates select="/content-unit/*/related-material/related/*" />
  </xsl:for-each>
</xsl:template>


</xsl:stylesheet>