<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:template name="wkedit">

            
            <form method="post" action="http://mimosa.hbs.edu/xmlpub/item_edit.jhtml?_DARGS=%2Fwkedit%2Fitem_edit.jhtml.1"  style="display:inline">

            <input type="hidden" name="/hbs/wk/ui/ItemEditHandler.ctItem.itemId">
                <xsl:attribute name="value"><xsl:call-template name="item_id" /></xsl:attribute>            
             </input>
            <input type="hidden" name="item_id_h">
                <xsl:attribute name="value"><xsl:call-template name="item_id" /></xsl:attribute>            
            </input>
            <input type="hidden" name="/hbs/wk/ui/ItemEditHandler.ctItem.itemId">
                <xsl:attribute name="value"><xsl:call-template name="item_id" /></xsl:attribute>            
            </input>
            <input type="hidden" name="_D:/hbs/wk/ui/ItemEditHandler.ctItem.itemId" value=" "/>

            <input type="hidden" name="title">
                <xsl:attribute name="value"><xsl:call-template name="title" /></xsl:attribute>            
            </input>
            <input type="hidden" name="_D:title" value=" "/>

            <input type="hidden" name="subt1" value=""/>
            <input type="hidden" name="_D:subt1" value=" "/>

            <input type="hidden" name="subt2" value=""/>
            <input type="hidden" name="_D:subt2" value=" "/>

            <input type="hidden" name="product_type">
                <xsl:attribute name="value"><xsl:call-template name="product_type" /></xsl:attribute>            
            </input>
            <input type="hidden" name="_D:product_type" value=" "/>

            <input type="hidden" name="content_status">
                <xsl:attribute name="value"><xsl:call-template name="content_status" /></xsl:attribute>
            </input>
            <input type="hidden" name="_D:content_status" value=" "/>

            <input type="hidden" name="item_type">
                <xsl:attribute name="value"><xsl:call-template name="item_type" /></xsl:attribute>
            </input>
            <input type="hidden" name="_D:item_type" value=" "/>

            <input type="hidden" name="org_id">
                <xsl:attribute name="value"><xsl:call-template name="org_id" /></xsl:attribute>
            </input>
            <input type="hidden" name="_D:org_id" value=" "/>

            <input type="hidden" name="author1">
                <xsl:attribute name="value"><xsl:call-template name="author1" /></xsl:attribute>
            </input>
            <input type="hidden" name="_D:author1" value=" "/>

            <input type="hidden" name="author2">
                <xsl:attribute name="value"><xsl:call-template name="author2" /></xsl:attribute>
            </input>
            <input type="hidden" name="_D:author2" value=" "/>

            <input type="hidden" name="author3">
                <xsl:attribute name="value"><xsl:call-template name="author3" /></xsl:attribute>
            </input>
            <input type="hidden" name="_D:author3" value=" "/>

            <input type="hidden" name="author4">
                <xsl:attribute name="value"><xsl:call-template name="author4" /></xsl:attribute>
            </input>
            <input type="hidden" name="_D:author4" value=" "/>

            <input type="hidden" name="author5">
                <xsl:attribute name="value"><xsl:call-template name="author5" /></xsl:attribute>
            </input>
            <input type="hidden" name="_D:author5" value=" "/>

            <input type="hidden" name="author6">
                <xsl:attribute name="value"><xsl:call-template name="author6" /></xsl:attribute>
            </input>
            <input type="hidden" name="_D:author6" value=" "/>

            <input type="hidden" name="publishedDate">
                <xsl:attribute name="value"><xsl:call-template name="publishedDate" /></xsl:attribute>
            </input>
            <input type="hidden" name="_D:publishedDate" value=" "/>

            <input type="hidden" name="ins_insight">
                <xsl:attribute name="value"><xsl:call-template name="ins_insight" /></xsl:attribute>
            </input>
            <input type="hidden" name="_D:ins_insight" value=" "/>

            <input type="hidden" name="ins_entered_by">
                <xsl:attribute name="value"></xsl:attribute>
            </input>
            <input type="hidden" name="_D:ins_entered_by" value=" "/>
            
            <xsl:if test="/content-unit/book-review">

                <input type="hidden" name="origPubDate">
                    <xsl:attribute name="value"><xsl:call-template name="origPubDate" /></xsl:attribute>
                </input>
                <input type="hidden" name="_D:origPubDate" value=" "/>    

                <input type="hidden" name="copyrightDate">
                    <xsl:attribute name="value"><xsl:call-template name="copyrightDate" /></xsl:attribute>
                </input>
                <input type="hidden" name="_D:copyrightDate" value=" "/>    

                <input type="hidden" name="ins_pullquote_1_f2">
                    <xsl:attribute name="value"><xsl:call-template name="ins_pullquote_1_f2" /></xsl:attribute>
                </input>
                <input type="hidden" name="_D:ins_pullquote_1_f2" value=" "/> 

                <textarea style="display:none" name="/hbs/wk/ui/ItemEditHandler.ctInsightContent.blurb2"><xsl:call-template name="blurb2" /></textarea>
                <input type="hidden" name="_D:/hbs/wk/ui/ItemEditHandler.ctInsightContent.blurb2" value=" "/> 

                <input type="hidden" name="ins_topic_page_pic_f2">
                    <xsl:attribute name="value"></xsl:attribute>
                </input>
                <input type="hidden" name="_D:ins_topic_page_pic_f2" value=" "/> 

                <input type="hidden" name="ins_purchase_link_f2">
                    <xsl:attribute name="value"></xsl:attribute>
                </input>
                <input type="hidden" name="_D:ins_purchase_link_f2" value=" "/> 

                <input type="hidden" name="ins_purchase_link_graphic_f2">
                    <xsl:attribute name="value"></xsl:attribute>
                </input>
                <input type="hidden" name="_D:ins_purchase_link_graphic_f2" value=" "/> 
            </xsl:if>
            
            <xsl:if test="/content-unit/web-review">

                <input type="hidden" name="/hbs/wk/ui/ItemEditHandler.ctInsightContent.headline2">
                    <xsl:attribute name="value"><xsl:call-template name="headline2" /></xsl:attribute>
                </input>
                <input type="hidden" name="_D:/hbs/wk/ui/ItemEditHandler.ctInsightContent.headline2" value=" "/> 

                <textarea style="display:none" name="/hbs/wk/ui/ItemEditHandler.ctInsightContent.blurb2"><xsl:call-template name="blurb2" /></textarea>
                <input type="hidden" name="_D:/hbs/wk/ui/ItemEditHandler.ctInsightContent.blurb2" value=" "/> 

                <input type="hidden" name="/hbs/wk/ui/ItemEditHandler.ctInsightContent.headlineLink">
                    <xsl:attribute name="value"><xsl:call-template name="headlineLink" /></xsl:attribute>
                </input>
                <input type="hidden" name="_D:/hbs/wk/ui/ItemEditHandler.ctInsightContent.headlineLink" value=" "/> 

                <input type="hidden" name="/hbs/wk/ui/ItemEditHandler.ctInsightContent.pullquote1">
                    <xsl:attribute name="value"><xsl:call-template name="pullquote1" /></xsl:attribute>
                </input>
                <input type="hidden" name="_D:/hbs/wk/ui/ItemEditHandler.ctInsightContent.pullquote1" value=" "/> 

                <input type="hidden" name="/hbs/wk/ui/ItemEditHandler.ctInsightContent.topicPagePic">
                    <xsl:attribute name="value"></xsl:attribute>
                </input>
                <input type="hidden" name="_D:/hbs/wk/ui/ItemEditHandler.ctInsightContent.topicPagePic" value=" "/> 
            </xsl:if>
            
            

            <input type="hidden" name="_D:/hbs/wk/ui/ItemEditHandler.submit" value=" " />
            <xsl:if test="not(/content-unit/article)">
            <input type="submit" value="Submit" name="/hbs/wk/ui/ItemEditHandler.submit" class="submit" id="submitbutton"/>
            </xsl:if>
            </form>

</xsl:template>




<xsl:template match="*">
    &lt;UNMATCHED&gt;<xsl:value-of select="name()"/><xsl:apply-templates />&lt;/UNMATCHED&gt;
</xsl:template>


</xsl:stylesheet>
