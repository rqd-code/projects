<?xml version="1.0"?>
<?altova_samplexml wk_0000.xml?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" omit-xml-declaration="yes"/>
	<xsl:key name="list" match="company" use="@reference"/>
	<xsl:include href="fields.xsl"/>
	<xsl:include href="contentData.xsl"/>
	<xsl:include href="form.xsl"/>
	<xsl:strip-space elements="*"/>
	<xsl:preserve-space elements=""/>
	<xsl:template match="/">
		<html>
			<head>
				<title>Preview</title>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				<style>
            body {font-family: Arial;font-size: 12px;}
            dt {font-weight: bold; border-bottom: 1px solid #eee;}
            iframe {display: none;}
           </style>
			</head>
			<body>
				<xsl:call-template name="wkedit"/>
				<dt>Notes</dt>
				<xsl:if test="/content-unit/article/note-grp/note">
					<dd>From: <xsl:value-of select="/content-unit/article/note-grp/note/@from"/>
					</dd>
					<dd>To: <xsl:value-of select="/content-unit/article/note-grp/note/@to"/>
					</dd>
					<dd>
						<xsl:value-of select="/content-unit/article/note-grp/note"/>
					</dd>
				</xsl:if>
				<xsl:if test="/content-unit/article[@column='fac_QandA']">
					<dt>
						<br/>This is a faculty QA</dt>
				</xsl:if>
				<xsl:if test="/content-unit/web-review">
					<div id="extract">
						<h2>Production Metadata</h2>
						<dl>
							<dt>ID</dt>
							<dd>
								<xsl:call-template name="item_id"/>
							</dd>
							<dt>Primary Topic</dt>
							<dd>
								<xsl:value-of select="/content-unit/*/about-grp/wk-topics/topic[@type='primary']/@topic"/>
							</dd>
							<dt>Secondary Topics</dt>
							<dd>
								<xsl:for-each select="/content-unit/*/about-grp/wk-topics/topic[@type='secondary']">
									<xsl:value-of select="./@topic"/>
								</xsl:for-each>
							</dd>
							<dt>Notes</dt>
							<dd>Coming Soon</dd>
							<dt>Metadata: Companies Mentioned</dt>
							<dd>
								<xsl:for-each select="/content-unit/*/about-grp/taxonomy-topics/company[generate-id(.)=generate-id(key('list',@reference))]/@reference">
									<xsl:for-each select="key('list',.)">
										<xsl:sort/>
										<li>
											<xsl:value-of select="."/>
										</li>
									</xsl:for-each>
								</xsl:for-each>
							</dd>
						</dl>
					</div>
				</xsl:if>
				<h2>Document Information</h2>
				<dl>
					<dt>Title</dt>
					<dd>
						<xsl:call-template name="title"/>
					</dd>
					<dt>ID</dt>
					<dd>
						<xsl:call-template name="item_id"/>
					</dd>
					<dt>Subtitle 1</dt>
					<dd>
						<xsl:call-template name="subt1"/>
					</dd>
					<dt>Subtitle 2</dt>
					<dd>
						<xsl:call-template name="subt2"/>
					</dd>
					<dt>Product Type</dt>
					<dd>
						<xsl:call-template name="product_type"/>
					</dd>
					<dt>Content Status</dt>
					<dd>
						<xsl:call-template name="content_status"/>
					</dd>
					<dt>Item Type</dt>
					<dd>
						<xsl:call-template name="item_type"/>
					</dd>
					<dt>Org ID</dt>
					<dd>
						<xsl:call-template name="org_id"/>
					</dd>
					<dt>URL</dt>
					<dd>
						<xsl:call-template name="url"/>
					</dd>
					<dt>Author Type</dt>
					<dd>
						<xsl:call-template name="author_type"/>
					</dd>
					<dt>Author 1</dt>
					<dd>
						<xsl:call-template name="author1"/>
					</dd>
					<dt>Author 2</dt>
					<dd>
						<xsl:call-template name="author2"/>
					</dd>
					<dt>Author 3</dt>
					<dd>
						<xsl:call-template name="author3"/>
					</dd>
					<dt>Author 4</dt>
					<dd>
						<xsl:call-template name="author4"/>
					</dd>
					<dt>Author 5</dt>
					<dd>
						<xsl:call-template name="author5"/>
					</dd>
					<dt>Author 6</dt>
					<dd>
						<xsl:call-template name="author6"/>
					</dd>
										<dt>Job Title</dt>
					<dd>
						<xsl:call-template name="job-title"/>
					</dd>
					<dt>WK Published Date</dt>
					<dd>
						<xsl:call-template name="publishedDate"/>
					</dd>
					<dt>Topic</dt>
					<dd>
						<xsl:call-template name="ins_insight"/>
					</dd>
					<dt>Note</dt>
					<dd>
						<xsl:call-template name="ins_entered_by"/>
					</dd>
						<dt>Related Material</dt>
					<dd><xsl:call-template name="related-material"></xsl:call-template></dd>
					
					<xsl:if test="/content-unit/article">
						<dt>Article Body</dt>
						<dd>
							<xsl:call-template name="blurb2"/>
						</dd>	
					</xsl:if>
					<xsl:if test="/content-unit/book-review">
						<dt>OrigPubDate</dt>
						<dd>
							<xsl:call-template name="origPubDate"/>
						</dd>
						<dt>Copyright Year</dt>
						<dd>
							<xsl:call-template name="copyrightDate"/>
						</dd>
						<dt>Book Tag Line</dt>
						<dd>
							<xsl:call-template name="ins_pullquote_1_f2"/>
						</dd>
						<dt>Book Blurb</dt>
						<dd>
							<xsl:call-template name="blurb2"/>
						</dd>
						<dt>Book Image</dt>
						<dt>Purchase Link</dt>
						<dd>
							<xsl:call-template name="ins_purchase_link_f2"/>
						</dd>
						<dt>Purchase Link Graphic</dt>
					</xsl:if>
					<xsl:if test="/content-unit/web-review">
						<dt>Topic Page Headline</dt>
						<dd>
							<xsl:call-template name="headline2"/>
						</dd>
						<dt>Review Text</dt>
						<dd>
							<xsl:call-template name="blurb2"/>
						</dd>
						<dt>URL</dt>
						<dd>
							<xsl:call-template name="headlineLink"/>
						</dd>
						<dt>Tag Line</dt>
						<dd>
							<xsl:call-template name="pullquote1"/>
						</dd>
						<dt>Website Image</dt>
						<dd/>
					</xsl:if>
				</dl>
				<iframe src="http://mimosa.hbs.edu/wkedit/item_edit.jhtml?item_id=4777&amp;mode=update"/>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="*">
    &lt;UNMATCHED&gt;<xsl:value-of select="name()"/>
		<xsl:apply-templates/>&lt;/UNMATCHED&gt;
</xsl:template>
</xsl:stylesheet>
