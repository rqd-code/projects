<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:import href="lib/utils.xsl"/>
	<xsl:import href="lib/renderings.xsl"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<Events>
				<xsl:call-template name="query-events"/>
			</Events>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<xsl:template match="/Render">
		<div id="events-content">
			<div class="events">
				<ul class="media-list4">
					<xsl:for-each select="Events/hbs:QueryResults/z:row">
						<xsl:sort select="@ows_Date" order="ascending"/>
						<xsl:if test="position()=1 or substring(@ows_Date,6,2) != substring(preceding-sibling::z:row[1]/@ows_Date,6,2)">
							<xsl:if test="position()!=1">
								<div class="hr">&#160;</div>
							</xsl:if>
							<h3>
								<xsl:call-template name="month-fullname">
									<xsl:with-param name="month" select="substring(@ows_Date,6,2)"/>
								</xsl:call-template>&#160;<xsl:value-of select="substring(@ows_Date,1,4)" disable-output-escaping="yes"/>
							</h3>
						</xsl:if>
						<xsl:call-template name="render-event">
							<xsl:with-param name="item" select="."/>
						</xsl:call-template>
					</xsl:for-each>
				</ul>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>