﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
xmlns:hbs="urn:hbs"
xmlns:z="#RowsetSchema"
xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime"  
xmlns:msxsl="urn:schemas-microsoft-com:xslt"
exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:import href="lib/utils.xsl"/>
	<xsl:import href="lib/renderings.xsl"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<xsl:call-template name="query-alumni"/>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<xsl:template match="/Render">
		<xsl:for-each select="hbs:QueryResults/z:row">
			<xsl:variable name="SidebarPhoto"><xsl:call-template name="ExtractImagePath"><xsl:with-param name="imageTag" select="@ows_SidebarPhoto"></xsl:with-param></xsl:call-template></xsl:variable>
			<xsl:variable name="morelink"><xsl:choose>
				<xsl:when test="@ows_ProfileLookUp">/doctoral/research-community/students-faculty/Pages/profile-details.aspx?profile=<xsl:value-of select="substring-after(@ows_ProfileLookUp,'#')"/></xsl:when>
				<xsl:when test="@ows_MoreLink"><xsl:value-of select="@ows_MoreLink"/>
				</xsl:when>
			</xsl:choose></xsl:variable>
			<div class="sidebar-item" data-wcm-edit-url="/doctoral/Lists/AlumniSidebar/EditForm.aspx?ID={@ows_ID}">
				<xsl:if test="$SidebarPhoto">
					<img src="{$SidebarPhoto}" border="0" class="sidebar-photo"/>
				</xsl:if>
				<xsl:if test="@ows_FullName1">
					<div class="kappa-uc hbsred sidebar-personname">
						<a href="{normalize-space($morelink)}">
							<xsl:value-of select="@ows_FullName1" disable-output-escaping="yes"/>
						</a>
					</div>
				</xsl:if>
				<p>
					<strong class="mu-uc">Degree</strong>
					<br/>
					<xsl:if test="@ows_Degree">
						<xsl:value-of select="@ows_Degree" disable-output-escaping="yes"/>&#160;</xsl:if>
					<xsl:if test="@ows_Year">
						<xsl:value-of select="@ows_Year" disable-output-escaping="yes"/>
					</xsl:if>
				</p>
				<p>
					<xsl:if test="@ows_JobTitle1">
						<strong class="mu-uc">Position</strong>
						<br/>
						<xsl:value-of select="@ows_JobTitle1" disable-output-escaping="yes"/>
					</xsl:if>
					<xsl:if test="@ows_Placement">,<br/><xsl:value-of select="@ows_Placement" disable-output-escaping="yes"/></xsl:if>
				</p>
				<xsl:if test="@ows_ProfileBlurb">
					<p>
						<xsl:value-of select="@ows_ProfileBlurb" disable-output-escaping="yes"/>
					</p>
				</xsl:if>
				<xsl:if test="@ows_JobTitle">
					<p>
						<xsl:value-of select="@ows_JobTitle" disable-output-escaping="yes"/>
					</p>
				</xsl:if>
				<xsl:if test="@ows_MoreLink">
					<p class="more">
						<span class="txt-arrow">&#x2192; </span> <a><xsl:attribute name="href"><xsl:value-of select="@ows_MoreLink"/></xsl:attribute>More about <xsl:value-of select="@ows_FirstName1" disable-output-escaping="yes"/></a>
					</p>
				</xsl:if>
			</div>
			<div class="hr">
			</div>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
