﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:param name="args"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<!--
    
    Query Data
    
    -->
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<xsl:call-template name="query-topics"/>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<!--
    
    Render Output
    
    -->
	<xsl:template match="/Render">
		<div class="row" id="apply-dropmenu" data-wcm-edit-url="/doctoral/Lists/Topics/">
			<div class="span5">
				<label class="field-select">
					<span class="field-select-btn">
					</span>
					<select class="links" name="programs">
						<option value="" selected="selected">- Select area of Study -</option>
						<xsl:for-each select="hbs:QueryResults/z:row">
							<xsl:variable name="link"><xsl:value-of select="@ows_ApplyOnlineLink" disable-output-escaping="yes"/></xsl:variable>
							<option value="{$link}">
								<xsl:value-of select="@ows_Title" disable-output-escaping="yes"/>
							</option>
						</xsl:for-each>
					</select>
				</label>
			</div>
			<div class="span2">
				<input type="button" class="btn-submit admissions-apply" value="Apply Online"/>
			</div>
		</div>
		<div class="shim24">
		</div>
	</xsl:template>
</xsl:stylesheet>