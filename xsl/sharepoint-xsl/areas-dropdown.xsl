﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:param name="args"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<!--
    
    Query Data
    
    -->
	<xsl:template match="/Root">
		<Step1>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<xsl:copy-of select="hbs:Request/PATH_INFO"/>
			<hbs:Repeat/>
			<xsl:variable name="topicid"><xsl:choose><xsl:when test="contains(hbs:Request/PATH_INFO,'/accounting-management')">3</xsl:when>
						<xsl:when test="contains(hbs:Request/PATH_INFO,'/business-economics')">2</xsl:when>
						<xsl:when test="contains(hbs:Request/PATH_INFO,'/health-policy')">4</xsl:when>
						<xsl:when test="contains(hbs:Request/PATH_INFO,'/management')">6</xsl:when>
						<xsl:when test="contains(hbs:Request/PATH_INFO,'/marketing')">5</xsl:when>
						<xsl:when test="contains(hbs:Request/PATH_INFO,'/organizational-behavior')">7</xsl:when>
						<xsl:when test="contains(hbs:Request/PATH_INFO,'/strategy')">1</xsl:when>
						<xsl:when test="contains(hbs:Request/PATH_INFO,'/technology-operations-management')">8</xsl:when></xsl:choose>
				</xsl:variable>
			<Topic>
				<xsl:call-template name="query-topics-by-id">
					<xsl:with-param name="topicid" select="normalize-space($topicid)"/>
				</xsl:call-template>
			</Topic>
		</Step1>
	</xsl:template>
	<!--
    
    Render Output
    
    -->
	<xsl:template match="/Step1">
		<Render>
			<Students>
				<xsl:call-template name="query-current-students-by-topic">
					<xsl:with-param name="topic"><xsl:for-each select="Topic//z:row"><xsl:value-of select="normalize-space(@ows_Title)"/></xsl:for-each></xsl:with-param>
					<xsl:with-param name="localfolder"><xsl:for-each select="Topic//z:row"><xsl:value-of select="normalize-space(@ows_LocalFolder)"/></xsl:for-each></xsl:with-param>
				</xsl:call-template>
			</Students>
			<xsl:copy-of select="Topic"/>
			<xsl:copy-of select="PATH_INFO"/>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<!--
    
    Render Output 
   
    -->
	<xsl:template match="/Render">
		<xsl:variable name="columns"><xsl:choose><xsl:when test="contains(PATH_INFO,'/accounting-management')">2</xsl:when>
						<xsl:when test="contains(PATH_INFO,'/business-economics')">4</xsl:when>
						<xsl:when test="contains(PATH_INFO,'/health-policy')">1</xsl:when>
						<xsl:when test="contains(PATH_INFO,'/management')">2</xsl:when>
						<xsl:when test="contains(PATH_INFO,'/marketing')">2</xsl:when>
						<xsl:when test="contains(PATH_INFO,'/organizational-behavior')">4</xsl:when>
						<xsl:when test="contains(PATH_INFO,'/strategy')">2</xsl:when>
						<xsl:when test="contains(PATH_INFO,'/technology-operations-management')">3</xsl:when></xsl:choose>
			</xsl:variable>
		<xsl:variable name="split" select="ceiling(count(Students//z:row) div normalize-space($columns))"/>
		<div class="dropdown-container2" data-wcm-edit-url="/doctoral/Lists/CurrentStudents/">
			<a class="dropdown-toggle2 white mu-uc black-bg" href="#">
				<span class="icon-expand-inverted inherit-bg">
				</span>Current Students in this Program</a>
			<div class="hr">
			</div>
			<div>
				<xsl:attribute name="class">black-bg dropdown-menu2 dropdown-split<xsl:value-of select="normalize-space($columns)"/></xsl:attribute>
				<xsl:if test="$columns &lt;= 1"><xsl:attribute name="class">black-bg col1 dropdown-menu2 dropdown-split3</xsl:attribute></xsl:if>
				<ul>

					<xsl:for-each select="Students//z:row[position() &lt;= $split]">
						<xsl:call-template name="render-dropdown-item">
							<xsl:with-param name="item" select="."/>
						</xsl:call-template>
					</xsl:for-each>
				</ul>
				
				<xsl:if test="$columns &gt; 1">
					<ul>
						<xsl:for-each select="Students//z:row[position() &gt; $split and position() &lt;= ($split*2)]">
							<xsl:call-template name="render-dropdown-item">
								<xsl:with-param name="item" select="."/>
							</xsl:call-template>
						</xsl:for-each>
					</ul>
				</xsl:if>
				<xsl:if test="$columns &gt; 2">
					<ul>
						<xsl:for-each select="Students//z:row[position() &gt; ($split*2) and position() &lt;= ($split*3)]">
							<xsl:call-template name="render-dropdown-item">
								<xsl:with-param name="item" select="."/>
							</xsl:call-template>
						</xsl:for-each>
					</ul>
				</xsl:if>
				<xsl:if test="$columns &gt; 3">
					<ul>
						<xsl:for-each select="Students//z:row[position() &gt; ($split*3)]">
							<xsl:call-template name="render-dropdown-item">
								<xsl:with-param name="item" select="."/>
							</xsl:call-template>
						</xsl:for-each>
					</ul>
				</xsl:if>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="render-dropdown-item">
		<xsl:param name="item"/>
		<xsl:variable name="link">/faculty/Pages/profile.aspx?facId=<xsl:value-of select="$item/@ows_RISID"/></xsl:variable>
		<li>
			<a class="mu-uc white">
				<xsl:if test="$item/@ows_RISID != ''">
					<xsl:attribute name="href"><xsl:value-of select="$link"></xsl:value-of></xsl:attribute>
				</xsl:if>
				<xsl:value-of select="$item/@ows_FirstName1" disable-output-escaping="yes"/>&#160;<xsl:value-of select="$item/@ows_LastName" disable-output-escaping="yes"/></a>
		</li>
	</xsl:template>
</xsl:stylesheet>