﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
	<xsl:param name="args"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<!--
    
    Query Data
    
    -->
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<!--
    
    Render Output
    
    -->
	<xsl:template match="/Render">
		<div class="sidebar-item">
			<h3 class="kappa-uc">Suggest a Topic</h3>
			<script type="text/javascript" src="/doctoral/Style Library/hbs/js/plugins/jquery.validate.min.js">
			</script>
			<script type="text/javascript">
		        // <![CDATA[
					jQuery(function(){
						var thanks = window.location.search.substring(1);
						$("#thanks").hide();
						if (thanks == "thanks") {
							$("#thanks").show();
						}
					});
					// ]]>
		 </script>
			<div id="thanks">
				<p>Thank you for suggesting a topic for the Executive Directo’s blog. John Korn will be reviewing your comments and suggestions, and will be posting messages regularly. If you would like to ask specific program questions, please email <a class="to" href="#">doctoralprograms+hbs.edu</a>.</p>
			</div>
			<div method="post" action="http://hbsapps.hbs.edu/emailForm/process" class="validated form">
				<p>
					<label for="name" class="kappa">Name:</label>
					<br />
					<input name="Name" id="name" class="required field" size="22" type="text" />
				</p>
				<p>
					<label for="email" class="kappa">E-mail:</label>
					<br />
					<input size="22" maxlength="128" name="f" class="required email field" id="email" type="text" />
				</p>
				<p>
					<label for="comments" class="kappa">Suggestion:</label>
					<br />
					<textarea name="Question" class="required  field" id="comments" cols="21" rows="8">
					</textarea>
				</p>
				<p>
					<input name="openaccess" type="hidden"/>
					<input name="e" value="rdornin@hbs.edu,adaniell@hbs.edu,psanivada@hbs.edu,sagrant@hbs.edu" type="hidden"/>
					<input type="hidden" name="u" value="http://authdev.hbs.edu/doctoral/program-overview/Pages/from-the-executive-director.aspx?thanks" />
					<input type="hidden" name="aTemplate" value="http://www.hbs.edu/doctoral/blog/blog.email.admin.fml" />
					<input value="Submit" class="btn-submit" type="submit" />
				</p>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>