﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
xmlns:hbs="urn:hbs"
xmlns:z="#RowsetSchema"
xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime"  
xmlns:msxsl="urn:schemas-microsoft-com:xslt"
exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:import href="lib/utils.xsl"/>
	<xsl:import href="lib/renderings.xsl"/>
	<xsl:param name="args"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<xsl:call-template name="query-blogs"/>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<xsl:template match="/Render">
		<div class="blog">
			<xsl:choose>
				<xsl:when test="//@ows_Date">
					<xsl:for-each select="//z:row">
						<xsl:variable name="datestr" select="substring-before(@ows_Date,' ')"/>
						<xsl:variable name="monthnum" select="substring-before(substring-after($datestr,'-'),'-')"/>
						<xsl:variable name="daynum" select="substring-after(substring-after($datestr,'-'),'-')"/>
						<xsl:variable name="yearnum" select="substring-before($datestr,'-')"/>
						<xsl:variable name="monthstr">
							<xsl:call-template name="month-name">
								<xsl:with-param name="month" select="$monthnum"/>
							</xsl:call-template>
							</xsl:variable>
						<xsl:if test="position() &lt; 2">
							<div class="media"  data-wcm-edit-url="/doctoral/Lists/Blogs/EditForm.aspx?ID={@ows_ID}">
								<div class="row">
									<div class="span1">
										<img src="/doctoral/PublishingImages/john-korn-54wx50h.jpg" class="stroke3" alt="" width="54" height="50"/>&#160;</div>
									<div class="span7">
										<ul class="linear mu-uc">
											<li><xsl:value-of select="$daynum" disable-output-escaping="yes"/>&#160;<xsl:value-of select="$monthstr" disable-output-escaping="yes"/>&#160;<xsl:value-of select="$yearnum" disable-output-escaping="yes"/></li>
											<li><span class="ash">From the Exective Director</span></li>
										</ul>
										<h4 class="eta">
											<a class="hbsred" href="/doctoral/program-overview/Pages/from-the-executive-director.aspx">
												<xsl:value-of select="@ows_Title" disable-output-escaping="yes"/>
											</a>
										</h4>
										<div class="nu">By <xsl:value-of select="@ows_PostAuthor" disable-output-escaping="yes"/></div>
										<div class="shim15"></div>
										<p>
											<xsl:value-of select="@ows_HomeBlurb" disable-output-escaping="yes"/>&#160;<a class="hbsred" href="/doctoral/program-overview/Pages/from-the-executive-director.aspx">Full Post.</a></p>
									</div>
								</div>
							</div>
						</xsl:if>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<p>There are no current blog postings from the Executive Director.</p>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
