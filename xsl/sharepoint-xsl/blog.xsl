﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
xmlns:hbs="urn:hbs"
xmlns:z="#RowsetSchema"
xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime"  
xmlns:msxsl="urn:schemas-microsoft-com:xslt"
exclude-result-prefixes="hbs z msxsl"
>
	<xsl:import href="lib/queries.xsl"/>
	<xsl:import href="lib/utils.xsl"/>
	<xsl:import href="lib/renderings.xsl"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='blogs'">
				<hbs:Debug/>
			</xsl:if>
			<xsl:copy-of select="hbs:Request"/>
			<xsl:call-template name="query-blogs"/>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<xsl:template match="/Render">
		<div class="row">
			<div class="span9">
				<xsl:choose>
					<xsl:when test="hbs:Request/showall">
						<xsl:for-each select="//z:row">
							<xsl:call-template name="render-blog">
							</xsl:call-template>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<xsl:for-each select="//z:row">
							<xsl:if test="position() &lt;= 4">
								<xsl:call-template name="render-blog">
								</xsl:call-template>
							</xsl:if>
						</xsl:for-each>
						<div class="row">
							<div class="span9">
								<p class="more">
									<span class="txt-arrow">&#x2192;</span>&#160;<a href="?showall=1">Show All Postings</a>
								</p>
							</div>
						</div>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="render-blog">
		<xsl:variable name="datestr" select="substring-before(@ows_Date,' ')"/>
		<xsl:variable name="monthnum" select="substring-before(substring-after($datestr,'-'),'-')"/>
		<xsl:variable name="daynum" select="substring-after(substring-after($datestr,'-'),'-')"/>
		<xsl:variable name="yearnum" select="substring-before($datestr,'-')"/>
		<xsl:variable name="monthstr">
					<xsl:call-template name="month-name">
						<xsl:with-param name="month" select="$monthnum"/>
					</xsl:call-template>
				</xsl:variable>
		<a name="{$monthstr}{$daynum}{@ows_ID}" />
		<div class="row">
			<div class="span9">
				<div class="kappa-uc">
					<xsl:value-of select="$daynum" disable-output-escaping="yes"/>&#160;<xsl:value-of select="$monthstr" disable-output-escaping="yes"/>&#160;<xsl:value-of select="$yearnum" disable-output-escaping="yes"/></div>
				<xsl:if test="@ows_Title != '' and @ows_Title != 'Untitled'">
					<h3 class="eta post-title" data-wcm-edit-url="/doctoral/Lists/Blogs/EditForm.aspx?ID={@ows_ID}">
						<xsl:value-of select="@ows_Title" disable-output-escaping="yes"/>
					</h3>
				</xsl:if>
				<xsl:if test="@ows_Title = 'Untitled'">
					<h3 class="eta post-title" data-wcm-edit-url="/doctoral/Lists/Blogs/EditForm.aspx?ID={@ows_ID}">&#160;</h3>
				</xsl:if>
				<xsl:value-of select="@ows_Body" disable-output-escaping="yes"/>
			</div>
		</div>
		<div class="hr">&#160;</div>
	</xsl:template>
</xsl:stylesheet>
