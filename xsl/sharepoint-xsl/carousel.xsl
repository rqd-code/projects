﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:import href="lib/utils.xsl"/>
	<xsl:import href="lib/renderings.xsl"/>
	<xsl:param name="args"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<CarouselView>
				<xsl:call-template name="query-carouselview"/>
			</CarouselView>
			<Assets>
				<xsl:call-template name="query-assets"/>
			</Assets>
			<Profiles>
				<xsl:call-template name="query-profiles"/>			
			</Profiles>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<xsl:template match="/Render">
		<xsl:variable name="headerline"><xsl:value-of select="/Render/CarouselView/hbs:QueryResults/z:row/@ows_GlobalHeaderLine" disable-output-escaping="yes"/></xsl:variable>
<xsl:variable name="headerlineid"><xsl:value-of select="/Render/CarouselView/hbs:QueryResults/z:row/@ows_ID" disable-output-escaping="yes"/></xsl:variable>
		<xsl:variable name="AssetsItemsOrdered">
			<xsl:call-template name="build-assets">
			        <xsl:with-param name="list" select="CarouselView//z:row/@ows_RelatedAsset"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:for-each select="msxsl:node-set($AssetsItemsOrdered)/Item">
			<xsl:if test="position() &lt; 13">
				<xsl:variable name="homepageLargePhoto"><xsl:call-template name="ExtractImagePath"><xsl:with-param name="imageTag" select="@HomepageLargePhoto"></xsl:with-param></xsl:call-template></xsl:variable>
				<xsl:variable name="link"><xsl:value-of select="@URL"/></xsl:variable>
				<xsl:variable name="bgimage">background-image: url('<xsl:value-of select="normalize-space($homepageLargePhoto)"/>');</xsl:variable>
				<xsl:variable name="allprofileslink">/doctoral/research-community/students-faculty/Pages/default.aspx</xsl:variable>

				<div class="wrap largebg silver" style="{$bgimage}">
					<div class="container h1-alpha-uc h2-beta h3-delta">
						<div class="row">
							<div class="span8">
								<h1 class="heading" data-wcm-edit-url="/doctoral/Lists/CarouselView/EditForm.aspx?ID={$headerlineid}">
									<xsl:value-of select="$headerline" disable-output-escaping="yes"/>
								</h1>
								<div class="black eta">
									<blockquote style="text-indent:-10px; color:{@TextColor}">“<xsl:value-of select="@Quote" disable-output-escaping="yes"/>”</blockquote>
									<div class="shim20">&#160;</div>
									<!-- <a href="{$link}" class="btn-arrow">Read <xsl:value-of select="@FullName"/>&#8217;s Story <span class="right"></span></a> -->
									<a href="{$link}" class="btn-arrow"><xsl:value-of select="@FullName"/>, <xsl:value-of select="@Line2" disable-output-escaping="yes"/> <span class="right"></span></a>
									<div class="shim8">&#160;</div>
									<a href="{$allprofileslink}" class="btn-arrow">View All Profiles <span class="right"></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</xsl:if>
		</xsl:for-each>
		<div class="coral-bg accent-stripe">
			<div class="container" data-wcm-edit-url="/doctoral/Lists/CarouselView/EditForm.aspx?ID={CarouselView//z:row/@ows_ID}">
				<div class="row">
					<div class="span8">&#160;</div>
					<div class="span4">
						<div class="our-mission">
							<div class="shim21">&#160;</div>
							<xsl:value-of select="CarouselView//z:row/@ows_MissionContent" disable-output-escaping="yes"/>
						</div>
					</div>
				</div>
				<div class="row assets">
					<div class="span8">
						<xsl:for-each select="msxsl:node-set($AssetsItemsOrdered)/Item">
							<xsl:if test="position() &lt; 13">
								<xsl:variable name="link"><xsl:value-of select="@URL"/></xsl:variable>
								<xsl:variable name="thumbnailPhoto"><xsl:call-template name="ExtractImagePath"><xsl:with-param name="imageTag" select="@ThumbnailPhoto"></xsl:with-param></xsl:call-template></xsl:variable>
								<div class="home-thumbs item{position()-1}" data-wcm-edit-url="/doctoral/Lists/CarouselProfiles/EditForm.aspx?ID={@ID}"
		                   data-wcm-edit-label="Home Profile Asset"
		                   data-wcm-new-url="/doctoral/Lists/CarouselProfiles/NewForm.aspx?RootFolder=%2Fdoctoral%2FLists%2FCarouselProfiles&amp;ContentTypeId=0x0100423B721519FE4640A309386EF5CA2E180200E42D2D7C2CBD094FBF2E18DCE88953EE"
		                   data-wcm-new-label="Home Profile Asset">
									<xsl:if test="position() &lt; 2">
										<xsl:attribute name="class">home-thumbs active item<xsl:value-of select="position()-1"/></xsl:attribute>
									</xsl:if>
									<div class="assetImage">
										<img>
											<xsl:attribute name="src"><xsl:value-of select="$thumbnailPhoto" disable-output-escaping="yes"/></xsl:attribute>
											<xsl:attribute name="width">190</xsl:attribute>
											<xsl:attribute name="class">stroke3</xsl:attribute>
											<xsl:attribute name="height">118</xsl:attribute>
											<xsl:attribute name="alt"><xsl:value-of select="@Line1" disable-output-escaping="yes"/></xsl:attribute>
										</img>
									</div>
									<a href="#" class="thumb-link">
										<div class="white mu-uc  underline">
											<xsl:value-of select="@Line1" disable-output-escaping="yes"/>
										</div>
										<div class="black mu-uc">
											<xsl:value-of select="@Line2" disable-output-escaping="yes"/>
										</div>
									</a>
								</div>
							</xsl:if>
						</xsl:for-each>
					</div>
					<div class="span4">&#160;</div>
				</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="build-assets">
		<xsl:param name="list" />
		<xsl:variable name="newlist" select="concat(normalize-space($list), ' ')" />
		<xsl:variable name="first" select="substring-before($newlist, ';#')" />
		<xsl:variable name="remaining" select="substring-after($newlist, ';#')" />
		<xsl:variable name="newlist2" select="concat(normalize-space($remaining), ' ')" />
		<xsl:variable name="first2" select="substring-before($newlist2, ';#')" />
		<xsl:variable name="remaining2" select="substring-after($newlist2, ';#')" />
		<Item>
			<xsl:attribute name="Key"><xsl:value-of select="$first" /></xsl:attribute>
			<xsl:choose>
				<xsl:when test="$first2">
					<xsl:attribute name="Val"><xsl:value-of select="$first2" /></xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="Val"><xsl:value-of select="normalize-space($newlist2)" /></xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<!--match by ID and get the rest of the information-->
			<xsl:for-each select="/Render/Assets//z:row[@ows_ID=$first]">
				<xsl:variable name="space">&#160;</xsl:variable>
				<!--Attribute that we always pass in-->
				<xsl:attribute name="ID"><xsl:value-of select="@ows_ID"/></xsl:attribute>
				<xsl:attribute name="Heading"><xsl:value-of select="@ows_Heading"/></xsl:attribute>
				<xsl:attribute name="Quote"><xsl:value-of select="@ows_Quote"/></xsl:attribute>
				<xsl:attribute name="HomepageLargePhoto"><xsl:value-of select="@ows_HompageLargePhoto"/></xsl:attribute>
				<xsl:attribute name="TextColor"><xsl:value-of select="@ows_TextColor"/></xsl:attribute>
				<xsl:variable name="profileID"><xsl:value-of select="substring-before(@ows_RelatedProfile,';')"/></xsl:variable>
				<xsl:variable name="profileUsername"><xsl:value-of select="substring-after(@ows_RelatedProfile,';#')"/></xsl:variable>
				<xsl:for-each select="/Render/Profiles//z:row[@ows_ID=$profileID]">
					<!--for doctoral, we don't care if student faculty asset and we are just showing student firstname-->
					<xsl:attribute name="Line1"><xsl:value-of select="@ows_FirstName1"/><xsl:value-of select="$space"/><xsl:value-of select="@ows_LastName"/></xsl:attribute>
					<xsl:attribute name="Line2"><xsl:value-of select="substring-after(@ows_Topic,'#')" disable-output-escaping="yes"/></xsl:attribute>
					<xsl:attribute name="URL">/doctoral/research-community/students-faculty/Pages/profile-details.aspx?profile=<xsl:value-of select="$profileUsername"/></xsl:attribute>
					<xsl:attribute name="FullName"><xsl:value-of select="@ows_FirstName1"/>&#160;<xsl:value-of select="@ows_LastName"/></xsl:attribute>
					<xsl:attribute name="UserName"><xsl:value-of select="$profileUsername"/></xsl:attribute>
					<xsl:attribute name="ThumbnailPhoto"><xsl:value-of select="@ows_ThumbnailPhoto"/></xsl:attribute>
				</xsl:for-each>
			</xsl:for-each>
		</Item>
		<xsl:if test="$remaining2">
			<xsl:call-template name="build-assets">
				<xsl:with-param name="list" select="$remaining2" />
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet> 