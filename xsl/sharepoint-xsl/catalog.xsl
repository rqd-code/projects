﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
xmlns:hbs="urn:hbs"
xmlns:z="#RowsetSchema"
xmlns:atom="http://www.w3.org/2005/Atom"
xmlns:ext="http://www.hbs.edu"  
xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime"  
xmlns:msxsl="urn:schemas-microsoft-com:xslt"
exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:import href="lib/utils.xsl"/>
	<xsl:import href="lib/renderings.xsl"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<FacultyLookupResults>
				<xsl:call-template name="getremote-fr-rss-results">
					<xsl:with-param name="cacheKey">catalog</xsl:with-param>
				</xsl:call-template>
			</FacultyLookupResults>
			<xsl:call-template name="query-course-catalog"/>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<xsl:template match="/Render">
		<xsl:variable name="primary" select="/Render" />
		<xsl:for-each select="hbs:QueryResults/z:row">
			<div class="row">
				<div class="span7">
					<ul class="media-list9" data-wcm-edit-url="/doctoral/Lists/Courses/EditForm.aspx?ID={@ows_ID}"
		                   data-wcm-edit-label="Catalog Item"
		                   data-wcm-new-url="/doctoral/Lists/Courses/NewForm.aspx?RootFolder=%2Fdoctoral%2FLists%2FCourses&amp;ContentTypeId=0x0100B0A13AE5B764C94AB993C96BFE859C4F001CF0CA5973662B4B840F2CCFE7525A95"
		                   data-wcm-new-label="Catalog Item"					
					>
						<li class="media">
							<ul class="linear mu-uc">
								<li>
									<xsl:value-of select="@ows_CourseNum" disable-output-escaping="yes"/>
								</li>
							</ul>
							<h4 class="eta">
								<xsl:value-of select="@ows_Title" disable-output-escaping="yes"/>
							</h4>
							<p class="nu">Last Offered:&#160;<span class="offered"><xsl:value-of select="@ows_SemesterOffered" disable-output-escaping="yes"/>&#160;<xsl:value-of select="@ows_YearOffered" disable-output-escaping="yes"/></span><br/>
						Last Taught By:&#160;<span class="faculty">
									<xsl:call-template name="render-faculty">
										<xsl:with-param name="nonhbsfaculty"><xsl:value-of select="@ows_NonHBSFaculty" disable-output-escaping="yes"/></xsl:with-param>
										<xsl:with-param name="all-faculty"><xsl:copy-of select="$primary/FacultyLookupResults"/></xsl:with-param>
										<xsl:with-param name="type">catalog</xsl:with-param>
									</xsl:call-template>						
					</span></p>
							<p>
								<xsl:value-of select="@ows_PastBlurb" disable-output-escaping="yes"/>
							</p>
						</li>
					</ul>
				</div>
			</div>
			<div class="hr">&#160;</div>
			<div class="shim32">
			</div>
			<div class="shim2">
			</div>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
