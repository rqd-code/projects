﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
xmlns:hbs="urn:hbs"
xmlns:z="#RowsetSchema"
xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime"  
xmlns:msxsl="urn:schemas-microsoft-com:xslt"
exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:import href="lib/utils.xsl"/>
	<xsl:import href="lib/renderings.xsl"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<FacultyLookupResults>
				<xsl:call-template name="getremote-fr-rss-results">
					<xsl:with-param name="cacheKey">courses</xsl:with-param>
				</xsl:call-template>
			</FacultyLookupResults>
			<xsl:call-template name="query-current-courses"/>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<xsl:template match="/Render">
		<xsl:variable name="primary" select="/Render" />
		<div class="curriculum">
			<xsl:for-each select="hbs:QueryResults/z:row">
				<xsl:if test="position()=1 or @ows_Semester != preceding-sibling::z:row[1]/@ows_Semester">
					<xsl:if test="position()!=1">
						<div class="shim32">
						</div>
					</xsl:if>
					<h3>
						<xsl:value-of select="@ows_Semester"/>
					</h3>
					<div class="hr4">&#160;
					</div>
				</xsl:if>
				<dl class="plusminus" data-wcm-edit-url="/doctoral/Lists/Courses/EditForm.aspx?ID={@ows_ID}"
		                   data-wcm-edit-label="Course Item"
		                   data-wcm-new-url="/doctoral/Lists/Courses/NewForm.aspx?RootFolder=%2Fdoctoral%2FLists%2FCourses&amp;ContentTypeId=0x0100CA4AE94A7F702B41804D0AAEB707EFAA00CFA450CDCDFEC3458F49A9051321B104"
		                   data-wcm-new-label="Course Item"
				>
					<dt>
						<a class="course-title">
							<xsl:value-of select="@ows_Title" disable-output-escaping="yes"/>
						</a>
						<br/>
						<div class="paragraph">Instructor:&#160;<span class="faculty">
									<xsl:call-template name="render-faculty">
										<xsl:with-param name="nonhbsfaculty"><xsl:value-of select="@ows_NonHBSFaculty" disable-output-escaping="yes"/></xsl:with-param>
										<xsl:with-param name="all-faculty"><xsl:copy-of select="$primary/FacultyLookupResults"/></xsl:with-param>
										<xsl:with-param name="type">courses</xsl:with-param>
									</xsl:call-template>								
						</span></div>
					</dt>
					<dd>
						<div class="mu-uc">
							<xsl:value-of select="@ows_CourseNum" disable-output-escaping="yes"/>
							<xsl:if test="@ows_HUCourseNum"><br/>
							<xsl:value-of select="@ows_HUCourseNum" disable-output-escaping="yes"/>
							</xsl:if>
						</div>
						<div class="nu">
							<xsl:value-of select="hbs:Replace(@ows_ScheduleLocation,'|','&#160;&#160;|&#160;&#160;')" disable-output-escaping="yes"/>
							<!--this is cheating, fix with css class when you have time-->
						</div>
						<div class="shim17">
						</div>
						<xsl:value-of select="@ows_Blurb" disable-output-escaping="yes"/>
					</dd>
				</dl>
				<div class="hr">&#160;</div>
			</xsl:for-each>
		</div>
		<p>For the <a href="http://coursecatalog.harvard.edu/OASIS/CourseCat/index.html" class="ext">Harvard University Course Catalog</a></p>
	</xsl:template>
</xsl:stylesheet>
