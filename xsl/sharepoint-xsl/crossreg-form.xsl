﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
	<xsl:param name="args"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<!--
    
    Query Data
    
    -->
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<!--
    
    Render Output
    
    -->
	<xsl:template match="/Render">
		<script type="text/javascript" src="/doctoral/Style Library/hbs/js/plugins/jquery.validate.min.js">
		</script>
		<div action="http://hbsapps.hbs.edu/emailForm/process" class="form validated" method="post">
			<p>
				<label for="firstname" class="kappa">First Name</label>
				<br />
				<input name="First Name" id="firstname" class="field required" size="22" type="text" />
			</p>
			<p>
				<label for="lastname" class="kappa">Last Name</label>
				<br />
				<input name="Last Name" id="lastname" class="field required" size="22" type="text" />
			</p>
			<p>
				<label for="email" class="kappa">Email Address used for previous course registration</label>
				<br />
				<input size="22" maxlength="128" name="f" class="field required" id="email" type="text" />
			</p>
			<p>
				<label for="affiliatedschool" class="kappa">Affiliated School</label>
				<br />
				<input size="22" maxlength="128" name="f" class="field required" id="affiliatedschool" type="text" />
			</p>
			<p>
				<label for="coursenumber" class="kappa">Course Number</label>
				<br />
				<input size="22" maxlength="128" name="f" class="field required" id="coursenumber" type="text" />
			</p>
			<p>
				<label for="coursename" class="kappa">Course Name</label>
				<br />
				<input size="22" maxlength="128" name="f" class="field required" id="coursename" type="text" />
			</p>
			<p>
				<input name="openaccess" type="hidden"/>
				<INPUT TYPE="hidden" NAME="s" VALUE="Cross Registration Form Submittal"/>
				<input name="e" value="rdornin@hbs.edu,adaniell@hbs.edu,psanivada@hbs.edu,sagrant@hbs.edu" type="hidden"/>
				<input type="hidden" name="u" value="http://authqa.hbs.edu/doctoral/registrar/cross-registration/Pages/crossreg-thanks.aspx" />
				<input value="Submit" class="btn-submit" type="submit" />
			</p>
		</div>
	</xsl:template>
</xsl:stylesheet>