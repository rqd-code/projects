﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:param name="args"/>
	<xsl:template match="/Root">
	  <Render>
        <xsl:if test="hbs:Request/debug = 'breadcrumb'"><hbs:Debug/></xsl:if>
	    <hbs:GetPage/>
	    <Parents>
			<hbs:GetNavigation Provider="GlobalNavSiteMapProvider">
				<ParentNode>
				<ParentNode>
				<ParentNode>
				<ParentNode>
				<ParentNode>
				<ParentNode>
				<ParentNode>
				<ParentNode>
				<ChildNodes Depth="10"/>
				</ParentNode>
				</ParentNode>
				</ParentNode>
				</ParentNode> 
				</ParentNode>
				</ParentNode>
				</ParentNode>
				</ParentNode>
			</hbs:GetNavigation>   
	    </Parents>
		<ParentNavigation>
		    <xsl:choose>
			    <xsl:when test="contains(hbs:Request/URL,'default.aspx')">
					<hbs:GetNavigation>
						<ParentNode>
		                   <ChildNodes Depth="1"/>
						</ParentNode>
					</hbs:GetNavigation>
			    </xsl:when>
			    <xsl:otherwise>
					<hbs:GetNavigation>
                       	<ParentNode>
						<ParentNode>
		                   <ChildNodes Depth="1"/>
						</ParentNode>
						</ParentNode>
					</hbs:GetNavigation>
			    </xsl:otherwise>
		    </xsl:choose>
		</ParentNavigation>
		<Navigation>
		    <xsl:choose>
			    <xsl:when test="contains(hbs:Request/URL,'default.aspx')">
				    <!--<xsl:attribute name="Default">1</xsl:attribute>-->
					<hbs:GetNavigation>
						<ChildNodes Depth="1"/>
					</hbs:GetNavigation>
			    </xsl:when>
			    <xsl:otherwise>
					<hbs:GetNavigation>
						<ParentNode>
		                   <ChildNodes Depth="1"/>
						</ParentNode>
					</hbs:GetNavigation>
			    </xsl:otherwise>
		    </xsl:choose>
		</Navigation>
    <hbs:Repeat/>
	  </Render>
	</xsl:template>
	<xsl:template match="/Render">
	
          <div class="breadcrumb">

	          <a href="/" class="ink">Harvard Business School</a>
	          <span class="txt-arrow">&#x2192;</span>

	          <a href="/doctoral/" class="ink">Doctoral</a>
	          <span class="txt-arrow">&#x2192;</span>

              <xsl:call-template name="render-dropdown">
                 <xsl:with-param name="node" select="ParentNavigation/hbs:Navigation/Node"/>
                 <xsl:with-param name="nomatch" select="'Doctoral'"/>
                 <xsl:with-param name="hasarrow" select="true()"/>
              </xsl:call-template>

                  
              <xsl:call-template name="render-dropdown">
                 <xsl:with-param name="node" select="Navigation/hbs:Navigation/Node"/>
                 <xsl:with-param name="nomatch" select="'Doctoral'"/>
              </xsl:call-template>

	          <xsl:if test="not(Navigation[@Default])">
		          <span class="txt-arrow">&#x2192;</span>
		          <xsl:value-of select="hbs:Page/z:row/@ows_Title"/>
	          </xsl:if>
          
          </div> 
    </xsl:template>
    
    <xsl:template name="render-dropdown">
      <xsl:param name="node"/>
      <xsl:param name="nomatch"/>
      <xsl:param  name="hasarrow" select="false()"/>
        <xsl:choose>
           <xsl:when test="$node/Node and $node/Title != $nomatch">
				<div class="dropdown-container">
				   <a class="ink nu dropdown-toggle" href="{$node/Url}">
				      <xsl:value-of select="$node/Title"/>
				      <span class="icon-select"></span>
				   </a>
				   <ul class="dropdown-menu nu">
			          <li>
			          	  <a href="{$node/Url}" class="white">
			        	  <xsl:if test="$node[@Current] and not(contains($node/Url,'default.aspx'))">
			                 <xsl:attribute name="class">active inherit-color</xsl:attribute>
			              </xsl:if>
			          	  <xsl:value-of select="$node/Title"/></a>
			          </li>
			          <li class="divider inherit-bg"></li>
			          <xsl:for-each select="$node/Node">
			             <li>
			             <a href="{Url}" class="white">
			             <xsl:if test="@Current"><xsl:attribute name="class">active inherit-color</xsl:attribute></xsl:if>
			             <xsl:value-of select="Title"/></a>
			             </li>
			          </xsl:for-each>
				   </ul>
				</div> 
                <xsl:if test="$hasarrow"><span class="txt-arrow">&#x2192;</span></xsl:if>
           </xsl:when>
           <xsl:when test="$node/Title != $nomatch">
			   <a class="ink nu" href="{$node/Url}">
			      <xsl:value-of select="$node/Title"/>
			   </a>
               <xsl:if test="$hasarrow"><span class="txt-arrow">&#x2192;</span></xsl:if>
           </xsl:when>
        </xsl:choose>
    </xsl:template>
    

</xsl:stylesheet>