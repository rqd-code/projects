﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/renderings.xsl"/>
	<xsl:param name="args"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:template match="/Root">
		<Step1>
			<xsl:if test="hbs:Request/debug='breadcrumb'">
				<hbs:Debug/>
			</xsl:if>
			<hbs:GetPage/>
			<hbs:Repeat/>
		</Step1>
	</xsl:template>
	<xsl:template match="Step1">
		<div class="breadcrumb">
			<xsl:call-template name="render-breadcrumb-start">
			</xsl:call-template>
			<xsl:value-of select="hbs:Page/z:row/@ows_Title"/>
		</div>
	</xsl:template>
</xsl:stylesheet> 