﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:import href="lib/utils.xsl"/>
	<xsl:import href="lib/renderings.xsl"/>
	<xsl:param name="args"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<hbs:GetPage/>
			<Parents>
				<hbs:GetNavigation>
					<ParentNode>
						<ParentNode>
							<ChildNodes Depth="1"/>
						</ParentNode>
					</ParentNode>
				</hbs:GetNavigation>
			</Parents>
			<ItemId>
				<xsl:value-of select="hbs:Request/profile"/>
			</ItemId>
			<Profiles>
				<xsl:call-template name="query-profiles"/>
			</Profiles>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<xsl:template match="Render">
		<xsl:variable name="itemId"><xsl:value-of select="ItemId"/></xsl:variable>
		<xsl:variable name="itemTitle">
		<xsl:choose>
		<xsl:when test="Profiles//z:row[@ows_Title=$itemId]/@ows_ContentType = 'Student-Faculty-Profile'"><xsl:value-of select="Profiles//z:row[@ows_Title=$itemId]/@ows_TitleOfProfile"/></xsl:when>	
		<xsl:otherwise><xsl:value-of select="Profiles//z:row[@ows_Title=$itemId]/@ows_FirstName1"/>&#160;<xsl:value-of select="Profiles//z:row[@ows_Title=$itemId]/@ows_LastName"/></xsl:otherwise>
		</xsl:choose>
		
		
</xsl:variable>
		<!--Implement custom breadcrumb-->
		<div class="breadcrumb">
			<xsl:call-template name="render-breadcrumb-profiles">
				<xsl:with-param name="parentItems" select="Parents/hbs:Navigation"/>
				<xsl:with-param name="items" select="Profiles//z:row"/>
				<xsl:with-param name="activate" select="$itemTitle "/>
			</xsl:call-template>
		</div>
	</xsl:template>
</xsl:stylesheet> 