﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:import href="lib/utils.xsl"/>
	<xsl:import href="lib/renderings.xsl"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<xsl:call-template name="query-home-featured-research"/>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<xsl:template match="/Render">
		<script type="text/javascript" src="http://www.hbs.edu/faculty/Style Library/hbs/js/jquery.js">
		</script>
		<script type="text/javascript" src="http://www.hbs.edu/faculty/Style Library/hbs/js/jquery-ui.js">
		</script>
		<script type="text/javascript" src="/doctoral/style library/hbs/js/doctoral-finder.js">
		</script>
		<script type="text/javascript">
    jQuery(function($){
       DoctoralFinder.wire("http://www.hbs.edu/faculty/Style Library/hbs/includes/autocomplete-doctoralfinder.aspx");
    });
    </script>
		<div class="form field-group-vertical" id="finder-container">
			<label class="kappa-uc">Find a Doctoral Student</label>
			<div class="row">
				<div class="span3">
					<input type="text" placeholder="Name" id="doctoralfinder" value="" class="field"/>
				</div>
				<div class="span1">
					<button type="submit" class="btn-submit">
						<span class="icon-search-white">
						</span>
					</button>
				</div>
			</div>
		</div>
		<div class="shim6">
		</div>
		
							<p class="more">
				<span class="txt-arrow">&#x2192;</span>&#160;<a href="https://inside.hbs.edu/Departments/doctoral/Pages/default.aspx">For Current Students</a>&#160;<span class="nu">(login required)</span>
	    </p>
		<div class="shim16">
		</div>


	</xsl:template>
</xsl:stylesheet>