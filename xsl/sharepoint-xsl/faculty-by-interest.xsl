﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   xmlns:atom="http://www.w3.org/2005/Atom"
   xmlns:ext="http://www.hbs.edu"   
   exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:import href="lib/utils.xsl"/>
	<xsl:import href="lib/renderings.xsl"/>
	<xsl:param name="args"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:template match="/Root">
		<Step1>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<xsl:copy-of select="hbs:Request/PATH_INFO"/>
			<xsl:variable name="topicid"><xsl:choose><xsl:when test="contains(hbs:Request/PATH_INFO,'/accounting-management')">3</xsl:when>
						<xsl:when test="contains(hbs:Request/PATH_INFO,'/business-economics')">2</xsl:when>
						<xsl:when test="contains(hbs:Request/PATH_INFO,'/health-policy')">4</xsl:when>
						<xsl:when test="contains(hbs:Request/PATH_INFO,'/management')">6</xsl:when>
						<xsl:when test="contains(hbs:Request/PATH_INFO,'/marketing')">5</xsl:when>
						<xsl:when test="contains(hbs:Request/PATH_INFO,'/organizational-behavior')">7</xsl:when>
						<xsl:when test="contains(hbs:Request/PATH_INFO,'/strategy')">1</xsl:when>
						<xsl:when test="contains(hbs:Request/PATH_INFO,'/technology-operations-management')">8</xsl:when></xsl:choose>
				</xsl:variable>
			<FBI>
				<xsl:call-template name="query-topics-by-id">
					<xsl:with-param name="topicid" select="normalize-space($topicid)"/>
				</xsl:call-template>
			</FBI>
			<hbs:Repeat/>
		</Step1>
	</xsl:template>
	<!--
    
    Query Data
    
    -->
	<xsl:template match="/Step1">
		<Render>
			<FacultyLookupResults>
				<xsl:variable name="topics"><xsl:value-of select="hbs:ToLower(FBI//z:row/@ows_RISQuery)"/></xsl:variable>
				<xsl:variable name="unique-cache-key"><xsl:choose>
						<xsl:when test="contains(PATH_INFO,'/accounting-management')">accounting-management</xsl:when>
						<xsl:when test="contains(PATH_INFO,'/business-economics')">business-economics</xsl:when>
						<xsl:when test="contains(PATH_INFO,'/health-policy')">health-policy</xsl:when>
						<xsl:when test="contains(PATH_INFO,'/management')">management</xsl:when>
						<xsl:when test="contains(PATH_INFO,'/marketing')">marketing</xsl:when>
						<xsl:when test="contains(PATH_INFO,'/organizational-behavior')">organization-behavior</xsl:when>
						<xsl:when test="contains(PATH_INFO,'/strategy')">strategy</xsl:when>
						<xsl:when test="contains(PATH_INFO,'/technology-operations-management')">technology-operations-management</xsl:when></xsl:choose>
				</xsl:variable>
				<xsl:call-template name="getremote-fr-rss-results">
					<xsl:with-param name="items" select="$topics"/>
					<xsl:with-param name="type">primarytopics</xsl:with-param>
					<xsl:with-param name="cacheKey" select="$unique-cache-key"/>
				</xsl:call-template>
			</FacultyLookupResults>
			<xsl:copy-of select="FBI"/>
			<xsl:copy-of select="PATH_INFO"/>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<!--
    
    Render Output
    
    -->
	<xsl:template match="/Render">
		<h3 class="kappa-uc">Faculty By Interest</h3>
		<div class="shim4">
		</div>
		<xsl:variable name="primary" select="/Render" />
		<xsl:variable name="row" select="FBI/hbs:QueryResults/z:row"/>
		<dl class="plusminus">
			<xsl:for-each select="hbs:Split($row/@ows_RISQuery, ';')">
				<xsl:if test="@Val != ''">
					<xsl:variable name="PrimaryTopic"><xsl:value-of select="hbs:ToLower(hbs:Replace(normalize-space(@Val),'%20',' '))"/></xsl:variable>
			        <xsl:comment><![CDATA[[if IE]>
			        <style type="text/css">
			        	.iefix { width: 400px;}
			        </style>
			        <![endif]]]></xsl:comment>
					<dt class="iefix" data-wcm-edit-url="/doctoral/Lists/Topics/EditForm.aspx?ID={$row/@ows_ID}">
						<a class="ucfirst">
							<xsl:value-of select="hbs:Replace(hbs:Replace(concat(hbs:ToUpper(substring($PrimaryTopic, 1, 1)), substring($PrimaryTopic, 2)),' and ',' &amp; '),' ','&#160;')" disable-output-escaping="yes"/>
						</a>
					</dt>
					<dd>
						<div class="shim12">
						</div>
						<ul class="unbulleted" style="margin-left:14px;">
							<xsl:for-each select="$primary/FacultyLookupResults//atom:feed/atom:entry">
								<!--<xsl:if test="position()!=1 and ext:faculty/ext:id != preceding-sibling::atom:entry[1]/ext:faculty/ext:id">-->
								<xsl:call-template name="render-faculty-profile">
									<xsl:with-param name="item" select="ext:faculty"/>
									<xsl:with-param name="primary-topic" select="$PrimaryTopic"/>
								</xsl:call-template>
							</xsl:for-each>
						</ul>
					</dd>
				</xsl:if>
			</xsl:for-each>
		</dl>
		<div class="shim13">
		</div>
		<div class="fbi-arrow">
			<a  class="btn-arrow teal-bg"  href="http://pine.hbs.edu/external/areasInterestShow.do?KEY=KWD">
				<span class="right">
				</span>View All Topics</a>
		</div>
		<div class="shim8">
		</div>
		<xsl:choose>
			<xsl:when test="contains(PATH_INFO,'/accounting-management')">
				<div class="fbi-arrow">
					<a  class="btn-arrow teal-bg" href="http://www.hbs.edu/units/am/">
						<span class="right">
						</span>Accounting &amp; Management Faculty Unit</a>
				</div>
			</xsl:when>
			<xsl:when test="contains(PATH_INFO,'/business-economics')">
				<div class="fbi-arrow">
					<a  class="btn-arrow teal-bg" href="http://economics.harvard.edu/people/people-type/faculty">
						<span class="right">
						</span>Harvard Economics Faculty</a>
				</div>
			</xsl:when>
			<xsl:when test="contains(PATH_INFO,'/health-policy')">
				<div class="fbi-arrow">
					<a  class="btn-arrow teal-bg" href="http://www.healthpolicy.fas.harvard.edu/faculty.htm">
						<span class="right">
						</span>Harvard Health Policy Faculty</a>
				</div>
			</xsl:when>
			<xsl:when test="contains(PATH_INFO,'/management')">
				<div class="fbi-arrow">
					<a  class="btn-arrow teal-bg" href="http://www.hbs.edu/units/ob/">
						<span class="right">
						</span>Organizational Behavior Faculty Unit</a>
				</div>
			</xsl:when>
			<xsl:when test="contains(PATH_INFO,'/marketing')">
				<div class="fbi-arrow">
					<a  class="btn-arrow teal-bg" href="http://www.hbs.edu/units/marketing/">
						<span class="right">
						</span>Marketing Faculty Unit</a>
				</div>
			</xsl:when>
			<xsl:when test="contains(PATH_INFO,'/organizational-behavior')">
				<div class="fbi-arrow">
					<a  class="btn-arrow teal-bg" href="http://www.isites.harvard.edu/icb/icb.do?keyword=k3007&amp;pageid=icb.page18831">
						<span class="right">
						</span>Harvard Psychology Faculty</a>
				</div>
				<div class="shim8">
				</div>
				<div class="fbi-arrow">
					<a  class="btn-arrow teal-bg" href="http://www.wjh.harvard.edu/soc/pages/faculty.html">
						<span class="right">
						</span>Harvard Sociology Faculty</a>
				</div>
			</xsl:when>
			<xsl:when test="contains(PATH_INFO,'/strategy')">
				<div class="fbi-arrow">
					<a  class="btn-arrow teal-bg" href="http://www.hbs.edu/units/strategy/">
						<span class="right">
						</span>Strategy Unit Faculty</a>
				</div>
			</xsl:when>
			<xsl:when test="contains(PATH_INFO,'/technology-operations-management')">
				<div class="fbi-arrow">
					<a  class="btn-arrow teal-bg" href="http://www.hbs.edu/units/tom/">
						<span class="right">
						</span>TOM Faculty Unit</a>
				</div>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>