﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
	<xsl:param name="args"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<!--
    
    Query Data
    
    -->
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<!--
    
    Render Output
    
    -->
	<xsl:template match="/Render">
		<h3 class="kappa-uc">Ask a Question</h3>
		<script type="text/javascript" src="/doctoral/Style Library/hbs/js/plugins/jquery.validate.min.js">
		</script>
		<script type="text/javascript">
			jQuery(function(){
				var thanks = window.location.search.substring(1);
				$("#thanks").hide();
				if (thanks == "thanks") {
					$("#thanks").show();
				}
			});
		 </script>

		<div id="thanks">
			<p>
				<strong>We have received your submission.</strong>
				<br/>

			Thank you for your interest in Harvard Business School's Doctoral Programs. Your inquiry is important to us, and our doctoral admissions staff will respond to you soon.</p>
		</div>
		<div action="http://hbsapps.hbs.edu/emailForm/process" class="form validated" method="post">
			<p>
				<label for="name" class="kappa">Name</label>
				<br />
				<input name="Name" id="name" class="field required" size="22" type="text" />
			</p>
			<p>
				<label for="email" class="kappa">E-mail</label>
				<br />
				<input size="22" maxlength="128" name="f" class="field required" id="email" type="text" />
			</p>
			<p>
				<label for="comments" class="kappa">Question</label>
				<br />
				<textarea name="Question" class="field required" id="comments" cols="22" rows="8">
				</textarea>
			</p>
			<p>
				<input name="openaccess" type="hidden"/>
				<input name="e" value="rdornin@hbs.edu,adaniell@hbs.edu,psanivada@hbs.edu,sagrant@hbs.edu" type="hidden"/>
				<input type="hidden" name="u" value="http://authdev.hbs.edu/doctoral/faqs/Pages/default.aspx?thanks" />
				<input type="hidden" name="aTemplate" value="http://www.hbs.edu/doctoral/faqs/faq.email.admin.fml" />
				<input value="Submit" class="btn-submit" type="submit" />
			</p>
		</div>
	</xsl:template>
</xsl:stylesheet>