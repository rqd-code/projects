﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
xmlns:hbs="urn:hbs"
xmlns:z="#RowsetSchema"
xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime"  
xmlns:msxsl="urn:schemas-microsoft-com:xslt"
exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:import href="lib/utils.xsl"/>
	<xsl:import href="lib/renderings.xsl"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<xsl:call-template name="query-home-featured-research"/>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<xsl:template match="/Render">
		<div class="featured-research-container">
			<h3 class="kappa-uc">Featured Student&#160;/&#160;Faculty Publications</h3>
			<div class="shim8">
			</div>
			<ul class="media-list3 media-list-dividers">
				<xsl:for-each select="hbs:QueryResults/z:row">
					<xsl:variable name="publicationlink"><xsl:value-of select="@ows_URL"/></xsl:variable>
					<li class="media" data-wcm-edit-url="/doctoral/Lists/HomeFeaturedResearch/EditForm.aspx?ID={@ows_ID}">
						<ul class="linear mu-uc">
							<li>
								<xsl:value-of select="@ows_PubDate"/>
							</li>
						
							<li class="ash">
								<xsl:value-of select="@ows_Source"/>
							</li>
						</ul>
						<h4 class="kappa-uc">
							<a href="{$publicationlink}" class="teal">
								<xsl:value-of select="@ows_Title" disable-output-escaping="yes"/>
							</a>
						</h4>
						<div class="nu">
							<!-- logic for Re:By needed -->
							<xsl:choose>
								<xsl:when test="contains(@ows_PersonType,'Author')">by </xsl:when>
								<xsl:when test="contains(@ows_PersonType,'Cited')">Re: </xsl:when>
								<xsl:otherwise>By: </xsl:otherwise>
							</xsl:choose>
							<xsl:value-of select="@ows_Authors" disable-output-escaping="yes"/>
						</div>
					</li>
				</xsl:for-each>
			</ul>
			<p class="more">
				<span class="txt-arrow">&#x2192;</span>&#160;<a href="/doctoral/Documents/2012ResearchBrochure.pdf">All 2012 Publications</a>
	    </p>
		</div>
		<!-- this arrow might be removed 
		<div class="home-arrow">
			<a href="/doctoral/placement/Pages/default.aspx" class="btn-arrow teal-bg">
				<span class="right">
				</span>See Students on the Job Market
			</a>
		</div>-->
	</xsl:template>
</xsl:stylesheet>