﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
	<xsl:param name="args"/>
	<xsl:variable name="cachetime" select="'1440'"/>
	<!-- 10080 - 1 week cache -->
	<xsl:variable name="twitterhost" select="'http://www.hbs.edu'"/>
	<xsl:variable name="getRemoteHost" select="'http://dev.hbs.edu'"/>
	<xsl:variable name="checklist" select="'True'"/>
	<!--   
    Fields used when returning an item
    -->
	<xsl:template name="MiscSettingsFields">
		<FieldRef Name="ID"/>
		<FieldRef Name="Title"/>
		<FieldRef Name="Content"/>
	</xsl:template>    
	<xsl:template name="SidebarFields">
		<FieldRef Name="ID"/>
		<FieldRef Name="Title"/>
		<FieldRef Name="Content"/>
	</xsl:template>
	<xsl:template name="TopicsFields">
		<FieldRef Name="ID"/>
		<FieldRef Name="Title"/>
		<FieldRef Name="LocalFolder"/>
		<FieldRef Name="RISQuery"/>
		<FieldRef Name="ApplyOnlineLink"/>
	</xsl:template>
	<xsl:template name="CurrentStudentsFields">
		<FieldRef Name="ID"/>
		<FieldRef Name="Title"/>
		<FieldRef Name="Topic"/>
		<FieldRef Name="FirstName1"/>
		<FieldRef Name="LastName"/>	
		<FieldRef Name="RISID"/>		
		<FieldRef Name="ProfileLookUp"/>
		<FieldRef Name="Visibility"/>						
	</xsl:template>
	<xsl:template name="CoursesFields">
		<FieldRef Name="ID"/>
		<FieldRef Name="Title"/>		
		<FieldRef Name="CourseNum"/>
		<FieldRef Name="HUCourseNum"/>		
		<FieldRef Name="Faculty"/>
		<FieldRef Name="Blurb"/>
		<FieldRef Name="ScheduleLocation"/>
		<FieldRef Name="Semester"/>
		<FieldRef Name="CourseCatalog"/>
		<FieldRef Name="CourseNum"/>
		<FieldRef Name="YearOffered"/>
		<FieldRef Name="SemesterOffered"/>
		<FieldRef Name="PastBlurb"/>
		<FieldRef Name="FacultySelector"/>
		<FieldRef Name="LastTaughtBy"/>
		<FieldRef Name="LastTaughtFacultySelector"/>
		<FieldRef Name="IsCurrentCourse"/>
		<FieldRef Name="NonHBSFaculty"/>		
	</xsl:template>
	<xsl:template name="OnJobMarketFields">
		<FieldRef Name="ID"/>
		<FieldRef Name="FirstName1"/>
		<FieldRef Name="LastName"/>
		<FieldRef Name="Title"/>
		<FieldRef Name="DegreeType"/>
		<FieldRef Name="AbstractBlurb"/>
		<FieldRef Name="AbstractTitle"/>
		<FieldRef Name="ResumeLink"/>
		<FieldRef Name="CoverLetterLink"/>
		<FieldRef Name="WebsiteLink"/>
		<FieldRef Name="Topic"/>
		<FieldRef Name="ProfileLookUp"/>
		<FieldRef Name="ThesisCochairLine"/>
		<FieldRef Name="FacultyAdvisors"/>
		<FieldRef Name="EMail"/>
		<FieldRef Name="Year"/>
		<FieldRef Name="Visibility"/>
		<FieldRef Name="NonHBSFaculty"/>
	</xsl:template>
	<xsl:template name="AlumniFields">
		<FieldRef Name="ID"/>
		<FieldRef Name="FullName1"/>
		<FieldRef Name="FirstName1"/>
		<FieldRef Name="LastName"/>
		<FieldRef Name="SidebarPhoto"/>
		<FieldRef Name="ProfileLookUp"/>
		<FieldRef Name="ProfileBlurb"/>
		<FieldRef Name="JobTitle1"/>
		<FieldRef Name="Degree"/>
		<FieldRef Name="Year"/>
		<FieldRef Name="Placement"/>
		<FieldRef Name="MoreLink"/>
		<FieldRef Name="Title"/>
		<FieldRef Name="Year"/>
		<FieldRef Name="Visibility"/>
		<FieldRef Name="SortOrder"/>
	</xsl:template>
	<xsl:template name="HomeFeaturedResearchFields">
		<FieldRef Name="ID"/>
		<FieldRef Name="Title"/>
		<FieldRef Name="Content"/>
		<FieldRef Name="Source"/>
		<FieldRef Name="PubDate"/>
		<FieldRef Name="URL"/>
		<FieldRef Name="Authors"/>
		<FieldRef Name="PersonType"/>
	</xsl:template>
	<xsl:template name="BlogsFields">
		<FieldRef Name="ID"/>
		<FieldRef Name="Title"/>
		<FieldRef Name="PostAuthor"/>
		<FieldRef Name="Date"/>
		<FieldRef Name="Body"/>
		<FieldRef Name="HomeBlurb"/>
		<FieldRef Name="FormattedDate"/>
	</xsl:template>
	<xsl:template name="EventsFields">
		<FieldRef Name="ID"/>
		<FieldRef Name="Title"/>
		<FieldRef Name="Description"/>
		<FieldRef Name="Date"/>
		<FieldRef Name="URL"/>
		<FieldRef Name="IsTBD"/>
		<FieldRef Name="MultiDates"/>
	</xsl:template>
	<xsl:template name="AssetsFields">
		<FieldRef Name="ID"/>
		<FieldRef Name="Title"/>
		<FieldRef Name="Heading"/>
		<FieldRef Name="Quote"/>
		<FieldRef Name="HompageLargePhoto"/>
		<FieldRef Name="RelatedProfile"/>
		<FieldRef Name="TextColor"/>
		<FieldRef Name="SortOrder"/>
		<FieldRef Name="Visibility"/>
	</xsl:template>
	<xsl:template name="CarouselViewFields">
		<FieldRef Name="ID"/>
		<FieldRef Name="Title"/>
		<FieldRef Name="RelatedAsset"/>
		<FieldRef Name="MissionContent"/>
		<FieldRef Name="GlobalHeaderLine"/>
	</xsl:template>
	<xsl:template name="ProfilesFields">
		<FieldRef Name="ID"/>
		<FieldRef Name="Title"/>
		<FieldRef Name="ContentType"/>
		<FieldRef Name="FirstName1"/>
		<FieldRef Name="LastName"/>
		<FieldRef Name="ProfileBlurb"/>
		<FieldRef Name="InSidebarCarousel"/>
		<FieldRef Name="ShortBlurb"/>
		<FieldRef Name="Placement"/>
		<FieldRef Name="Affiliation"/>
		<FieldRef Name="Degree"/>
		<FieldRef Name="Quote"/>
		<FieldRef Name="HomeRegion"/>
		<FieldRef Name="UndergraduateEducation"/>
		<FieldRef Name="PreviousExperience"/>
		<FieldRef Name="HBSActivities"/>
		<FieldRef Name="ThumbnailPhoto"/>
		<FieldRef Name="SidebarPhoto"/>
		<FieldRef Name="Year"/>
		<FieldRef Name="SortOrder"/>
		<FieldRef Name="CurrentPosition"/>
		<FieldRef Name="CurrentMajorActivities"/>
		<FieldRef Name="AreasOfStudy"/>
		<FieldRef Name="Featured"/>
		<FieldRef Name="Topic"/>
		<FieldRef Name="Visibility0"/>
		<FieldRef Name="FacultySelector"/>
		<FieldRef Name="FacultyFirstName"/>
		<FieldRef Name="FacultyLastName"/>
		<FieldRef Name="FacultyIntroSentence"/>
		<FieldRef Name="FacultyFullName"/>
		<FieldRef Name="FacultyUserName"/>
		<FieldRef Name="TitleOfProfile"/>
		<FieldRef Name="ProjectName"/>
		<FieldRef Name="SecondStudentFirstName"/>
		<FieldRef Name="SecondStudentLastName"/>
		<FieldRef Name="ProfilePhoto"/>
		<FieldRef Name="QuoteAttribution"/>
	</xsl:template>
	<xsl:template name="FacultyProfilesFields">
		<FieldRef Name="ID"/>
		<FieldRef Name="Title"/>
		<FieldRef Name="FacultyUsernames"/>
		<FieldRef Name="FacultySelector"/>
	</xsl:template>
	<!--
    
    Add sidebars to a page
    
    -->	
	<xsl:template name="query-sidebars">
		<xsl:param name="key"/>
		<p>
			<xsl:value-of select="$key"/>
		</p>
		<hbs:SPQuery Name="Sidebars">
			<Site Path="~SiteCollection" />
			<List Name="Sidebars" />
			<Cache Key="hbs.doctoral.sidebars.{$key}" Minutes="{$cachetime}" CheckForListChanges="{$checklist}"/>
			<View>
				<Query>
					<Where>
						<Eq>
							<FieldRef Name="ID"/>
							<Value Type="Counter">
								<xsl:value-of select="$key"/>
							</Value>
						</Eq>
					</Where>
				</Query>
				<ViewFields>
					<xsl:call-template name="SidebarFields"/>
				</ViewFields>
				<RowLimit>1</RowLimit>
			</View>
		</hbs:SPQuery>
	</xsl:template>
	<!--
    
    Sidebar carousel
    
    -->
	<xsl:template name="query-sidebar-carousel">
		<hbs:SPQuery Name="Profiles">
			<Site Path="~SiteCollection" />
			<List Name="Profiles" />
			<Cache Key="hbs.doctoral.sidebarcarousel" Minutes="{$cachetime}" CheckForListChanges="{$checklist}"/>
			<View>
				<Query>
					<Where>
						<Eq>
							<FieldRef Name="InSidebarCarousel"/>
							<Value Type="Text">Yes</Value>
						</Eq>
					</Where>
				</Query>
				<ViewFields>
					<xsl:call-template name="ProfilesFields"/>
				</ViewFields>
				<RowLimit>4</RowLimit>
			</View>
		</hbs:SPQuery>
	</xsl:template>
	<!--
    
    LeftNav Content on Profiles page
    http://authqa.hbs.edu/doctoral/research-community/students-faculty/Pages/default.aspx
    
    -->	
	<xsl:template name="query-profile-nav-content">
		<hbs:SPQuery Name="MiscSettings">
			<Site Path="~SiteCollection" />
			<List Name="MiscSettings" />
			<Cache Key="hbs.doctoral.profile.nav.content" Minutes="{$cachetime}" CheckForListChanges="{$checklist}"/>
			<View>
				<Query>
					<Where>
						<Eq>
							<FieldRef Name="Title"/>
							<Value Type="Text">ProfilesLeftNavContent</Value>
						</Eq>
					</Where>				
				</Query>
				<ViewFields>
					<xsl:call-template name="MiscSettingsFields"/>
				</ViewFields>
			</View>
		</hbs:SPQuery>
	</xsl:template>

	<!--
    
    Blogs on homepage and blog page for exec director
    
    -->	
	<xsl:template name="query-blogs">
		<hbs:SPQuery Name="Blogs">
			<Site Path="~SiteCollection" />
			<List Name="Blogs" />
			<Cache Key="hbs.doctoral.blogs" Minutes="{$cachetime}" CheckForListChanges="{$checklist}"/>
			<View>
				<Query>
					<OrderBy>
						<FieldRef Name="Date" Ascending="FALSE"/>
					</OrderBy>
				</Query>
				<ViewFields>
					<xsl:call-template name="BlogsFields"/>
				</ViewFields>
			</View>
		</hbs:SPQuery>
	</xsl:template>
	<!--
    
    Alumni sidebar
    
    -->	
	<xsl:template name="query-alumni">
		<hbs:SPQuery Name="AlumniSidebar">
			<Site Path="~SiteCollection" />
			<List Name="AlumniSidebar" />
			<Cache Key="hbs.doctoral.alumni.sidebar" Minutes="{$cachetime}" CheckForListChanges="{$checklist}"/>
			<View>
				<Query>
					<Where>
						<Eq>
							<FieldRef Name="Visibility"/>
							<Value Type="Text">Visible</Value>
						</Eq>
					</Where>
					<OrderBy>
						<FieldRef Name="SortOrder"/>
					</OrderBy>
				</Query>
				<ViewFields>
					<xsl:call-template name="AlumniFields"/>
				</ViewFields>
			</View>
		</hbs:SPQuery>
	</xsl:template>
	<!--
    
    for the placement/students on job market
    
    -->		
	<xsl:template name="query-on-job-market">
		<hbs:SPQuery Name="StudentsOnJobMarket">
			<Site Path="~SiteCollection" />
			<List Name="StudentsOnJobMarket" />
			<Cache Key="hbs.doctoral.onjobmarket" Minutes="{$cachetime}" CheckForListChanges="{$checklist}"/>
			<View>
				<Query>
					<Where>
						<Eq>
							<FieldRef Name="Visibility"/>
							<Value Type="Text">Visible</Value>
						</Eq>
					</Where>
					<OrderBy>
						<FieldRef Name="Topic" Ascending="TRUE"/>
						<FieldRef Name="StudentLastName" Ascending="TRUE"/>
					</OrderBy>
				</Query>
				<ViewFields>
					<xsl:call-template name="OnJobMarketFields"/>
				</ViewFields>
			</View>
		</hbs:SPQuery>
	</xsl:template>
	<!--
    
    for the home page carousel profiles
    
    -->		
	<xsl:template name="query-assets">
		<hbs:SPQuery Name="query-assets">
			<Site Path="~SiteCollection" />
			<List Name="CarouselProfiles" />
			<Cache Key="hbs.doctoral.carousel-profiles" Minutes="{$cachetime}" CheckForListChanges="{$checklist}" />
			<View>
				<Query>	
					<OrderBy>
						<FieldRef Name="Title"/>
					</OrderBy>
				</Query>
				<ViewFields>
					<xsl:call-template name="AssetsFields"/>
				</ViewFields>
			</View>
		</hbs:SPQuery>
	</xsl:template>
	<!--
    
    for courses
    
    -->		
	<xsl:template name="query-current-courses">
		<hbs:SPQuery Name="query-current-courses">
			<Site Path="~SiteCollection" />
			<List Name="Courses" />
			<Cache Key="hbs.doctoral.courses" Minutes="{$cachetime}" CheckForListChanges="{$checklist}" />
			<View>
				<Query>
					<Where>
						<Eq>
							<FieldRef Name="IsCurrentCourse"/>
							<Value Type="Text">Yes</Value>
						</Eq>
					</Where>				
					<OrderBy>
						<FieldRef Name="Semester"/>
						<FieldRef Name="CourseNum"/>
					</OrderBy>
				</Query>
				<ViewFields>
					<xsl:call-template name="CoursesFields"/>
				</ViewFields>
			</View>
		</hbs:SPQuery>
	</xsl:template>
	<!--
    
    for course catalog
    
    -->		
	<xsl:template name="query-course-catalog">
		<hbs:SPQuery Name="query-course-catalog">
			<Site Path="~SiteCollection" />
			<List Name="Courses" />
			<Cache Key="hbs.doctoral.course.catalog" Minutes="{$cachetime}" CheckForListChanges="{$checklist}" />
			<View>
				<Query>
					<Where>
						<Eq>
							<FieldRef Name="IsCurrentCourse"/>
							<Value Type="Text">No</Value>
						</Eq>
					</Where>					
					<OrderBy>
						<FieldRef Name="CourseNum"/>
					</OrderBy>
				</Query>
				<ViewFields>
					<xsl:call-template name="CoursesFields"/>
				</ViewFields>
			</View>
		</hbs:SPQuery>
	</xsl:template>

	<!--
    
    defines what gets shown in carousel on home page
    
    -->		
	<xsl:template name="query-carouselview">
		<hbs:SPQuery Name="query-carouselview">
			<Site Path="~SiteCollection" />
			<List Name="CarouselView" />
			<Cache Key="hbs.doctoral.carouselview" Minutes="{$cachetime}" CheckForListChanges="{$checklist}" />
			<View>
				<Query>
					<OrderBy>
						<FieldRef Name="Title"/>
					</OrderBy>
				</Query>
				<ViewFields>
					<xsl:call-template name="CarouselViewFields"/>
				</ViewFields>
			</View>
		</hbs:SPQuery>
	</xsl:template>
	<!--
    
    query list of local topics for various dropdowns
    
    -->			
	<xsl:template name="query-topics">
		<hbs:SPQuery Name="query-topics">
			<Site Path="~SiteCollection" />
			<List Name="Topics" />
			<Cache Key="hbs.doctoral.topics.local" Minutes="{$cachetime}" CheckForListChanges="{$checklist}" />
			<View>
				<Query>
					<OrderBy>
						<FieldRef Name="Title"/>
					</OrderBy>
				</Query>
				<ViewFields>
					<xsl:call-template name="TopicsFields"/>
				</ViewFields>
			</View>
		</hbs:SPQuery>
	</xsl:template>
	<!--
    
    query list of local topics for the f+r rss feed
    
    -->		
	<xsl:template name="query-topics-by-id">
		<xsl:param name="topicid"/>
		<hbs:SPQuery Name="query-topics-by-id">
			<Site Path="~SiteCollection" />
			<List Name="Topics" />
			<Cache Key="hbs.doctoral.topics.by.id.{$topicid}" Minutes="{$cachetime}" CheckForListChanges="{$checklist}"/>
			<View>
				<Query>
					<Where>
						<Eq>
							<FieldRef Name="ID"/>
							<Value Type="Text">
								<xsl:value-of select="$topicid"/>
							</Value>
						</Eq>
					</Where>
					<OrderBy>
						<FieldRef Name="Title" Ascending="TRUE"/>
					</OrderBy>
				</Query>
				<ViewFields>
					<xsl:call-template name="TopicsFields"/>
				</ViewFields>
				<RowLimit>1</RowLimit>
			</View>
		</hbs:SPQuery>
	</xsl:template>
	<!--
    
    query current students by topic
    
    -->		
	<xsl:template name="query-current-students-by-topic">
		<xsl:param name="topic"/>
		<xsl:param name="localfolder"/>	
		<hbs:SPQuery Name="query-current-students-by-topic">
			<Site Path="~SiteCollection" />
			<List Name="CurrentStudents" />
			<Cache Key="hbs.doctoral.current.students.by.{$localfolder}" Minutes="{$cachetime}" CheckForListChanges="{$checklist}"/>
			<View>
				<Query>
					<Where>
					<And>
						<Eq>
							<FieldRef Name="Topic"/>
							<Value Type="Text">
								<xsl:value-of select="$topic"/>
							</Value>
						</Eq>
						<Eq>
							<FieldRef Name="Visibility"/>
							<Value Type="Text">Visible</Value>
						</Eq>	
						</And>					
					</Where>
					<OrderBy>
						<FieldRef Name="LastName" Ascending="TRUE"/>
					</OrderBy>
				</Query>
				<ViewFields>
					<xsl:call-template name="CurrentStudentsFields"/>
				</ViewFields>
			</View>
		</hbs:SPQuery>
	</xsl:template>

	<!--
    
    query profiles list for the people search page
    
    -->		
	<xsl:template name="query-profiles">
		<hbs:SPQuery Name="query-profiles">
			<Site Path="~SiteCollection" />
			<List Name="Profiles" />
			<Cache Key="hbs.doctoral.profiles.local" Minutes="{$cachetime}" CheckForListChanges="{$checklist}" />
			<View>
				<Query>
					<Where>
						<Eq>
							<FieldRef Name="Visibility0"/>
							<Value Type="Text">Visible</Value>
						</Eq>	                        
					</Where>
					<OrderBy>
						<FieldRef Name="LastName" Ascending="TRUE"/>
					</OrderBy>
				</Query>
				<ViewFields>
					<xsl:call-template name="ProfilesFields"/>
				</ViewFields>
			</View>
		</hbs:SPQuery>
	</xsl:template>
	<!--
    
    query profiles list by id for profile details page
    
    -->		
	<xsl:template name="query-profiles-by-id">
		<xsl:param name="profileId"/>
		<hbs:SPQuery Name="query-profiles-by-id">
			<Site Path="~SiteCollection" />
			<List Name="Profiles" />
			<Cache Key="hbs.doctoral.profiles.by.id.{$profileId}" Minutes="{$cachetime}" CheckForListChanges="{$checklist}"/>
			<View>
				<Query>
					<Where>
						<And>
							<Eq>
								<FieldRef Name="Title"/>
								<Value Type="Text">
									<xsl:value-of select="$profileId"/>
								</Value>
							</Eq>
							<Eq>
								<FieldRef Name="Visibility0"/>
								<Value Type="Text">Visible</Value>
							</Eq>
						</And>	   
					</Where>
					<OrderBy>
						<FieldRef Name="LastName" Ascending="TRUE"/>
					</OrderBy>
				</Query>
				<ViewFields>
					<xsl:call-template name="ProfilesFields"/>
				</ViewFields>
				<RowLimit>1</RowLimit>
			</View>
		</hbs:SPQuery>
	</xsl:template>
	<!--
    
    query profiles by topic
    
    -->		
	<xsl:template name="query-profiles-by-topic">
		<xsl:param name="topic"/>
		<xsl:variable name="topic-with-splitinfo"><xsl:value-of select="$topic"/>;#</xsl:variable>
		<hbs:SPQuery Name="query-profiles-by-topic">
			<Site Path="~SiteCollection" />
			<List Name="Profiles" />
			<Cache Key="hbs.doctoral.profiles.by.topic.{$topic}" Minutes="{$cachetime}" CheckForListChanges="{$checklist}"/>
			<View>
				<Query>
					<Where>
						<Eq>
							<FieldRef Name="Topic"/>
							<Value Type="Text">
								<xsl:value-of select="$topic"/>
							</Value>
						</Eq>
					</Where>
					<OrderBy>
						<FieldRef Name="LastName" Ascending="TRUE"/>
					</OrderBy>
				</Query>
				<ViewFields>
					<xsl:call-template name="ProfilesFields"/>
				</ViewFields>
			</View>
		</hbs:SPQuery>
	</xsl:template>
	
	<!--
    
    query featured research on the sidebar on home page
    
    -->		
	<xsl:template name="query-home-featured-research">
		<hbs:SPQuery Name="query-home-featured-research">
			<Site Path="~SiteCollection" />
			<List Name="HomeFeaturedResearch" />
			<Cache Key="hbs.doctoral.homefeatured" Minutes="{$cachetime}" CheckForListChanges="{$checklist}" />
			<View>
				<ViewFields>
					<xsl:call-template name="HomeFeaturedResearchFields"/>
				</ViewFields>
			</View>
		</hbs:SPQuery>
	</xsl:template>
	<!--
    
    query acadmeic calendar lists
    
    -->	
	<xsl:template name="query-events">
		<xsl:param name="checklist" select="'True'"/>
			<hbs:SPQuery Name="query-events">
				<Site Path="~SiteCollection" />
				<List Name="Events" />
				<Cache Key="hbs.doctoral.academiccalendar" Minutes="{$cachetime}" CheckForListChanges="{$checklist}" />
				<View>
					<Query>
						<OrderBy>
							<FieldRef Name="Date" />
						</OrderBy>
					</Query>
					<ViewFields>
						<xsl:call-template name="EventsFields"/>
					</ViewFields>
				</View>
			</hbs:SPQuery>
	</xsl:template>
	<!--
   Pass in a string of names as provided by people picker.
   Splits each name into this format: <Name>Alavarez, Jose</Name> <Name>Henderson, Rebecca</Name> ..
   Does a GetRemote to F&R site, performs a fullname match.
   Returns faculty information in XML format.	
   -->
	<xsl:template name="getremote-fr-rss-results">
		<xsl:param name="items"/>
		<xsl:param name="cacheKey"/>
		<xsl:param name="type"/>
		<!--split up the values-->
		<xsl:variable name="FacultyNames">
   			<xsl:for-each select="hbs:Multivalue('lookup',$items)">
		    	<FacultyName><xsl:value-of select="normalize-space(substring-before(@Val,'@'))"/></FacultyName>
	   		</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="querystring"><xsl:choose>				
			<xsl:when test="$type != 'primarytopics' and contains($items,'#')"><xsl:call-template name="build-fr-querystring"><xsl:with-param name="items" select="msxsl:node-set($FacultyNames)/*"/></xsl:call-template></xsl:when>
			<xsl:otherwise><xsl:value-of select="$items"/></xsl:otherwise></xsl:choose></xsl:variable>
		<xsl:variable name="url-parameters"><xsl:if test="$type != ''">?<xsl:value-of select="$type"/>=<xsl:value-of select="$querystring"/></xsl:if></xsl:variable>
		<hbs:GetRemote>
			<Cache Key="hbs.doctoral.fr.rss.results.{$cacheKey}" Minutes="30"/>
			<Request Url="{$getRemoteHost}/faculty/faculty-info-rss.aspx{$url-parameters}" ResultType="Xml" Method="GET">
				<Header Name="Timeout" Value="3000"/>
			</Request>
		</hbs:GetRemote>
	</xsl:template>
	<!--
	Builds querystring.  For example...
	Pass in collection: <Name>Alavarez, Jose</Name> <Name>Henderson, Rebecca</Name>
	Returns: "Jose+Alvares;Rebecca+Henderson;"
	-->
	<xsl:template name="build-fr-querystring">
		<xsl:param name="items"/>
		<xsl:for-each select="$items">
			<xsl:variable name="searchName"><xsl:value-of select="."/></xsl:variable>		
			<!--<xsl:variable name="searchName"><xsl:call-template name="build-fr-search-name"><xsl:with-param name="facultyLookup" select="."/></xsl:call-template></xsl:variable>-->
			<xsl:if test="not(contains($searchName, ' and ')) and ($searchName != '')">
				<xsl:value-of select="normalize-space($searchName)"/>
				<xsl:if test="not(position()=last())">
					<xsl:text>;</xsl:text>
				</xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!--
	Formats the name and replaces ' ' with '+'. For example... 
	Pass in "Alvarez, Jose" 
	Returns "Jose+Alvarez"
	-->
	<xsl:template name="build-fr-search-name">
		<xsl:param name="facultyLookup"/>
		<xsl:variable name="firstName" select="normalize-space(substring-after($facultyLookup,','))"/>
		<xsl:variable name="lastName" select="normalize-space(substring-before($facultyLookup,','))"/>
		<xsl:variable name="searchName"><xsl:value-of select="$firstName"/>+<xsl:value-of select="$lastName"/></xsl:variable>
		<hbs:Trim>
			<xsl:value-of select="$searchName" disable-output-escaping="yes"/>
		</hbs:Trim>
	</xsl:template>
</xsl:stylesheet>