﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   xmlns:atom="http://www.w3.org/2005/Atom"
   xmlns:ext="http://www.hbs.edu"
   exclude-result-prefixes="hbs z msxsl">
	<!--Used on courses, catalog, and students on job market pages -->
	<xsl:template name="faculty-punctuation">
		<xsl:choose>
			<xsl:when test="position() != 1 and position() != last() ">,&#160;</xsl:when>
			<xsl:when test="position() = last() and position() = 2"> and </xsl:when>
			<xsl:when test="position() = last() and position() &gt; 2">,&#160;and&#160;</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="render-faculty">
		<xsl:param name="nonhbsfaculty"/>
		<xsl:param name="facultylookup"/>
		<xsl:param name="handentered"/>
		<xsl:param name="all-faculty"/>
		<xsl:param name="type"/>
		<xsl:choose>
			<xsl:when test="$nonhbsfaculty != ''">
				<xsl:for-each select="hbs:Split($nonhbsfaculty,',')">
					<xsl:variable name="hbsfac" select="normalize-space(substring-after(substring-before(@Val,')'),'hbs\'))"/>
					<xsl:choose>
						<xsl:when test="contains(@Val,'hbs\')">	
							<xsl:variable name="match">											
								<xsl:for-each select="msxsl:node-set($all-faculty)//atom:feed/atom:entry/ext:faculty[ext:username = $hbsfac]">
									<xsl:choose><!--print first initial for job market faculty-->
										<xsl:when test="$type='jobmarket'">&lt;a href=/faculty/Pages/profile.aspx?facId=<xsl:value-of select="ext:id"/>&gt;<xsl:value-of select="hbs:ToUpper(substring(ext:firstname,1,1))"/>. <xsl:value-of select="ext:lastname"/>&lt;/a&gt;</xsl:when>
										<xsl:otherwise>&lt;a href=/faculty/Pages/profile.aspx?facId=<xsl:value-of select="ext:id"/>&gt;<xsl:value-of select="ext:fullname"/>&lt;/a&gt;</xsl:otherwise>
									</xsl:choose>
									<xsl:choose><!--for students on job market page-->
										<xsl:when test="contains(hbs:ToLower(@Val),'(co-chair)')"><xsl:text> (Co-Chair)</xsl:text></xsl:when>
										<xsl:when test="contains(hbs:ToLower(@Val),'(chair)')"><xsl:text> (Chair)</xsl:text></xsl:when>
									</xsl:choose>
								</xsl:for-each>
							</xsl:variable>
							<xsl:choose>
								<xsl:when test="$match != ''"><xsl:call-template name="faculty-punctuation"/><xsl:value-of select="$match" disable-output-escaping="yes"/></xsl:when>
								<xsl:otherwise><xsl:call-template name="faculty-punctuation"/>N/A</xsl:otherwise>
							</xsl:choose>							
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="faculty-punctuation"/><xsl:value-of select="normalize-space(@Val)" disable-output-escaping="yes"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:when>
			<xsl:when test="$facultylookup != ''">
				<xsl:for-each select="hbs:Multivalue('lookup',$facultylookup)">
					<xsl:variable name="match">
						<xsl:variable name="hbsfac"><xsl:value-of select="normalize-space(substring-before(@Val,'@'))"/></xsl:variable>
						<xsl:for-each select="msxsl:node-set($all-faculty)//atom:feed/atom:entry/ext:faculty[ext:username = $hbsfac]">
							<xsl:choose>
								<!-- profile search page only --><xsl:when test="$type = 'profilefullname'"><xsl:value-of select="ext:firstname"/>&#160;<xsl:value-of select="ext:lastname"/></xsl:when>
								<!--profile search and profile details --><xsl:when test="$type = 'profilefirstname'"><xsl:value-of select="ext:firstname"/></xsl:when>
								<xsl:otherwise>&lt;a href=http://www.hbs.edu/faculty/Pages/profile.aspx?facId=<xsl:value-of select="ext:id"/>&gt;<xsl:value-of select="ext:fullname"/>&lt;/a&gt;</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</xsl:variable>
					<xsl:choose>
						<xsl:when test="$match != ''"><xsl:call-template name="faculty-punctuation"/><xsl:value-of select="$match" disable-output-escaping="yes"/></xsl:when>
						<xsl:otherwise><xsl:call-template name="faculty-punctuation"/>N/A</xsl:otherwise>
					</xsl:choose>				
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$handentered" disable-output-escaping="yes"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--Used on profile search page only -->
	<xsl:template name="collaboration-people">
		<xsl:param name="item"/>
		<xsl:value-of select="normalize-space($item/@ows_FirstName1)"/>&#160;<xsl:value-of select="normalize-space($item/@ows_LastName)"/><xsl:if test="$item/@ows_SecondStudentFirstName != '' and $item/@ows_SecondStudentLastName != ''">, <xsl:value-of select="normalize-space($item/@ows_SecondStudentFirstName)"/>&#160;<xsl:value-of select="normalize-space($item/@ows_SecondStudentLastName)"/></xsl:if> &amp;  <xsl:if test="$item/@ows_SecondStudentFirstName != '' and $item/@ows_SecondStudentLastName != ''"><br/></xsl:if>    </xsl:template>
	<!--Used on profile search and profile details pages -->
	<xsl:template name="collaboration-people-firstnames">
		<xsl:param name="item"/>
		<xsl:value-of select="normalize-space($item/@ows_FirstName1)"/>
		<xsl:if test="$item/@ows_SecondStudentFirstName != '' and $item/@ows_SecondStudentLastName != ''">,&#160;<xsl:value-of select="normalize-space($item/@ows_SecondStudentFirstName)"/></xsl:if> &amp;     </xsl:template>
	<xsl:template name="month-name">
		<xsl:param name="month"/>
		<xsl:choose>
			<xsl:when test="$month = 1">Jan</xsl:when>
			<xsl:when test="$month = 2">Feb</xsl:when>
			<xsl:when test="$month = 3">Mar</xsl:when>
			<xsl:when test="$month = 4">Apr</xsl:when>
			<xsl:when test="$month = 5">May</xsl:when>
			<xsl:when test="$month = 6">Jun</xsl:when>
			<xsl:when test="$month = 7">Jul</xsl:when>
			<xsl:when test="$month = 8">Aug</xsl:when>
			<xsl:when test="$month = 9">Sep</xsl:when>
			<xsl:when test="$month = 10">Oct</xsl:when>
			<xsl:when test="$month = 11">Nov</xsl:when>
			<xsl:when test="$month = 12">Dec</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!--Render custom breadcrumb's start nodes-->
	<xsl:template name="render-breadcrumb-start">
		<a href="http://www.hbs.edu/" class="ink">Harvard Business School</a>
		<span class="txt-arrow">&#x2192;</span>
		<a href="/doctoral/" class="ink">Doctoral</a>
		<span class="txt-arrow">&#x2192;</span>
	</xsl:template>
	<!--Render custom breadcrumb for profiles-->
	<xsl:template name="render-breadcrumb-profiles">
		<xsl:param name="parentItems"/>
		<xsl:param name="items"/>
		<xsl:param name="activate"/>
		<xsl:call-template name="render-breadcrumb-start"/>
		<xsl:call-template name="render-breadcrumb-parent-dropdown">
			<xsl:with-param name="items" select="$parentItems"/>
			<xsl:with-param name="activate" select="'Students &amp; Faculty'"/>
		</xsl:call-template>
		<span class="txt-arrow">&#x2192;</span>
		<a href="/doctoral/research-community/students-faculty/Pages/default.aspx" class="ink">Students &amp; Faculty</a>
	</xsl:template>
	<!--
	Render parent breadcrumb dropdown. This is used in conjunction with custom breadcurmbs.
	-->
	<xsl:template name="render-breadcrumb-parent-dropdown">
		<xsl:param name="items"/>
		<xsl:param name="activate"/>
		<xsl:variable name="landingPage" select="$items/Node/Title"></xsl:variable>
		<xsl:variable name="landingPageUrl" select="$items/Node/Url"></xsl:variable>
		<div class="dropdown-container">
			<a class="ink nu dropdown-toggle" href="{$landingPageUrl}">
				<xsl:value-of select="$landingPage" disable-output-escaping="yes"/>
				<span class="icon-select">
				</span>
			</a>
			<ul class="dropdown-menu nu">
				<li>
					<a class="white" href="{$landingPageUrl}">
						<xsl:value-of select="$landingPage" disable-output-escaping="yes"/>
					</a>
				</li>
				<li class="divider inherit-bg">
				</li>
				<xsl:for-each select="$items/Node/Node">
					<xsl:variable name="theTitle"><xsl:value-of select="Title"/></xsl:variable>
					<li>
						<a href="{Url}" class="white">
							<xsl:choose>
								<xsl:when test="hbs:ToLower($activate) = hbs:ToLower($theTitle)">
									<xsl:attribute name="class">active inherit-color</xsl:attribute>
								</xsl:when>
							</xsl:choose>
							<xsl:value-of select="$theTitle" disable-output-escaping="yes"/>
						</a>
					</li>
				</xsl:for-each>
			</ul>
		</div>
	</xsl:template>
	<!--Used on the faculty by interest pages-->
	<xsl:template name="render-faculty-profile">
		<xsl:param name="item"/>
		<xsl:param name="primary-topic"/>
		<xsl:variable name="isemeriti"><xsl:value-of select="$item/ext:isemeriti"/></xsl:variable>
		<xsl:variable name="facultyID"><xsl:value-of select="$item/ext:id"/></xsl:variable>
		<xsl:variable name="facultyFullName"><xsl:value-of select="$item/ext:fullname" /></xsl:variable>
		<xsl:variable name="facultyTitle"><xsl:value-of select="hbs:Replace(hbs:Replace($item/ext:title,'&lt;br&gt;','; '),',','; ')"/></xsl:variable>
		<xsl:variable name="ristopic-cleaned">begin<xsl:value-of select="hbs:Replace($item/ext:risprimarytopics,';# ','end;#begin')" disable-output-escaping="yes"/>end</xsl:variable>
		<!--so we can compare primary topic of account to ristopocs like account and manaagement to get an exact match-->
		<xsl:variable name="primarytopic-cleaned">begin<xsl:value-of select="$primary-topic" disable-output-escaping="yes"/>end</xsl:variable>
		<xsl:if test="contains($ristopic-cleaned,$primarytopic-cleaned) and contains($isemeriti,'0')">
			<li class="nu" >
				<a href="/faculty/Pages/profile.aspx?facId={$facultyID}">
					<xsl:value-of select="$facultyFullName" disable-output-escaping="yes"/>
				</a>
			</li>
		</xsl:if>
	</xsl:template>
	<xsl:template name="render-event">
		<xsl:param name="item"/>
		<xsl:variable name="datestr" select="substring-before($item/@ows_Date,' ')"/>
		<xsl:variable name="monthnum" select="substring-before(substring-after($datestr,'-'),'-')"/>
		<xsl:variable name="daynum" select="substring-after(substring-after($datestr,'-'),'-')"/>
		<xsl:variable name="year" select="substring-before($datestr,'-')"/>
		<xsl:variable name="monthstr">
				<xsl:call-template name="month-name">
						<xsl:with-param name="month" select="$monthnum"/>
				</xsl:call-template>
		</xsl:variable>
		<li class="media">
			<div class="row">
				<div class="span1">
					<span class="event-thumbnail no-hover">
						<xsl:call-template  name="render-event-thumbnail">
							<xsl:with-param name="monthstr" select="$monthstr"/>
							<xsl:with-param name="isTBD" select="$item/@ows_IsTBD"/>
							<xsl:with-param name="daynum" select="$daynum"/>
						</xsl:call-template>
					</span>
				</div>
				<div class="span6" data-wcm-edit-url="/doctoral/Lists/Events/EditForm.aspx?ID={$item/@ows_ID}">
					<ul class="linear mu-uc black">
						<li>
							<xsl:choose>
								<xsl:when test="$item/@ows_MultiDates">
									<xsl:value-of select="normalize-space($item/@ows_MultiDates)" disable-output-escaping="yes"/>&#160;</xsl:when>
								<xsl:otherwise>
									<xsl:value-of disable-output-escaping="yes" select="$daynum"/>&#160;<xsl:value-of disable-output-escaping="yes" select="$monthstr"/>&#160;</xsl:otherwise>
							</xsl:choose>
							<xsl:value-of select="$year" disable-output-escaping="yes"/>
						</li>
					</ul>
					<h4 class="kappa">
						<xsl:value-of select="$item/@ows_Title" disable-output-escaping="yes"/>
					</h4>
					<div class="nu ink">
						<xsl:value-of select="$item/@ows_Description" disable-output-escaping="yes"/>
					</div>
				</div>
			</div>
		</li>
	</xsl:template>
	<xsl:template name="render-event-thumbnail">
		<xsl:param name="monthstr"/>
		<xsl:param name="isTBD"/>
		<xsl:param name="daynum"/>
		<xsl:value-of select="$monthstr" disable-output-escaping="yes"/>
		<xsl:choose>
			<xsl:when test="$isTBD = 'Yes'">
				<span style="background-color:#878787;">
					<xsl:text>TBD</xsl:text>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<span>
					<xsl:value-of select="$daynum" disable-output-escaping="yes"/>
				</span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>