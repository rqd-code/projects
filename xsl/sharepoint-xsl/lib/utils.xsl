﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
   
   
  <!--Global Variables-->
  <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" /> 
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" /> 
  <xsl:variable name="space"><xsl:text> </xsl:text></xsl:variable> <!--&#160; b/c doesn't cause a line break when it should-->
	
<!--Extract Image Path Only (gets the value of src=) -->	
<xsl:template name="ExtractImagePath">
	<xsl:param name="imageTag"/>
	<xsl:choose><xsl:when test="$imageTag"><xsl:value-of select="substring-before(substring-after($imageTag,'src=&quot;'), '&quot; ')"/></xsl:when>
		<xsl:otherwise>none</xsl:otherwise>
	</xsl:choose>
</xsl:template>

   
   <!--
   
   A to Z letters
   
   -->
   <xsl:template name="letters">
   		<letter uc="A" lc="a"/>
		<letter uc="B" lc="b"/>
		<letter uc="C" lc="c"/>
		<letter uc="D" lc="d"/>
		<letter uc="E" lc="e"/>
		<letter uc="F" lc="f"/>
		<letter uc="G" lc="g"/>
		<letter uc="H" lc="h"/>
		<letter uc="I" lc="i"/>
		<letter uc="J" lc="j"/>
		<letter uc="K" lc="k"/>
		<letter uc="L" lc="l"/>
		<letter uc="M" lc="m"/>
		<letter uc="N" lc="n"/>
		<letter uc="O" lc="o"/>
		<letter uc="P" lc="p"/>
		<letter uc="Q" lc="q"/>
		<letter uc="R" lc="r"/>
		<letter uc="S" lc="s"/>
		<letter uc="T" lc="t"/>
		<letter uc="U" lc="u"/>
		<letter uc="V" lc="v"/>
		<letter uc="W" lc="w"/>
		<letter uc="X" lc="x"/>
		<letter uc="Y" lc="y"/>
		<letter uc="Z" lc="z"/>
	</xsl:template>
      <!--
   
   month Name for event dates RQD, 8/20
   
   -->

 	<xsl:template name="month-name">
		<xsl:param name="month"/>
		<xsl:choose>
			<xsl:when test="$month = 1">Jan</xsl:when>
			<xsl:when test="$month = 2">Feb</xsl:when>
			<xsl:when test="$month = 3">Mar</xsl:when>
			<xsl:when test="$month = 4">Apr</xsl:when>
			<xsl:when test="$month = 5">May</xsl:when>
			<xsl:when test="$month = 6">Jun</xsl:when>
			<xsl:when test="$month = 7">Jul</xsl:when>
			<xsl:when test="$month = 8">Aug</xsl:when>
			<xsl:when test="$month = 9">Sep</xsl:when>
			<xsl:when test="$month = 10">Oct</xsl:when>
			<xsl:when test="$month = 11">Nov</xsl:when>
			<xsl:when test="$month = 12">Dec</xsl:when>
		</xsl:choose>
	</xsl:template>
      <!--
   
   full month Name for event dates RQD, 8/20
   
   -->
	
	<xsl:template name="month-fullname">
		<xsl:param name="month"/>
		<xsl:choose>
			<xsl:when test="$month = 1">January</xsl:when>
			<xsl:when test="$month = 2">February</xsl:when>
			<xsl:when test="$month = 3">March</xsl:when>
			<xsl:when test="$month = 4">April</xsl:when>
			<xsl:when test="$month = 5">May</xsl:when>
			<xsl:when test="$month = 6">June</xsl:when>
			<xsl:when test="$month = 7">July</xsl:when>
			<xsl:when test="$month = 8">August</xsl:when>
			<xsl:when test="$month = 9">September</xsl:when>
			<xsl:when test="$month = 10">October</xsl:when>
			<xsl:when test="$month = 11">November</xsl:when>
			<xsl:when test="$month = 12">December</xsl:when>
		</xsl:choose>
	</xsl:template>
	
  
   
	<!--
   
   Splits
   
   -->
	<xsl:template name="util-split">
		<xsl:param name="field" />
		<xsl:param name="delim" />
		<xsl:choose>
			<xsl:when test="contains($field, $delim)">
				<Item>
					<xsl:attribute name="Val"><xsl:value-of select="normalize-space(substring-before($field,$delim))" /></xsl:attribute>
				</Item>
				<xsl:call-template name="util-split">
					<xsl:with-param name="field" select="substring-after($field, $delim)" />
					<xsl:with-param name="delim" select="$delim" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<Item>
					<xsl:attribute name="Val"><xsl:value-of select="normalize-space($field)" /></xsl:attribute>
				</Item>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
		
	<!--
   
   Multivalue Splits
   
   -->
	<xsl:template name="util-split-multivalue">
		<xsl:param name="field" />
		<xsl:call-template name="util-output-tokens">
			<xsl:with-param name="list" select="$field" />
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="util-output-tokens">
		<xsl:param name="list" />
		<xsl:variable name="newlist" select="concat(normalize-space($list), ' ')" />
		<xsl:variable name="first" select="substring-before($newlist, ';#')" />
		<xsl:variable name="remaining" select="substring-after($newlist, ';#')" />
		<xsl:variable name="newlist2" select="concat(normalize-space($remaining), ' ')" />
		<xsl:variable name="first2" select="substring-before($newlist2, ';#')" />
		<xsl:variable name="remaining2" select="substring-after($newlist2, ';#')" />
		<xsl:if test="$first">
		<Item>
			<xsl:attribute name="Key"><xsl:value-of select="$first" /></xsl:attribute>
			<xsl:choose>
				<xsl:when test="$first2">
					<xsl:attribute name="Val"><xsl:value-of select="$first2" /></xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="Val"><xsl:value-of select="normalize-space($newlist2)" /></xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
		</Item>
		</xsl:if>
		<xsl:if test="$remaining2">
			<xsl:call-template name="util-output-tokens">
				<xsl:with-param name="list" select="$remaining2" />
			</xsl:call-template>
		</xsl:if>
	</xsl:template>


	<!--
	
	String Replace
	
	-->
	<xsl:template name="util-replace-string">
		<xsl:param name="text"/>
		<xsl:param name="replace"/>
		<xsl:param name="with"/>
		<xsl:choose>
			<xsl:when test="contains($text,$replace)">
				<xsl:value-of select="substring-before($text,$replace)"/>
				<xsl:value-of select="$with"/>
				<xsl:call-template name="util-replace-string">
					<xsl:with-param name="text" select="substring-after($text,$replace)"/>
					<xsl:with-param name="replace" select="$replace"/>
					<xsl:with-param name="with" select="$with"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--
	
	Font Replace
	
	-->
	<xsl:template name="util-replace-fonts">
		<xsl:param name="text"/>
		<xsl:variable name="temp1">
		<xsl:call-template name="util-replace-string">
			<xsl:with-param name="text"><xsl:value-of select="$text"/></xsl:with-param>
			<xsl:with-param name="replace" select="'style='"/>
			<xsl:with-param name="with" select="'xstyle='"/>
		</xsl:call-template>
		</xsl:variable>
		<xsl:call-template name="util-replace-string">
			<xsl:with-param name="text"><xsl:value-of select="$temp1"/></xsl:with-param>
			<xsl:with-param name="replace" select="'face='"/>
			<xsl:with-param name="with" select="'xface='"/>
		</xsl:call-template>
	</xsl:template>
	<!--
	
	URL Encoding
	
	-->
	<xsl:variable name="ascii"> !"#$%&amp;'()*+,-./0123456789:;&lt;=&gt;?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~</xsl:variable>
	<xsl:variable name="latin1">&#160;&#161;&#162;&#163;&#164;&#165;&#166;&#167;&#168;&#169;&#170;&#171;&#172;&#173;&#174;&#175;&#176;&#177;&#178;&#179;&#180;&#181;&#182;&#183;&#184;&#185;&#186;&#187;&#188;&#189;&#190;&#191;&#192;&#193;&#194;&#195;&#196;&#197;&#198;&#199;&#200;&#201;&#202;&#203;&#204;&#205;&#206;&#207;&#208;&#209;&#210;&#211;&#212;&#213;&#214;&#215;&#216;&#217;&#218;&#219;&#220;&#221;&#222;&#223;&#224;&#225;&#226;&#227;&#228;&#229;&#230;&#231;&#232;&#233;&#234;&#235;&#236;&#237;&#238;&#239;&#240;&#241;&#242;&#243;&#244;&#245;&#246;&#247;&#248;&#249;&#250;&#251;&#252;&#253;&#254;&#255;</xsl:variable>
	<xsl:variable name="safe">!'()*-.0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz~</xsl:variable>
	<xsl:variable name="hex" >0123456789ABCDEF</xsl:variable>
	<xsl:template name="util-url-encode">
		<xsl:param name="str"/>
		<xsl:if test="$str">
			<xsl:variable name="first-char" select="substring($str,1,1)"/>
			<xsl:choose>
				<xsl:when test="contains($safe,$first-char)">
					<xsl:value-of select="$first-char"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="codepoint"> 
            <xsl:choose> 
              <xsl:when test="contains($ascii,$first-char)"> 
                <xsl:value-of select="string-length(substring-before($ascii,$first-char)) + 32"/> 
              </xsl:when> 
              <xsl:when test="contains($latin1,$first-char)"> 
                <xsl:value-of select="string-length(substring-before($latin1,$first-char)) + 160"/> 
              </xsl:when> 
              <xsl:otherwise> 
                <xsl:message terminate="no">Warning: string contains a character that is out of range! Substituting "?".</xsl:message> 
                <xsl:text>63</xsl:text> 
              </xsl:otherwise> 
            </xsl:choose> 
          </xsl:variable>
					<xsl:variable name="hex-digit1" select="substring($hex,floor($codepoint div 16) + 1,1)"/>
					<xsl:variable name="hex-digit2" select="substring($hex,$codepoint mod 16 + 1,1)"/>
					<xsl:value-of select="concat('%',$hex-digit1,$hex-digit2)"/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="string-length($str) &gt; 1">
				<xsl:call-template name="util-url-encode">
					<xsl:with-param name="str" select="substring($str,2)"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	
	
	
	
</xsl:stylesheet>