﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
    <xsl:param name="args"/>
    <xsl:output omit-xml-declaration="yes" method="html"/>
	<xsl:template match="/Root">
	  <Step1>
	    <xsl:if test="hbs:Request/debug='head'"><hbs:Debug/></xsl:if>

	    <Request>
	      <xsl:copy-of select="hbs:Request/SERVER_NAME"/>
	      <xsl:copy-of select="hbs:Request/LOCAL_ADDR"/>
	      <xsl:copy-of select="hbs:Request/PATH_INFO"/>
	    </Request>
	    <hbs:GetTime/>
	    <hbs:GetPage/>
		<UniversalHead>
	        <hbs:GetRemote>
				<xsl:choose>
				  <xsl:when test="contains(hbs:Request/SERVER_NAME,'dev') or contains(hbs:Request/SERVER_NAME, 'qa')">
					<Request Url="http://webdev.hbs.edu/shared/ssi/universal.head.html" ResultType="Text">
	                   <Header Name="Timeout" Value="3000"/>
	                </Request>
                  </xsl:when>
                  <xsl:otherwise>
				    <Cache Key="hbs.universal.head" RecoverOnError="True" Minutes="720"/>
					<Request Url="http://www.hbs.edu/shared/ssi/universal.head.html" ResultType="Text">
	                   <Header Name="Timeout" Value="3000"/>
	                </Request>
                  </xsl:otherwise>
                </xsl:choose>
	        </hbs:GetRemote>
		</UniversalHead>
	    <hbs:Repeat/>
	  </Step1>
	</xsl:template>
	<!--
	
	Set Page titles
	
	-->
	<xsl:template match="/Step1">
	  <Render>
	     <xsl:copy-of select="Request|Head|hbs:Time|hbs:Page|UniversalHead"/>
	     <hbs:Repeat/>
	  </Render>
	</xsl:template>

	<xsl:template match="/Render" xml:space="preserve">
	    <xsl:choose>
		    <xsl:when test="hbs:Page/z:row/@ows_Title = 'Home'">
		       <title>Doctoral - Harvard Business School</title>
		    </xsl:when>
		    <!--we will do these by hand-->
		    <xsl:when test="hbs:Page/z:row/@ows_Title = 'Profile Details'"></xsl:when>
		    <xsl:when test="hbs:Page/z:row/@ows_Title = 'topic'"></xsl:when>
		    <xsl:when test="hbs:Page/z:row">
		       <title><xsl:value-of select="hbs:Page/z:row/@ows_Title"/> - Doctoral - Harvard Business School</title>
		    </xsl:when>
	    </xsl:choose>
		
	
	    <xsl:variable name="root" xml:space="default">/doctoral</xsl:variable>
        <script type="text/javascript" src="http://www.hbs.edu/shared/js/jquery/1.8.3/jquery.js"></script>
		<xsl:value-of select="UniversalHead/hbs:Response" disable-output-escaping="yes"/>

		<script type="text/javascript" src="/Style Library/hbs/js/plugins/jquery.timeago.js"></script>
        
	    <script type="text/javascript" src="http://www.hbs.edu/shared/js/widgets.js"></script>
        <script type="text/javascript" src="{$root}/Style Library/hbs/js/doctoral.js"></script>

        <script type="text/javascript" src="http://www.hbs.edu/js/analytics.js"></script>
        <script type="text/javascript">analytics.settings({profile:'doctoral'})</script>

        <xsl:if test="$args = 'authoring'">
        	<script type="text/javascript">
        	    if (/prod/.test(document.location.href)) { require.config({paths: {wcm: 'http://www.hbs.edu/sharepoint/wcm/js'}});
   		        } else { require.config({paths: {wcm: 'http://webdev.hbs.edu/sharepoint/wcm/js'}}); }
				require(["wcm/wcm.main"], function (WCM) { WCM.installAuthoring();});
            </script>
        </xsl:if>
        <link rel="stylesheet" href="{$root}/Style Library/hbs/css/doctoral.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="{$root}/Style Library/hbs/css/print.css" type="text/css" media="print" />
        <link rel="image_src" href="http://www.hbs.edu/images/site/sharelogo.jpg" />
		<!--<script type="text/javascript" src="http://www.hbs.edu/js/mdetect.js"></script> no mobile site in bei-->

        <!--<meta name="HBSSearchSubset" content="doctoral"/>-->
	    <meta name="PageBuildDateTime" content="{hbs:Time/Sortable} on {Request/LOCAL_ADDR}"/>
	    
    </xsl:template>
</xsl:stylesheet>


