﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
   
	<xsl:output method="html" omit-xml-declaration="yes"/>

	<xsl:template match="/Root">
		<Render>
           <xsl:if test="hbs:Request/debug='header'"><hbs:Debug/></xsl:if>
           <Request><xsl:copy-of select="hbs:Request/PATH_INFO"/></Request>
		   <hbs:Repeat/>
		   
		   <hbs:GetPage></hbs:GetPage>
		</Render>
	</xsl:template>

	<xsl:template match="/Render">
	
	<xsl:variable name="headerColor">
	   <xsl:choose>
	      <xsl:when test="hbs:Page/z:row">
			<xsl:value-of select="hbs:Page/z:row/@ows_PrimaryColor"/>-inherit
	      </xsl:when>
	      <xsl:otherwise>
	        coral-inherit
	      </xsl:otherwise>
	   </xsl:choose>
    </xsl:variable>
	
	<xsl:text disable-output-escaping="yes">&lt;div class="</xsl:text><xsl:value-of select="normalize-space($headerColor)"/><xsl:text disable-output-escaping="yes">"&gt;</xsl:text>
	
    <div class="inherit-bg site-header">
     <div class="container">
	    <h1 class="gamma-uc"><a href="/doctoral/" class="white">Doctoral Programs</a></h1>
	    <ul class="toolbar">
	    
	        <li class="first">
	        <xsl:if test="contains(Request/PATH_INFO,'for-alumni')"><xsl:attribute name="class">active</xsl:attribute></xsl:if>
	        <a class="first  nav-social-media" href="https://inq.applyyourself.com/?ID=hbs-dba&amp;pid=1501"><span class="icon-social-media"></span> Introduce Yourself</a></li>
	        <li class="second">
	        <xsl:if test="contains(Request/PATH_INFO,'/admissions/Pages/apply-to-program.aspx')"><xsl:attribute name="class">active</xsl:attribute></xsl:if>
	        <a href="/doctoral/admissions/Pages/apply-to-program.aspx" class="nav-apply"><span class="icon-apply"></span> Apply Online</a></li>	  
	        <li>
	        <xsl:if test="contains(Request/PATH_INFO,'/placement/Pages/students-job-market.aspx')"><xsl:attribute name="class">active</xsl:attribute></xsl:if>
	        <a href="/doctoral/placement/Pages/students-job-market.aspx">Students on the Job Market</a></li>
      
	    </ul>
	    <div class="hr"></div>
	    <ul class="navbar">
	        <li>
	        <xsl:if test="contains(Request/PATH_INFO,'program-overview')"><xsl:attribute name="class">active</xsl:attribute></xsl:if>
	        <a class="first" href="/doctoral/program-overview/Pages/default.aspx">Program Overview</a></li>
			<li class="divider"></li>
	        <li>
	        <xsl:if test="contains(Request/PATH_INFO,'areas-of-study')"><xsl:attribute name="class">active</xsl:attribute></xsl:if>
	        <a href="/doctoral/areas-of-study/Pages/default.aspx">Areas of Study</a></li>
			<li class="divider"></li>
	        <li>
	        <xsl:if test="contains(Request/PATH_INFO,'research-community')"><xsl:attribute name="class">active</xsl:attribute></xsl:if>
	        <a href="/doctoral/research-community/Pages/default.aspx">Research Community</a></li>
			<li class="divider"></li>
	        <li>
	        <xsl:if test="contains(Request/PATH_INFO,'admissions')"><xsl:attribute name="class">active</xsl:attribute></xsl:if>
	        <a href="/doctoral/admissions/Pages/default.aspx">Admissions</a></li>
			<li class="divider"></li>
	        <li>
	        <xsl:if test="contains(Request/PATH_INFO,'placement')"><xsl:attribute name="class">active</xsl:attribute></xsl:if>
	        <a href="/doctoral/placement/Pages/default.aspx">Placement</a></li>
	        <li class="divider"></li>
	        <li>
	        <xsl:if test="contains(Request/PATH_INFO,'registrar')"><xsl:attribute name="class">active</xsl:attribute></xsl:if>
	        <a href="/doctoral/registrar/Pages/default.aspx">Registrar</a></li>
	        <li class="divider"></li>
	        <li>
	        <xsl:if test="contains(Request/PATH_INFO,'faqs')"><xsl:attribute name="class">active</xsl:attribute></xsl:if>
	        <a class="last" href="/doctoral/faqs/Pages/default.aspx">FAQS</a></li>


	    </ul>
	    <div class="hr"></div>
	 </div>	 
	 <div class="back-to-top">
         <a href="#" class="icon-back-to-top"></a>
         </div>
	</div> 
	    
	    
	</xsl:template>
</xsl:stylesheet>

