﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
xmlns:hbs="urn:hbs"
xmlns:z="#RowsetSchema"
xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime"  
xmlns:msxsl="urn:schemas-microsoft-com:xslt"
exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:import href="lib/utils.xsl"/>
	<xsl:import href="lib/renderings.xsl"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<FacultyLookupResults>
				<xsl:call-template name="getremote-fr-rss-results">
					<xsl:with-param name="cacheKey">jobmarket</xsl:with-param>
				</xsl:call-template>
			</FacultyLookupResults>
			<xsl:call-template name="query-on-job-market"/>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<xsl:template match="/Render">
		<div id="on-job-market">
			<p class="paragraph">Information will be updated throughout the summer and fall.</p>
			<xsl:for-each select="hbs:QueryResults/z:row">
				<xsl:if test="position()=1 or @ows_Topic != preceding-sibling::z:row[1]/@ows_Topic">
					<xsl:call-template name="sectionhead">
						<xsl:with-param name="item" select="."/>
						<xsl:with-param name="addtop"><xsl:if test="position() != 1">yes</xsl:if></xsl:with-param>
					</xsl:call-template>
				</xsl:if>
				<div data-wcm-edit-url="/doctoral/Lists/StudentsOnJobMarket/EditForm.aspx?ID={@ows_ID}">
					<xsl:if test="@ows_Topic = preceding-sibling::z:row[1]/@ows_Topic">
						<div class="shim10">
						</div>
					</xsl:if>
					<div class="student-item">
						<h2 class="eta">
							<xsl:value-of select="@ows_Title" disable-output-escaping="yes"/>
						</h2>
						<div class="mu-uc abstact">Abstract:</div>
						<dl class="plusminus">
							<dt class="paragraph">
								<a class="paragraph">
									<xsl:value-of select="@ows_AbstractTitle" disable-output-escaping="yes"/>
								</a>
							</dt>
							<dd class="paragraph">
								<xsl:value-of select="@ows_AbstractBlurb" disable-output-escaping="yes"/>
							</dd>
						</dl>
						<div class="nu facultyadvisors">Faculty Advisor(s): 
									<xsl:call-template name="render-faculty">
										<xsl:with-param name="nonhbsfaculty"><xsl:value-of select="@ows_NonHBSFaculty" disable-output-escaping="yes"/></xsl:with-param>
										<xsl:with-param name="all-faculty"><xsl:copy-of select="/Render/FacultyLookupResults"/></xsl:with-param>
										<xsl:with-param name="type">jobmarket</xsl:with-param>
									</xsl:call-template>								
							</div>
						<div class="nu cvinfo">
							<xsl:if test="@ows_CoverLetterLink">
								<a>
									<xsl:attribute name="href"><xsl:value-of select="normalize-space(@ows_CoverLetterLink)" disable-output-escaping="yes"/></xsl:attribute>Curriculum Vitae</a>&#160;&#160;<span class="ash">|</span>&#160;&#160;</xsl:if>
							<xsl:if test="@ows_EMail">
								<xsl:variable name="email"><xsl:value-of select="@ows_EMail" disable-output-escaping="yes"/></xsl:variable>
								<a class="to" href="mailto:{$email}">Send an Email</a>&#160;&#160;<span class="ash">|</span>&#160;&#160;</xsl:if>
							<xsl:if test="@ows_WebsiteLink">
								<a>
									<xsl:attribute name="href"><xsl:value-of select="normalize-space(@ows_WebsiteLink)" disable-output-escaping="yes"/></xsl:attribute>Website</a>
							</xsl:if>
						</div>
					</div>
				</div>
			</xsl:for-each>
		</div>
	</xsl:template>
	<xsl:template name="sectionhead">
		<xsl:param name="item"/>
		<xsl:param name="addtop"/>
		<h3>
			<xsl:if test="$addtop = 'yes'">
				<xsl:attribute name="class">top-space</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="substring-after($item/@ows_Topic,';#')"/>
		</h3>
		<div class="hr4">
		</div>
	</xsl:template>
</xsl:stylesheet>
