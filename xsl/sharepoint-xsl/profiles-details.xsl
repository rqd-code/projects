﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   xmlns:atom="http://www.w3.org/2005/Atom"
   exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:import href="lib/utils.xsl"/>
	<xsl:import href="lib/renderings.xsl"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:template match="/Root">
		<Step1>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<Profile>
				<xsl:call-template name="query-profiles-by-id">
					<xsl:with-param name="profileId" select="hbs:Request/profile"/>
				</xsl:call-template>
			</Profile>
			<hbs:Repeat/>
		</Step1>
	</xsl:template>
	<xsl:template match="/Step1">
		<Step2>
				<xsl:copy-of select="Profile"/>	
				<xsl:if test="Profile/hbs:QueryResults/z:row/@ows_ContentType='Student-Faculty-Profile'">	
					<FacultyLookupResults>
						<xsl:call-template name="getremote-fr-rss-results">
							<xsl:with-param name="items"><xsl:value-of select="Profile/hbs:QueryResults/z:row/@ows_FacultySelector"/></xsl:with-param>
							<xsl:with-param name="type">names</xsl:with-param>
							<xsl:with-param name="cacheKey"><xsl:value-of select="normalize-space(Profile/hbs:QueryResults/z:row/@ows_Title)"/></xsl:with-param>
						</xsl:call-template>
					</FacultyLookupResults>
				</xsl:if>	
			<hbs:Repeat/>				
		</Step2>
	</xsl:template>	
	<xsl:template match="/Step2">
		<Render>
			<xsl:copy-of select="Profile|FacultyLookupResults"/>
			<xsl:variable name="row" select="Profile/hbs:QueryResults/z:row"/>
			<xsl:variable name="fullname">
				<xsl:choose>
					<xsl:when test="$row/@ows_ContentType='Student-Faculty-Profile'">
						<xsl:call-template name="collaboration-people-firstnames">
							<xsl:with-param name="item" select="$row"/>			
						</xsl:call-template>
						<xsl:call-template name="render-faculty">
							<xsl:with-param name="facultylookup"><xsl:value-of select="$row/@ows_FacultySelector" disable-output-escaping="yes"/></xsl:with-param>
							<xsl:with-param name="all-faculty"><xsl:copy-of select="/Step2/FacultyLookupResults"/></xsl:with-param>
							<xsl:with-param name="type">profilefirstname</xsl:with-param>
							<xsl:with-param name="handentered"><xsl:value-of select="$row/@ows_FacultyFirstName" disable-output-escaping="yes"/></xsl:with-param>
						</xsl:call-template>	
					</xsl:when>
					<xsl:otherwise>
							<xsl:value-of select="normalize-space($row/@ows_FirstName1)" disable-output-escaping="yes"/>&#160;<xsl:value-of select="normalize-space($row/@ows_LastName)" disable-output-escaping="yes"/>
					</xsl:otherwise>
				</xsl:choose>			
			</xsl:variable>
			<hbs:SetControl Name="PageTitle">
				<xsl:value-of select="$fullname"/> - Students &amp; Faculty - Research Community - Doctoral - Harvard Business School     
           </hbs:SetControl>
			<hbs:SetControl Name="ProfileHeader">
				<xsl:variable name="img"><xsl:call-template name="ExtractImagePath"><xsl:with-param name="imageTag" select="$row/@ows_ProfilePhoto"></xsl:with-param></xsl:call-template></xsl:variable>
				<h1>Students &amp; Faculty</h1>
				<xsl:choose>
					<xsl:when test="$row/@ows_ContentType='Student-Faculty-Profile'">
						<h2 class="beta" data-wcm-edit-url="/doctoral/Lists/Profiles/EditForm.aspx?ID={$row/@ows_ID}"
		                   data-wcm-edit-label="Profile"
		                   data-wcm-new-url="/doctoral/Lists/Profiles/NewForm.aspx?RootFolder=%2Fdoctoral%2FLists%2FProfiles&amp;ContentTypeId=0x01003645C1B0F5045140993EB8D4AD3AC60302002C3D2E03C2456E4ABDA5E52ED0CD40B8"
		                   data-wcm-new-label="Student Profile"
		                   data-wcm-new-url2="/doctoral/Lists/Profiles/NewForm.aspx?RootFolder=%2Fdoctoral%2FLists%2FProfiles&amp;ContentTypeId=0x01003645C1B0F5045140993EB8D4AD3AC60301005C2C1CA88F69CE4894A67F3B8BC18DE9"
		                   data-wcm-new-label2="Faculty Profile">
							<xsl:call-template name="collaboration-people-firstnames">
								<xsl:with-param name="item" select="$row"/>				
							</xsl:call-template><xsl:call-template name="render-faculty">
				<xsl:with-param name="facultylookup"><xsl:value-of select="$row/@ows_FacultySelector" disable-output-escaping="yes"/></xsl:with-param>
				<xsl:with-param name="all-faculty"><xsl:copy-of select="/Step2/FacultyLookupResults"/></xsl:with-param>
				<xsl:with-param name="type">profilefirstname</xsl:with-param>
				<xsl:with-param name="handentered"><xsl:value-of select="$row/@ows_FacultyFirstName" disable-output-escaping="yes"/></xsl:with-param>
		</xsl:call-template>
						</h2>
					</xsl:when>
					<xsl:when test="$row/@ows_ContentType='Student-Profile' and $row/@ows_Affiliation = 'Alumni'">
						<h2 class="alpha" data-wcm-edit-url="/doctoral/Lists/Profiles/EditForm.aspx?ID={$row/@ows_ID}"
		                   data-wcm-edit-label="Profile"
		                   data-wcm-new-url="/doctoral/Lists/Profiles/NewForm.aspx?RootFolder=%2Fdoctoral%2FLists%2FProfiles&amp;ContentTypeId=0x01003645C1B0F5045140993EB8D4AD3AC60302002C3D2E03C2456E4ABDA5E52ED0CD40B8"
		                   data-wcm-new-label="Student Profile"
		                   data-wcm-new-url2="/doctoral/Lists/Profiles/NewForm.aspx?RootFolder=%2Fdoctoral%2FLists%2FProfiles&amp;ContentTypeId=0x01003645C1B0F5045140993EB8D4AD3AC60301005C2C1CA88F69CE4894A67F3B8BC18DE9"
		                   data-wcm-new-label2="Faculty Profile">
							<xsl:value-of select="normalize-space($row/@ows_FirstName1)"/>&#160;<xsl:value-of select="normalize-space($row/@ows_LastName)"/></h2>
					</xsl:when>
					<xsl:otherwise>
						<h2 class="alpha" data-wcm-edit-url="/doctoral/Lists/Profiles/EditForm.aspx?ID={$row/@ows_ID}"
		                   data-wcm-edit-label="Profile"
		                   data-wcm-new-url="/doctoral/Lists/Profiles/NewForm.aspx?RootFolder=%2Fdoctoral%2FLists%2FProfiles&amp;ContentTypeId=0x01003645C1B0F5045140993EB8D4AD3AC60302002C3D2E03C2456E4ABDA5E52ED0CD40B8"
		                   data-wcm-new-label="Student Profile"
		                   data-wcm-new-url2="/doctoral/Lists/Profiles/NewForm.aspx?RootFolder=%2Fdoctoral%2FLists%2FProfiles&amp;ContentTypeId=0x01003645C1B0F5045140993EB8D4AD3AC60301005C2C1CA88F69CE4894A67F3B8BC18DE9"
		                   data-wcm-new-label2="Faculty Profile">
							<xsl:value-of select="normalize-space($row/@ows_FirstName1)"/>&#160;<xsl:value-of select="normalize-space($row/@ows_LastName)"/></h2>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="$row/@ows_ContentType='Student-Faculty-Profile'">
					<div class="photo-holder">
						<div class="profile-photo">
							<img src="{$img}" width="565" height="318" alt="{$fullname}"/>
						</div>
					</div>
				</xsl:if>
			</hbs:SetControl>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<xsl:template match="/Render">
		<xsl:variable name="row" select="Profile/hbs:QueryResults/z:row"/>
		<div class="container">
			<div class="row">
				<div>
					<xsl:if test="$row/@ows_ContentType='Student-Faculty-Profile'">
						<xsl:attribute name="class">span9 center-content</xsl:attribute>
					</xsl:if>
					<xsl:if test="$row/@ows_ContentType!='Student-Faculty-Profile'">
						<xsl:attribute name="class">span9</xsl:attribute>
						<div class="cap">
						</div>
					</xsl:if>
					<blockquote class="eta-uc coral" style="text-indent:-10px">
						<xsl:if test="$row/@ows_QuoteAttribution!=''">
							<xsl:attribute name="class">eta-uc coral attribution</xsl:attribute>
						</xsl:if>
					“<xsl:value-of select="$row/@ows_Quote" disable-output-escaping="yes"/>”</blockquote>
					<xsl:if test="$row/@ows_QuoteAttribution!=''">
						<p class="credit mu-uc">— <xsl:value-of select="$row/@ows_QuoteAttribution" disable-output-escaping="yes"/></p>
					</xsl:if>
					<xsl:value-of select="$row/@ows_ProfileBlurb" disable-output-escaping="yes"/>
				</div>
				<div class="span3 sidebar vrule-offset">
					<div class="cap">
					</div>
					<div class="sidebar-item">
						<xsl:if test="$row/@ows_ContentType!='Student-Faculty-Profile'">
							<xsl:attribute name="class">sidebar-item sidebarmarginfix</xsl:attribute>
						</xsl:if>
						<xsl:variable name="alt"><xsl:value-of select="normalize-space($row/@ows_FirstName1)" disable-output-escaping="yes"/> <xsl:value-of select="normalize-space($row/@ows_LastName)" disable-output-escaping="yes"/></xsl:variable>
						<xsl:variable name="sidebarpic"><xsl:call-template name="ExtractImagePath"><xsl:with-param name="imageTag" select="$row/@ows_SidebarPhoto"></xsl:with-param></xsl:call-template></xsl:variable>
						<xsl:if test="$row/@ows_ContentType!='Student-Faculty-Profile'">
							<img src="{$sidebarpic}" border="0" width="220" height="220" class="sidebar-photo" alt="{$alt}"/>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="$row/@ows_ContentType='Student-Faculty-Profile'">
								<p>
									<strong class="mu-uc">Professor</strong>
									<br/>
									<xsl:choose>
										<xsl:when test="$row/@ows_FacultyIntroSentence != ''">
											<xsl:value-of select="$row/@ows_FacultyIntroSentence" disable-output-escaping="yes"/>
										</xsl:when>
										<xsl:when test="$row/@ows_FacultySelector != ''">
											<xsl:call-template name="render-faculty">
												<xsl:with-param name="facultylookup"><xsl:value-of select="$row/@ows_FacultySelector" disable-output-escaping="yes"/></xsl:with-param>
												<xsl:with-param name="type">profile-detail</xsl:with-param>
												<xsl:with-param name="all-faculty"><xsl:copy-of select="/Render/FacultyLookupResults"/></xsl:with-param>
											</xsl:call-template>
										</xsl:when>
										<xsl:otherwise>
												<xsl:value-of select="normalize-space($row/@ows_FacultyFirstName)" disable-output-escaping="yes"/>&#160;<xsl:value-of select="normalize-space($row/@ows_FacultyLastName)" disable-output-escaping="yes"/>										
										</xsl:otherwise>
									</xsl:choose>
								</p>
								<p>
									<strong class="mu-uc">Doctoral Student<xsl:if test="$row/@ows_SecondStudentFirstName != '' and $row/@ows_SecondStudentLastName != ''"><xsl:text>s</xsl:text></xsl:if></strong>
									<br/>
									<xsl:value-of select="normalize-space($row/@ows_FirstName1)" disable-output-escaping="yes"/>&#160;<xsl:value-of select="normalize-space($row/@ows_LastName)" disable-output-escaping="yes"/><xsl:if test="normalize-space($row/@ows_SecondStudentFirstName) = '' and normalize-space($row/@ows_SecondStudentLastName) = ''">,<br/></xsl:if>
							<xsl:if test="$row/@ows_SecondStudentFirstName != '' and $row/@ows_SecondStudentLastName != ''">
									&amp;&#160;<xsl:value-of select="normalize-space($row/@ows_SecondStudentFirstName)" disable-output-escaping="yes"/>&#160;<xsl:value-of select="normalize-space($row/@ows_SecondStudentLastName)" disable-output-escaping="yes"/>,<br/>
							</xsl:if>
							<xsl:if test="$row/@ows_Topic != ''">
								<xsl:value-of select="substring-after($row/@ows_Topic,'#')" disable-output-escaping="yes"/>
							</xsl:if>
								</p>
								<xsl:if test="$row/@ows_ProjectName != ''">
									<p>
										<strong class="mu-uc">Collaboration Project</strong>
										<br/>
										<xsl:value-of select="$row/@ows_ProjectName" disable-output-escaping="yes"/>
									</p>
								</xsl:if>
							</xsl:when>
							<xsl:when test="$row/@ows_ContentType='Student-Profile' and $row/@ows_Affiliation = 'Alumni'">
								<xsl:call-template name="student-info">
									<xsl:with-param name="row" select="$row"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="student-info">
									<xsl:with-param name="row" select="$row"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
						<div class="base">
						</div>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="student-info">
		<xsl:param name="row"/>
		<xsl:if test="$row/@ows_Placement != ''">
			<p>
				<strong class="mu-uc">Placement</strong>
				<br/>
				<xsl:value-of select="$row/@ows_Placement" disable-output-escaping="yes"/>
			</p>
		</xsl:if>
		<xsl:if test="$row/@ows_AreasOfStudy != ''">
			<p>
				<strong class="mu-uc">Area of Study</strong>
				<br/>
				<xsl:value-of select="$row/@ows_AreasOfStudy" disable-output-escaping="yes"/>
			</p>
		</xsl:if>
		<xsl:if test="$row/@ows_HomeRegion != ''">
			<p>
				<strong class="mu-uc">Home Region</strong>
				<br/>
				<xsl:value-of select="$row/@ows_HomeRegion" disable-output-escaping="yes"/>
			</p>
		</xsl:if>
		<xsl:if test="$row/@ows_UndergraduateEducation != ''">
			<p>
				<strong class="mu-uc">Education</strong>
				<br/>
				<xsl:value-of select="$row/@ows_UndergraduateEducation" disable-output-escaping="yes"/>
			</p>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet> 