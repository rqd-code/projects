﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:atom="http://www.w3.org/2005/Atom"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   xmlns:ext="http://www.hbs.edu/"
   exclude-result-prefixes="hbs z msxsl"
   >
	<xsl:import href="lib/renderings.xsl"/>
	<xsl:import href="lib/queries.xsl"/>
	<xsl:import href="lib/utils.xsl"/>
	<xsl:key name="facets" match="Facet" use="text()" />
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:template match="/Root">
		<Step1>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<Request>
				<xsl:copy-of select="hbs:Request/type | hbs:Request/page | hbs:Request/step"/>
				<xsl:if test="not(hbs:Request/page)">
					<page Default="True">1</page>
				</xsl:if>
				<xsl:if test="not(hbs:Request/step)">
					<step Default="True">100</step>
				</xsl:if>
			</Request>
			<Items>
				<xsl:call-template name="query-profiles"/>
			</Items>
			<NavBlurb>
				<xsl:call-template name="query-profile-nav-content"/>
			</NavBlurb>					
			<hbs:Repeat/>
		</Step1>
	</xsl:template>
	<!-- 
	
	Step1 - Expand the metadata about the records
	
	-->
	<xsl:template match="/Step1">
		<Step2>
			<xsl:copy-of select="Request|NavBlurb"/>
			<Items>
				<xsl:apply-templates select="Items/hbs:QueryResults/z:row" mode="expand-metadata"/>
			</Items>
			<hbs:Repeat/>
		</Step2>
	</xsl:template>
	<xsl:template match="z:row" mode="expand-metadata">
		<Item>
			<xsl:copy-of select="@*"/>
			<xsl:if test="@ows_Affiliation = 'Doctoral' and @ows_ContentType!='Student-Faculty-Profile'">
				<Facet Name="Type" SortVal="1">Students</Facet>
			</xsl:if>
			<xsl:if test="@ows_ContentType='Student-Faculty-Profile'">
				<Facet Name="Type" SortVal="2">Faculty &amp; Student Collaborations</Facet>
			</xsl:if>
			<xsl:if test="@ows_Affiliation = 'Alumni' and @ows_ContentType!='Student-Faculty-Profile'">
				<Facet Name="Type" SortVal="3">Recent Graduates</Facet>
			</xsl:if>
		</Item>
	</xsl:template>
	<!-- 
	
	Step2 - Build the facets
	
	-->
	<xsl:template match="/Step2">
		<Step3>
			<xsl:copy-of select="Items|Request|NavBlurb"/>
			<Featured>
				<xsl:copy-of select="Items/Item[@ows_Featured = 'Yes' and @ows_ContentType = 'Student-Faculty-Profile']"/>
			</Featured>
			<Facets>
				<xsl:for-each select="Items/Item/Facet[generate-id() = generate-id(key('facets',text())[1])]">
					<xsl:sort select="@SortVal">
					</xsl:sort>
					<Facet Name="{@Name}">
						<xsl:value-of select="."/>
					</Facet>
				</xsl:for-each>
			</Facets>
			<hbs:Repeat/>
		</Step3>
	</xsl:template>
	<!-- 
	
	Step3- mark the items for removal
		
	-->
	<xsl:template match="/Step3">
		<Step4>
			<xsl:copy-of select="Facets|Request|Featured|NavBlurb"/>
			<xsl:variable name="req" select="Request"/>
			<Items>
				<xsl:for-each select="Items/Item">
					<xsl:variable name="item" select="."/>
					<Item>
						<xsl:copy-of select="@*"/>
						<xsl:copy-of select="Facet"/>
						<xsl:if test="not($req/type)">
							<Match Reason="Default" LookingFor="Default"/>
						</xsl:if>
						<xsl:for-each select="$req/type">
							<xsl:variable name="lookingfor" select="."/>
							<xsl:if test="$item/Facet[@Name='Type' and text() = $lookingfor]">
								<Match Reason="Type" LookingFor="{$lookingfor}"/>
							</xsl:if>
						</xsl:for-each>
					</Item>
				</xsl:for-each>
			</Items>
			<hbs:Repeat/>
		</Step4>
	</xsl:template>
	<!-- 
	
	Step4 - remove the items that don't match
		
	-->
	<xsl:template match="/Step4">
		<Step5>
			<xsl:copy-of select="Facets|Request|Featured|NavBlurb"/>
			<Items>
				<xsl:for-each select="Items/Item">
					<xsl:choose>
						<xsl:when test="not(Match)">
						</xsl:when>
						<xsl:otherwise>
							<xsl:copy-of select="."/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</Items>
			<FacultyLookUpNames><xsl:for-each select="Items/Item[@ows_FacultySelector != '']"><xsl:value-of select="substring-after(substring-before(@ows_FacultySelector,'@'),'#')"/>;</xsl:for-each></FacultyLookUpNames>
			<hbs:Repeat/>
		</Step5>
	</xsl:template>
	<!-- 
	
	Step5 - add counts to the facets, and slice the results based on the page number
		
	-->
	<xsl:template match="/Step5">
		<Render View="Default">
			<xsl:copy-of select="Request|Featured|NavBlurb"/>
			<hbs:GetTime/>
			<xsl:variable name="count" select="count(Items/Item)"/>
			<Items Total="{$count}">
				<xsl:variable name="start" select="(number(Request/page - 1) * number(Request/step)) + 1"/>
				<xsl:variable name="end" select="number(Request/page) * number(Request/step)"/>
				<xsl:attribute name="Start"><xsl:value-of select="$start"/></xsl:attribute>
				<xsl:choose>
					<xsl:when test="$end &lt; $count">
						<xsl:attribute name="End"><xsl:value-of select="$end"/></xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="End"><xsl:value-of select="$count"/></xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="number(Request/page) * number(Request/step) &gt; $count">
					<xsl:attribute name="LastPage">1</xsl:attribute>
				</xsl:if>
				<textarea start="{$start}" end="{$end}">
				</textarea>
				<xsl:for-each select="Items/Item[position() &gt;= $start and position() &lt;= $end]">
					<xsl:copy-of select="."/>
				</xsl:for-each>
			</Items>
			<xsl:variable name="items" select="Items"/>
			<Facets>
				<xsl:for-each select="Facets/Facet">
					<Facet>
						<xsl:copy-of select="@Name"/>
						<xsl:variable name="name" select="@Name"/>
						<xsl:variable name="lookingfor" select="text()"/>
						<xsl:attribute name="Count"><xsl:value-of select="count($items/Item/Facet[@Name = $name and text() = $lookingfor])"/></xsl:attribute>
						<xsl:value-of select="."/>
					</Facet>
				</xsl:for-each>
			</Facets>
			<FacultyLookupResults>
				<xsl:call-template name="getremote-fr-rss-results">
					<xsl:with-param name="items"><xsl:value-of select="FacultyLookUpNames"/></xsl:with-param>
					<xsl:with-param name="type">names</xsl:with-param>
					<xsl:with-param name="cacheKey">profiles-listing</xsl:with-param>
				</xsl:call-template>
			</FacultyLookupResults>				
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<!-- 
	
	Step5 - Render the default view
		
	-->
	<xsl:template match="/Render[@View = 'Default']">
		<div class="container results" id="people-box">

			<div class="row">
				<div class="span3">
					<xsl:call-template name="render-facets"/>
				</div>
				<div class="span9">
					<xsl:call-template name="render-results"/>
				</div>
			</div>
		</div>
	</xsl:template>
	<!-- 
	
	FACET RENDERING
		
	-->
	<xsl:template name="render-facets">
		<div class="facets">
			<div class="hr3"/>
			<div class="shim20"/>
			<div id="profilenav" class="facet-pattern">
				<div class="toggle-container">
					<div class="kappa-uc">
						<h4>
							<a href="#" class="toggle-button black">
								People</a>
						</h4>
					</div>
					<div class="nu">
						<ul class="facet-list">
							<xsl:call-template name="render-all-button">
								<xsl:with-param name="facet" select="'type'"/>
							</xsl:call-template>
							<xsl:for-each select="/Render/Facets/Facet">
								<xsl:call-template name="render-facet-button">
									<xsl:with-param name="facet" select="'type'"/>
									<xsl:with-param name="value" select="."/>
								</xsl:call-template>
							</xsl:for-each>
							<xsl:call-template name="render-clear-button">
								<xsl:with-param name="facet" select="'type'"/>
								<xsl:with-param name="label" select="'Type'"/>
							</xsl:call-template>
						</ul>
					</div>
				</div>
			</div>
			<div class="shim20"/>
			<div class="hr3">
			</div>
			<div class="shim15">
			</div>

				<xsl:for-each select="/Render/NavBlurb/hbs:QueryResults/z:row">
					<div data-wcm-edit-url="/doctoral/Lists/MiscSettings/EditForm.aspx?ID={@ows_ID}">
						<xsl:value-of select="@ows_Content" disable-output-escaping="yes"/>
					</div>
				</xsl:for-each>
		
		</div>
	</xsl:template>
	<!-- 
	
	RENDER ALL BUTTON
		
	-->
	<xsl:template name="render-all-button">
		<xsl:param name="facet"/>
		<xsl:choose>
			<xsl:when test="/Render/Request/*[name() = $facet]">
				<li>
					<a class="ink">
						<xsl:attribute name="href">
			            <xsl:call-template name="remove-all-href">
				          <xsl:with-param name="request" select="/Render/Request"/>
				          <xsl:with-param name="param" select="$facet"/>
				        </xsl:call-template>
			        </xsl:attribute>
						<span class="icon-checkbox">
						</span>Select All</a>
				</li>
			</xsl:when>
			<xsl:otherwise><li style="margin-left:-21px;"><span class="icon-checkbox-checked"></span>Select All</li></xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- 
	
	RENDER CLEAR BUTTON
		
	-->
	<xsl:template name="render-clear-button">
		<xsl:param name="facet"/>
		<xsl:param name="label"/>
		<xsl:if test="/Render/Request/*[name() = $facet]">
			<div class="nu" style="xpadding-left:25px;padding-top:5px;"><a>
					<xsl:attribute name="href">
		            <xsl:call-template name="remove-all-href">
			          <xsl:with-param name="request" select="/Render/Request"/>
			          <xsl:with-param name="param" select="$facet"/>
			          <xsl:with-param name="anchor" select="'#people'"/>
			        </xsl:call-template></xsl:attribute>Clear All Selections</a></div>
		</xsl:if>
	</xsl:template>
	<!-- 
	
	RENDER FACET BUTTON
		
	-->
	<xsl:template name="render-facet-button">
		<xsl:param name="facet"/>
		<xsl:param name="value"/>
		<xsl:param name="class" select="''"/>
		<li>
			<xsl:choose>
				<xsl:when test="@Count = '0'">
					<a>
						<xsl:attribute name="class">ink <xsl:value-of select="$class"/></xsl:attribute>
						<xsl:attribute name="href">
	            <xsl:call-template name="add-href">
		          <xsl:with-param name="request" select="/Render/Request"/>
		          <xsl:with-param name="param" select="$facet"/>
		          <xsl:with-param name="val" select="$value"/>
		          <xsl:with-param name="anchor" select="'#people'"/>
		        </xsl:call-template>
		        </xsl:attribute>
						<span class="icon-checkbox">
						</span>
						<xsl:value-of select="."/>
					</a>
				</xsl:when>
				<xsl:when test="/Render/Request/*[name() = $facet and text() = $value]">
					<a>
						<xsl:attribute name="class"><xsl:value-of select="$class"/> ink</xsl:attribute>
						<xsl:attribute name="href">
	            <xsl:call-template name="remove-href">
		          <xsl:with-param name="request" select="/Render/Request"/>
		          <xsl:with-param name="param" select="$facet"/>
		          <xsl:with-param name="val" select="text()"/>
		          <xsl:with-param name="anchor" select="'#people'"/>
		        </xsl:call-template>
		        </xsl:attribute>
						<span class="icon-checkbox-checked">
						</span>
						<xsl:value-of select="."/>
					</a>
				</xsl:when>
				<xsl:when test="/Render[@View = 'Default']">
					<a>
						<xsl:attribute name="class">ink <xsl:value-of select="$class"/></xsl:attribute>
						<xsl:attribute name="href">
	            <xsl:call-template name="add-href">
		          <xsl:with-param name="request" select="/Render/Request"/>
		          <xsl:with-param name="param" select="$facet"/>
		          <xsl:with-param name="val" select="$value"/>
		          <xsl:with-param name="anchor" select="'#people'"/>
		        </xsl:call-template>
		        </xsl:attribute>
						<span class="icon-checkbox">
						</span>
						<xsl:value-of select="."/>
					</a>
				</xsl:when>
				<xsl:otherwise>
					<a>
						<xsl:attribute name="class"><xsl:value-of select="$class"/> ink</xsl:attribute>
						<xsl:attribute name="href">
	            <xsl:call-template name="add-href">
		          <xsl:with-param name="request" select="/Render/Request"/>
		          <xsl:with-param name="param" select="$facet"/>
		          <xsl:with-param name="val" select="$value"/>
		          <xsl:with-param name="anchor" select="'#people'"/>
		        </xsl:call-template>
		        </xsl:attribute>
						<span class="icon-checkbox">
						</span>
						<xsl:value-of select="."/>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</li>
	</xsl:template>
	<!-- 
	
	PEOPLE RESULTS RENDERING
		
	-->
	<xsl:template name="render-results">
		<div class="profile-listing">
			<xsl:call-template name="render-page-title"/>
			<!--<xsl:call-template name="render-pagination"/>-->
			<div class="hr4"/>
			<xsl:variable name="featurecount" select="number(count(/Render/Featured/Item))"/>
			<xsl:variable name="daycount" select="number(substring-before(/Render/hbs:Time/OLEAutomation,'.'))"/>
			<xsl:variable name="match" select="($daycount mod $featurecount)"/>
			<xsl:choose>
				<xsl:when test="/Render/Featured/Item and not(/Render/Request/*[not(@Default)])">
					<xsl:call-template name="render-person-feature" xml:space="preserve">
						<xsl:with-param name="item" select="/Render/Featured/Item[position() = $match + 1]"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<div class="shim20">
					</div>
					<div class="shim20">
					</div>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:for-each select="/Render/Items/Item">
				<xsl:if test="position() mod 3 = 1">
					<xsl:text disable-output-escaping="yes">&lt;div class=&quot;row listingrow&quot;&gt;</xsl:text>
				</xsl:if>
				<xsl:call-template name="render-person-result">
					<xsl:with-param name="item" select="."/>
				</xsl:call-template>
				<xsl:if test="position() mod 3 = 0 or position() = last()">
					<xsl:text disable-output-escaping="yes">&lt;/div&gt;</xsl:text>
				</xsl:if>
			</xsl:for-each>
			<!--<xsl:call-template name="render-pagination"/>-->
		</div>
	</xsl:template>
	<!-- 
	
	PAGE TITLE
		
	-->
	<xsl:template name="render-page-title">
		<xsl:choose>
			<xsl:when test="/Render/Request/type">
				<h1 class="delta">
					<xsl:for-each select="/Render/Request/*[name() = 'type']">
						<a class="ink">
							<xsl:attribute name="href">
		            <xsl:call-template name="remove-href">
			          <xsl:with-param name="request" select="/Render/Request"/>
			          <xsl:with-param name="param" select="name()"/>
			          <xsl:with-param name="val" select="text()"/>
			          <xsl:with-param name="anchor" select="'#people'"/>
			        </xsl:call-template>
		        </xsl:attribute>
							<xsl:value-of select="."/>
							<span class="icon-circle-close-red">
							</span>
						</a>
						<xsl:if test="not(position() = last())">
							<span class="txt-arrow">&#x2192;</span>
						</xsl:if>
					</xsl:for-each>
				</h1>
			</xsl:when>
			<xsl:otherwise>
				<h1 class="beta">All People</h1>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- 
	
	PEOPLE RENDERING
		
	-->
	<xsl:template name="render-person-result" xml:space="preserve">
		<xsl:param name="item" select="Empty"/>
		<xsl:variable name="ThumbnailPhoto"><xsl:call-template name="ExtractImagePath"><xsl:with-param name="imageTag" select="$item/@ows_ThumbnailPhoto"></xsl:with-param></xsl:call-template></xsl:variable>
		<xsl:variable name="FriendlyID">
	                  <xsl:value-of select="$item/@ows_Title" />
	                </xsl:variable>
		<xsl:variable name="fullname">
	                  <xsl:value-of select="$item/@ows_FirstName1" /><xsl:value-of select="$item/@ows_LastName" />
	                </xsl:variable>
		<xsl:variable name="topic">
	                  <xsl:value-of select="substring-after($item/@ows_Topic,';#')" />
	                </xsl:variable>
		<div class="span3" data-wcm-edit-url="/doctoral/Lists/Profiles/EditForm.aspx?ID={$item/@ows_ID}"
		                   data-wcm-edit-label="Profile"
		                   data-wcm-new-url="/doctoral/Lists/Profiles/NewForm.aspx?RootFolder=%2Fdoctoral%2FLists%2FProfiles&amp;ContentTypeId=0x01003645C1B0F5045140993EB8D4AD3AC60302002C3D2E03C2456E4ABDA5E52ED0CD40B8"
		                   data-wcm-new-label="Student Profile"
		                   data-wcm-new-url2="/doctoral/Lists/Profiles/NewForm.aspx?RootFolder=%2Fdoctoral%2FLists%2FProfiles&amp;ContentTypeId=0x01003645C1B0F5045140993EB8D4AD3AC60301005C2C1CA88F69CE4894A67F3B8BC18DE9"
		                   data-wcm-new-label2="Faculty Profile"
		>
			<div>
				<a href="profile-details.aspx?profile={normalize-space($FriendlyID)}">
					<img src="{$ThumbnailPhoto}" width="228" height="144" alt="{$fullname}" />
				</a>
			</div>
			<div class="media">
				<ul class="linear mu-uc">
					<xsl:choose>
						<xsl:when test="$item/@ows_Affiliation='Doctoral' and $item/@ows_ContentType = 'Student-Profile'">
							<li>Student</li>
							<li class="ash">
								<xsl:value-of select="$topic" disable-output-escaping="yes"/>
							</li>
						</xsl:when>
						<xsl:when test="$item/@ows_Affiliation='Alumni' and $item/@ows_ContentType = 'Student-Profile'">
							<li>
								<xsl:value-of select="normalize-space($item/@ows_Degree)" disable-output-escaping="yes"/>
								<xsl:value-of select="normalize-space($item/@ows_Year)" disable-output-escaping="yes"/>
							</li>
							<li class="ash">
								<xsl:value-of select="$topic" disable-output-escaping="yes"/>
							</li>
						</xsl:when>
						<xsl:when test="$item/@ows_ContentType = 'Student-Faculty-Profile' and $item/@ows_Affiliation='Alumni'">
							<li>Collaboration <xsl:value-of select="$item/@ows_Year" disable-output-escaping="yes"/></li>
							<li class="ash">
								<xsl:value-of select="$topic" disable-output-escaping="yes"/>
							</li>
						</xsl:when>
						<xsl:when test="$item/@ows_ContentType = 'Student-Faculty-Profile'">
							<li>Collaboration <xsl:value-of select="$item/@ows_Year" disable-output-escaping="yes"/></li>
							<li class="ash">
								<xsl:value-of select="$topic" disable-output-escaping="yes"/>
							</li>
						</xsl:when>
						<xsl:otherwise>
							<!--do nothing-->
						</xsl:otherwise>
					</xsl:choose>
				</ul>
				<h4 class="eta">
					<xsl:choose>
						<xsl:when test="$item/@ows_ContentType = 'Student-Faculty-Profile'">
							<a href="profile-details.aspx?profile={normalize-space($FriendlyID)}">
								<xsl:call-template name="collaboration-people">
									<xsl:with-param name="item" select="$item"/>			
								</xsl:call-template>
								<xsl:call-template name="render-faculty">
									<xsl:with-param name="facultylookup"><xsl:value-of select="$item/@ows_FacultySelector" disable-output-escaping="yes"/></xsl:with-param>
									<xsl:with-param name="all-faculty"><xsl:copy-of select="/Render/FacultyLookupResults"/></xsl:with-param>
									<xsl:with-param name="type">profilefullname</xsl:with-param>
									<xsl:with-param name="handentered"><xsl:value-of select="$item/@ows_FacultyFirstName" disable-output-escaping="yes"/> <xsl:value-of select="$item/@ows_FacultyLastName" disable-output-escaping="yes"/></xsl:with-param>
								</xsl:call-template>								
							</a>
						</xsl:when>
						<xsl:otherwise>
							<a href="profile-details.aspx?profile={normalize-space($FriendlyID)}">
								<xsl:value-of select="normalize-space($item/@ows_FirstName1)"/>
								<xsl:value-of select="normalize-space($item/@ows_LastName)"/>
							</a>
						</xsl:otherwise>
					</xsl:choose>
				</h4>
			</div>
		</div>
	</xsl:template>
	<!-- 
	
	PEOPLE FEATURE RENDERING
		
	-->
	<xsl:template name="render-person-feature" xml:space="preserve">
		<xsl:param name="item" select="Empty"/>
		<xsl:variable name="FriendlyID">
	                  <xsl:value-of select="$item/@ows_Title" />
	                </xsl:variable>
		<xsl:variable name="ThumbnailPhoto"><xsl:call-template name="ExtractImagePath"><xsl:with-param name="imageTag" select="$item/@ows_ThumbnailPhoto"></xsl:with-param></xsl:call-template></xsl:variable>
		<div class="row feature">
			<div class="span3">
				<a href="profile-details.aspx?profile={normalize-space($FriendlyID)}">
					<img src="{$ThumbnailPhoto}"/>
				</a>
			</div>
			<div class="span6" data-wcm-edit-url="/doctoral/Lists/Profiles/EditForm.aspx?ID={$item/@ows_ID}"
		                   data-wcm-edit-label="Profile"
		                   data-wcm-new-url="/doctoral/Lists/Profiles/NewForm.aspx?RootFolder=%2Fdoctoral%2FLists%2FProfiles&amp;ContentTypeId=0x01003645C1B0F5045140993EB8D4AD3AC60302002C3D2E03C2456E4ABDA5E52ED0CD40B8"
		                   data-wcm-new-label="Student Profile"
		                   data-wcm-new-url2="/doctoral/Lists/Profiles/NewForm.aspx?RootFolder=%2Fdoctoral%2FLists%2FProfiles&amp;ContentTypeId=0x01003645C1B0F5045140993EB8D4AD3AC60301005C2C1CA88F69CE4894A67F3B8BC18DE9"
		                   data-wcm-new-label2="Faculty Profile">
				<xsl:variable name="topic">
	                  <xsl:value-of select="substring-after($item/@ows_Topic,';#')" />
	                </xsl:variable>
				<h4 class="eta-uc">Faculty&#160;/&#160;Student Collaboration</h4>
				<ul class="linear mu-uc">
					<li>Collaboration<xsl:if test="$item/@ows_Year != ''"> <xsl:value-of select="$item/@ows_Year" disable-output-escaping="yes"/></xsl:if></li>
					<li class="ash">
						<xsl:value-of select="$topic" disable-output-escaping="yes"/>
					</li>
				</ul>
				<h3 class="beta">
					<a href="profile-details.aspx?profile={normalize-space($FriendlyID)}">
								<xsl:call-template name="collaboration-people-firstnames">
									<xsl:with-param name="item" select="$item"/>
								</xsl:call-template>
								<xsl:call-template name="render-faculty">
									<xsl:with-param name="facultylookup"><xsl:value-of select="$item/@ows_FacultySelector" disable-output-escaping="yes"/></xsl:with-param>
									<xsl:with-param name="all-faculty"><xsl:copy-of select="/Render/FacultyLookupResults"/></xsl:with-param>
									<xsl:with-param name="type">profilefirstname</xsl:with-param>
									<xsl:with-param name="handentered"><xsl:value-of select="$item/@ows_FacultyFirstName" disable-output-escaping="yes"/></xsl:with-param>

								</xsl:call-template>	
							</a>
				</h3>
				<blockquote class="kappa-uc" style="text-indent:-10px">“<xsl:value-of select="normalize-space($item/@ows_Quote)" disable-output-escaping="yes"/>”</blockquote>
			</div>
		</div>
		<div class="hr">
		</div>
	</xsl:template>
	<!-- 
	
	PAGINATION RENDERING
		
	-->
	<xsl:template name="render-pagination">
		<div class="row nu pagination">
			<div class="span3">Results per page: 
	        <select class="step">
	        <option><xsl:if test="/Render/Request/step = '24'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>24</option>
	        <option><xsl:if test="/Render/Request/step = '48'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>48</option>
	        <option><xsl:if test="/Render/Request/step = '72'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>72</option>
	        <option><xsl:if test="/Render/Request/step = '96'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>96</option>
	        </select>
        </div>
			<div class="span6" style="text-align:right">
				<xsl:choose>
					<xsl:when test="/Render/Request/page = '1'">
						<span class="icon-circle-arrow-left fade">
						</span>
					</xsl:when>
					<xsl:otherwise>
						<a>
							<xsl:attribute name="href">
            <xsl:call-template name="replace-href">
	          <xsl:with-param name="request" select="/Render/Request"/>
	          <xsl:with-param name="param" select="'page'"/>
	          <xsl:with-param name="val" select="number(/Render/Request/page) - 1"/>
	          <xsl:with-param name="anchor" select="'#people'"/>
	        </xsl:call-template>
        </xsl:attribute>
							<span class="icon-circle-arrow-left">
							</span>
						</a>
					</xsl:otherwise>
				</xsl:choose>
				<span>Showing <span class="mu-uc"><xsl:value-of select="/Render/Items/@Start"/>-<xsl:value-of select="/Render/Items/@End"/></span> of <span class="mu-uc"><xsl:value-of select="/Render/Items/@Total"/></span></span>
				<xsl:choose>
					<xsl:when test="/Render/Items[@LastPage]">
						<span class="icon-circle-arrow-right fade">
						</span>
					</xsl:when>
					<xsl:otherwise>
						<a>
							<xsl:attribute name="href">
            <xsl:call-template name="replace-href">
	          <xsl:with-param name="request" select="/Render/Request"/>
	          <xsl:with-param name="param" select="'page'"/>
	          <xsl:with-param name="val" select="number(Request/page) + 1"/>
	          <xsl:with-param name="anchor" select="'#people'"/>
	        </xsl:call-template>
        </xsl:attribute>
							<span class="icon-circle-arrow-right">
							</span>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:template>
	<!-- 
	
	URL BUILDERS
		
	-->
	<xsl:template name="remove-href">
		<xsl:param name="request"/>
		<xsl:param name="param"/>
		<xsl:param name="val"/>
		<xsl:param name="anchor" select="'#people'"/>
		<xsl:variable name="tmp">?<xsl:for-each select="$request/*[not(name() = $param and text() = $val or name() = 'page')]"><xsl:value-of select="name()"/>=<xsl:value-of select="hbs:UrlEncode(.)"/><xsl:if test="position()!=last()">&amp;</xsl:if></xsl:for-each><xsl:value-of select="$anchor"/></xsl:variable>
		<xsl:value-of select="normalize-space($tmp)"/>
	</xsl:template>
	<xsl:template name="remove-all-href">
		<xsl:param name="request"/>
		<xsl:param name="param"/>
		<xsl:param name="anchor" select="'#people'"/>
		<xsl:variable name="tmp">?<xsl:for-each select="$request/*[name() != $param]"><xsl:value-of select="name()"/>=<xsl:value-of select="hbs:UrlEncode(.)"/><xsl:if test="position()!=last()">&amp;</xsl:if></xsl:for-each><xsl:value-of select="$anchor"/></xsl:variable>
		<xsl:value-of select="normalize-space($tmp)"/>
	</xsl:template>
	<xsl:template name="add-href">
		<xsl:param name="request"/>
		<xsl:param name="param"/>
		<xsl:param name="val"/>
		<xsl:param name="anchor" select="'#people'"/>
		<xsl:variable name="link">
		<xsl:choose>
			<xsl:when test="$request/*">
				<xsl:variable name="existing">
				   <xsl:call-template name="build-href"><xsl:with-param name="request" select="$request"/></xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="concat($existing,'&amp;',$param,'=',hbs:UrlEncode($val),$anchor)" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat('?',$param,'=',hbs:UrlEncode($val),$anchor)" />
			</xsl:otherwise>
		</xsl:choose>
      </xsl:variable>
		<xsl:value-of select="normalize-space($link)"/>
	</xsl:template>
	<xsl:template name="replace-href">
		<xsl:param name="request"/>
		<xsl:param name="param"/>
		<xsl:param name="val"/>
		<xsl:param name="anchor" select="'#people'"/>
		<xsl:variable name="link">
		<xsl:choose>
			<xsl:when test="$request/*">
				<xsl:variable name="existing">
				   <xsl:call-template name="build-href"><xsl:with-param name="request" select="$request"/></xsl:call-template>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="contains($existing,concat($param,'='))">
						<xsl:variable name="before" select="substring-before($existing,$param)"/>
						<xsl:variable name="after" select="substring-after(substring-after($existing,$param),'&amp;')"/>
						<!--<xsl:variable name="after" select="substring-after(substring-before($existing,$param),'&amp;')"/>-->
						<!--<xsl:value-of select="concat($before,$after,$param,'=',hbs:UrlEncode($val))" />-->
						<xsl:value-of select="concat($before,$after,'&amp;',$param,'=',hbs:UrlEncode($val))" />					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="concat($existing,'&amp;',$param,'=',hbs:UrlEncode($val))" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat('?',$param,'=',hbs:UrlEncode($val))" />
			</xsl:otherwise>
		</xsl:choose>
      </xsl:variable>
		<xsl:value-of select="concat(hbs:Replace(normalize-space($link),'&amp;&amp;','&amp;'),$anchor)"/>
	</xsl:template>
	<xsl:template name="build-href">
		<xsl:param name="request" />?<xsl:for-each select="$request/*[name() != 'page']"><xsl:value-of select="name()"/>=<xsl:value-of select="hbs:UrlEncode(.)"/><xsl:if test="position()!=last()">&amp;</xsl:if></xsl:for-each>
	</xsl:template>
</xsl:stylesheet>