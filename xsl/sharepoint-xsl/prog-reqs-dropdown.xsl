﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:param name="args"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<!--
    
    Query Data
    
    -->
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<xsl:call-template name="query-topics"/>
			<xsl:copy-of select="hbs:Request/PATH_INFO"/>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<!--
    
    Render Output
    
    -->
	<xsl:template match="/Render">
		<div class="dropdown-container2">
			<a class="dropdown-toggle2 white mu-uc black-bg" href="#">
				<span class="icon-expand-inverted inherit-bg">
				</span>Select Another Degree
   				</a>
			<div class="hr">
			</div>
			<div class="dropdown-menu2 mu-uc black-bg dropdown-split3">
				<xsl:variable name="pathinfo"><xsl:value-of select="PATH_INFO"/></xsl:variable>
				<xsl:variable name="split" select="ceiling(count(//z:row) div 3)"/>
				<ul>
					<xsl:for-each select="hbs:QueryResults/z:row">
						<xsl:call-template name="render-dropdown-item">
							<xsl:with-param name="item" select="."/>
							<xsl:with-param name="path-info" select="$pathinfo"/>
						</xsl:call-template>
					</xsl:for-each>
				</ul>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="render-dropdown-item">
		<xsl:param name="item"/>
		<xsl:param name="path-info"/>
		<xsl:variable name="local-folder"><xsl:choose><xsl:when test="contains($item/@ows_LocalFolder,'accounting')">default.aspx</xsl:when><xsl:otherwise><xsl:value-of select="$item/@ows_LocalFolder"/>.aspx</xsl:otherwise></xsl:choose></xsl:variable>
		<li>
			<a class="white" href="{$local-folder}">
				<xsl:variable name="local-folder-clean">/<xsl:value-of select="normalize-space($local-folder)"/></xsl:variable>
				<xsl:if test="contains($path-info,$local-folder-clean)">
					<xsl:attribute name="class"> inherit-color active inherit-bg hover</xsl:attribute>
				</xsl:if>
				<xsl:value-of select="$item/@ows_Title" disable-output-escaping="yes"/>
			</a>
		</li>
	</xsl:template>
</xsl:stylesheet>