﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
xmlns:hbs="urn:hbs"
xmlns:z="#RowsetSchema"
xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime"  
xmlns:msxsl="urn:schemas-microsoft-com:xslt"
exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:import href="lib/utils.xsl"/>
	<xsl:import href="lib/renderings.xsl"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<xsl:call-template name="query-sidebar-carousel"/>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<xsl:template match="/Render">
		<div class="carousel-container has-shuffle">
			<ul class="carousel-panels">
				<xsl:for-each select="hbs:QueryResults/z:row">
					<xsl:if test="position() &lt; 5">
						<li data-wcm-edit-url="/doctoral/Lists/Profiles/EditForm.aspx?ID={@ows_ID}">
							<xsl:variable name="sidebarpic"><xsl:call-template name="ExtractImagePath"><xsl:with-param name="imageTag" select="@ows_SidebarPhoto"></xsl:with-param></xsl:call-template></xsl:variable>
							<img src="{$sidebarpic}" border="0" width="200" height="200" class="sidebar-photo"/>
							<div class="kappa-uc hbsred sidebar-personname">
								<xsl:variable name="personurl">/doctoral/research-community/students-faculty/Pages/profile-details.aspx?profile=<xsl:value-of select="@ows_Title" disable-output-escaping="yes"/>
</xsl:variable>
								<a href="{$personurl}">
									<xsl:value-of select="@ows_FirstName1" disable-output-escaping="yes"/>&#160;<xsl:value-of select="@ows_LastName" disable-output-escaping="yes"/></a>
							</div>
							<xsl:if test="@ows_Placement != ''">
								<p>
									<strong class="mu-uc">Placement</strong>
									<br/>
									<xsl:value-of select="@ows_Placement" disable-output-escaping="yes"/>
								</p>
							</xsl:if>
							<xsl:if test="@ows_AreasOfStudy != ''">
								<p>
									<strong class="mu-uc">Area of Study</strong>
									<br/>
									<xsl:value-of select="@ows_AreasOfStudy" disable-output-escaping="yes"/>
								</p>
							</xsl:if>
							<xsl:if test="@ows_HomeRegion != ''">
								<p>
									<strong class="mu-uc">Home Region</strong>
									<br/>
									<xsl:value-of select="@ows_HomeRegion" disable-output-escaping="yes"/>
								</p>
							</xsl:if>
							<xsl:if test="@ows_UndergraduateEducation != ''">
								<p>
									<strong class="mu-uc">Education</strong>
									<br/>
									<xsl:value-of select="@ows_UndergraduateEducation" disable-output-escaping="yes"/>
								</p>
							</xsl:if>
						</li>
					</xsl:if>
				</xsl:for-each>
			</ul>
			<div class="carousel-nav">
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
