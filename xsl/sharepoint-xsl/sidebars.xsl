﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:import href="lib/renderings.xsl"/>
	<xsl:import href="lib/utils.xsl"/>
	<xsl:template match="/Root">
		<Step1>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<CurrentPage>
				<hbs:GetPage/>
			</CurrentPage>
			<hbs:Repeat/>
		</Step1>
	</xsl:template>
	<xsl:template match="/Step1">
		<Render>
			<xsl:variable name="sidebarIDs">
				<xsl:call-template name="util-split-multivalue">
					<xsl:with-param name="field" select="CurrentPage/hbs:Page/z:row/@ows_RelatedSidebars"/>
          		</xsl:call-template>
        	</xsl:variable>
			<xsl:for-each select="msxsl:node-set($sidebarIDs)/Item">
				<Sidebars>
					<xsl:call-template name="query-sidebars">
						<xsl:with-param name="key" select="@Key"/>
					</xsl:call-template>
				</Sidebars>
			</xsl:for-each>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<xsl:template match="/Render">
		<xsl:for-each select="Sidebars/hbs:QueryResults/z:row">
			<div class="sidebar-item" data-wcm-edit-url="/doctoral/Lists/Sidebars/EditForm.aspx?ID={@ows_ID}">
				<xsl:value-of select="@ows_Content" disable-output-escaping="yes"/>
			</div>
			<div class="hr">&#160;</div>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>