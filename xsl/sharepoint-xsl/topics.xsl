﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:hbs="urn:hbs"
   xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="hbs z msxsl">
	<xsl:import href="lib/queries.xsl"/>
	<xsl:param name="args"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<!--
    
    Query Data
    
    -->
	<xsl:template match="/Root">
		<Render>
			<xsl:if test="hbs:Request/debug='true'">
				<hbs:Debug/>
			</xsl:if>
			<xsl:call-template name="query-topics"/>
			<xsl:copy-of select="hbs:Request/PATH_INFO"/>
			<hbs:Repeat/>
		</Render>
	</xsl:template>
	<!--
    
    Render Output
    
    -->
	<xsl:template match="/Render">
		<h1 class="delta-uc">8 Doctoral Programs</h1>
		<div class="shim21"></div>
		<xsl:variable name="pathinfo"><xsl:value-of select="PATH_INFO"/></xsl:variable>
		<xsl:variable name="split" select="ceiling(count(//z:row) div 2)"/>
		<div class="row">
			<div class="span4">
				<ul class="bulleted-custom">
					<xsl:for-each select="hbs:QueryResults/z:row[position() &lt;= $split]">
						<xsl:call-template name="render-dropdown-item">
							<xsl:with-param name="item" select="."/>
							<xsl:with-param name="path-info" select="$pathinfo"/>
						</xsl:call-template>
					</xsl:for-each>
				</ul>
			</div>
			<div class="span4">
				<ul class="bulleted-custom exdent">
					<xsl:for-each select="hbs:QueryResults/z:row[position() &gt; $split]">
						<xsl:call-template name="render-dropdown-item">
							<xsl:with-param name="item" select="."/>
							<xsl:with-param name="path-info" select="$pathinfo"/>
						</xsl:call-template>
					</xsl:for-each>
				</ul>
			</div>
		</div>
		<div class="shim22"></div>
	</xsl:template>
	<xsl:template name="render-dropdown-item">
		<xsl:param name="item"/>
		<xsl:param name="path-info"/>
		<xsl:variable name="local-folder">/doctoral/areas-of-study/<xsl:value-of select="$item/@ows_LocalFolder"/>/</xsl:variable>
		<li class="mu-uc" style="list-style-type: square;" data-wcm-edit-url="/doctoral/Lists/Topics/EditForm.aspx?ID={$item/@ows_ID}">
			<a href="{$local-folder}">
				<xsl:variable name="local-folder-clean">/<xsl:value-of select="normalize-space($local-folder)"/></xsl:variable>
				<xsl:value-of select="$item/@ows_Title" disable-output-escaping="yes"/>
			</a>
			<div class="shim6"></div>
		</li>
	</xsl:template>
</xsl:stylesheet>